

#ifndef SAMD20J_H_
	#define SAMD20J_H_
	#include "asf.h"
	#include "uart_drivers.h"
	
	//EEPROM response codes
	
	struct rtc_module				rtc_instance;
	struct rtc_calendar_alarm_time	alarm;
	struct rtc_calendar_time		time;
		
	bool	 EEPROM_write(uint16_t offset,uint16_t length, uint8_t *data);
	bool	 EEPROM_read_num(uint16_t offset, uint8_t *data);
	bool	 EEPROM_read_char(uint16_t offset,uint16_t length, uint8_t *data);
	bool     EEPROM_clear(uint16_t offset,uint16_t length) ;
	void	 wdt_reset(void);
	void tRTCGetTimeStamp(char *timestamp,int r_type);


	
#endif /* SAMD20_H_ */