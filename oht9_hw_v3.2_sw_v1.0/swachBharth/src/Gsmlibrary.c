
#include "Gsmlibrary.h"

uint32_t _ip    = 0;
char ipAddr[32] = {0};
char ip_string[16] = {0};
extern char gImeiNo[IMEI_SIZE];

uint32_t strToIP(const char* str);
static char gatewaySendNumber[]	 = "9891024477";
#define  INVALID_LAT_LONG	60
#define  BOOTUPDATA		  	0
#define  LIVEDATA         	1



#ifdef ENABLE_OTA
	bool program_memory(uint32_t address, uint8_t *buffer, uint16_t len);
#endif

bool gsmCheckPowerUp(void) { 
	serial_write("IS GSM POWERED ON        : ");
	int retry_count = 0;
	while (retry_count++ < 3) {
		if(tGsmPortSendCmdAndCheckResp("AT\r\n","OK\r\n",0)) {
			serial_write_ln("YES ");
			return true;
		}
		else {
			if (retry_count >= 2) {
					serial_write_ln("NO ");
					return false;
			}
		}
	}
}

/*This Function is used Turn On , Turn Off and Reboot gsm
 * if mode 
 *   0 = OFF
 *   1 = ON
 *   2 = REBOOT 
 */

void gsmPowerUpDown(uint8_t pin, uint8_t mode) {
  uint8_t loop = 1, repeat = 1;
  
  if(mode == REBOOT ) {
	  repeat = 2;
  }
  
  for(loop = 1; loop <= repeat; loop ++) { 
	  port_pin_set_output_level(pin,true);
	  delay_ms(3000);
	  port_pin_set_output_level(pin,false);
	  delay_ms(10000);   
  }
  if (mode == REBOOT || mode == ON) {		
	  delay_ms(30000);
  }
}
  
bool getImei(char *imei_no) {
	//AT+GSN
	//123456789012345	--> CRLF + 15 + CRLF = 19
	//OK				--> CRLF + 2 + CRLF =  6
	
	char gprsBuffer[GSMBUFFER40] = {0};
	char *s = NULL;
	
	tGsmPortFlushSerial();
	tGsmPortCleanBuffer(gprsBuffer,GSMBUFFER40);
	tGsmPortSendCmd("AT+GSN\r\n");
	tGsmPortReadData(gprsBuffer,GSMBUFFER40,DEFAULTTIMEOUT);
	
	if (NULL != ( s = strstr( gprsBuffer, "OK" ))) {
		memcpy(imei_no, gprsBuffer + 2,IMEILENGTH);
		imei_no[IMEI_SIZE - 1] = '\0';
		return true;
	}
	return false;
}

bool getImsi(char *imsi_no) {
	//AT+GSN
	//123456789012345	--> CRLF + 15 + CRLF = 19
	//OK				--> CRLF + 2 + CRLF =  6
	
	char gprsBuffer[GSMBUFFER40] = {0};
	char *s = NULL;
	
	tGsmPortFlushSerial();
	tGsmPortCleanBuffer(gprsBuffer,GSMBUFFER40);
	tGsmPortSendCmd("AT+CIMI\r\n");
	tGsmPortReadData(gprsBuffer,GSMBUFFER40,DEFAULTTIMEOUT);
	
	if (NULL != ( s = strstr( gprsBuffer, "OK" ))) {
		memcpy(imsi_no, gprsBuffer + 2,IMSILENGTH);
		imsi_no[IMSI_SIZE - 1] = '\0';
		return true;
	}
	return false;
}

void getIPAddr(char *ipaddr) {
	strcpy(ipaddr,ip_string);
}

void deleteAllSMS(int x) {
	
	// AT+CMGDA="DEL ALL"
	tGsmPortFlushSerial();
	if (x == 0) {
		tGsmPortSendCmd("AT+CMGDA=\"DEL ALL\"\r\n");
		tGsmPortWaitForResponse("OK\r\n",0,7);
	}
	
	if(x == 1) {
		for(int i=0;i<25;i++) {
			deleteSMS(i);	
		}
	}
	serial_debug_ln("deleted all SMS",STR,GSM_DBG_LEVEL);
	serial_write_ln("DELETING ALL SMS         : SUCESSFUL");
}

uint8_t GSM_init(void) {
	
	if(!tGsmPortSendCmdAndCheckResp("AT\r\n","OK\r\n",0)) {
	    serial_write_ln("AT error");
		return 0;
	}
	
	if(!tGsmPortSendCmdAndCheckResp("ATE0\r\n","OK\r\n",0)) {
		serial_write_ln("AT ECHO error");
		return 0;
	}
	
	if(!tGsmPortSendCmdAndCheckResp("AT+CFUN=1\r\n","OK\r\n",0)) {
	   serial_write_ln("AT+CFUN error");
		return 0;
	}
	
	if(!checkSIMStatus()) {
		serial_write_ln("IS SIM DETECTED          : NO");
		return CPIN_ERROR;
	}
	serial_write_ln("IS SIM DETECTED          : YES");
	return true;
}

bool checkSIMStatus(void) {
	
	char gprsBuffer[GSMBUFFER40] = {0};
	uint8_t  count = 0;
	
	while(count < 3) {
		tGsmPortSendCmd("AT+CPIN?\r\n");
		tGsmPortCleanBuffer(gprsBuffer,GSMBUFFER40);
		tGsmPortReadData(gprsBuffer,GSMBUFFER40,DEFAULTTIMEOUT);
		
		if((NULL != strstr(gprsBuffer,"+CPIN: READY"))) {
			return true;
		}
		count++;
		delay_ms(300);
	}																		   
	return false;
}	

bool sendSmsWithDeviceID(char *number, const char *data) {
	
	char messageContent[SMS_BUFFER_SIZE + GSMBUFFER40] = {0};
	
	if(!tGsmPortSendCmdAndCheckResp("AT+CMGF=1\r\n", "OK\r\n", 0)) { 
		return false;
	}
	delay_ms(500);
	sprintf(messageContent,"IMEI:%s\n%s",gImeiNo,data);
	tGsmPortFlushSerial();
	tGsmPortSendCmd("AT+CMGS=\"");
	tGsmPortSendCmd(number);
	serial_write_ln(messageContent);
	if(!tGsmPortSendCmdAndCheckResp("\"\r\n",">",0)) {
		return false;
	}
	
	delay_ms(1000);
	tGsmPortSendCmd(messageContent);
	delay_ms(500);
	tGsmPortSendEndMark();
	return tGsmPortWaitForResponse("OK\r\n", 0,5);
}

int8_t isSMSunread() {
    char gprsBuffer[GSMBUFFER50] = {0};  //48 is enough to see +CMGL:
    char *s = NULL ;
    
    //List of all UNREAD SMS and DON'T change the SMS UNREAD STATUS
    tGsmPortSendCmd("AT+CMGL=\"REC UNREAD\",1\r\n");
    /*If you want to change SMS status to READ you will need to send:
		AT+CMGL=\"REC UNREAD\"\r\n
		This command will list all UNREAD SMS and change all of them to READ
		
		If there is not SMS, response is (30 chars)
		AT+CMGL="REC UNREAD",1  --> 22 + 2
		--> 2
		OK                      --> 2 + 2
		
		If there is SMS, response is like (>64 chars)
		AT+CMGL="REC UNREAD",1
		+CMGL: 9,"REC UNREAD","XXXXXXXXX","","14/10/16,21:40:08+08"
		Here SMS text.
		OK  
		
		or
		
		AT+CMGL="REC UNREAD",1
		+CMGL: 9,"REC UNREAD","XXXXXXXXX","","14/10/16,21:40:08+08"
		Here SMS text.
		+CMGL: 10,"REC UNREAD","YYYYYYYYY","","14/10/16,21:40:08+08"
		Here second SMS        
		OK           
	*/
	
    tGsmPortCleanBuffer(gprsBuffer,GSMBUFFER30); 
    tGsmPortReadData(gprsBuffer,GSMBUFFER30,DEFAULTTIMEOUT); 
    //Serial.print("Buffer isSMSunread: ");Serial.println(gprsBuffer);
	
    if(NULL != ( s = strstr(gprsBuffer,"OK"))) {
        //In 30 bytes "doesn't" fit whole +CMGL: response, if recieve only "OK"
        //    means you don't have any UNREAD SMS
        delay_ms(50);
        return 0;
	} 
	else {
		
        //More buffer to read
        //We are going to flush serial data until OK is recieved
        tGsmPortWaitForResponse("OK\r\n", 0,5);        
        //tGsmPortFlushSerial();
        //We have to call command again
        tGsmPortSendCmd("AT+CMGL=\"REC UNREAD\",1\r\n");
        tGsmPortCleanBuffer(gprsBuffer,48); 
        tGsmPortReadData(gprsBuffer,47,DEFAULTTIMEOUT);
		
		if(NULL != ( s = strstr(gprsBuffer,"+CMGL:"))) {
            //There is at least one UNREAD SMS, get index/position
            s = strstr(gprsBuffer,":");
            if (s != NULL) {
                //We are going to flush serial data until OK is received
                tGsmPortWaitForResponse("OK\r\n", 0,5);
                return atoi(s+1);
			}
		} 
		else {
            return -1; 
		}
	} 
    return -1;
}

bool readSMS(int messageIndex, char *message, int length, char *phone, char *datetime) {
	/* Response is like:
		AT+CMGR=2
		
		+CMGR: "REC READ","XXXXXXXXXXX","","14/10/09,17:30:17+08"
		SMS text here
		
		So we need (more or lees), 80 chars plus expected message length in buffer. CAUTION FREE MEMORY
	*/
	
    int i = 0;
    char gprsBuffer[SMS_BUFFER_SIZE]; //#CHANGES_3P1
	memset(gprsBuffer,0,SMS_BUFFER_SIZE); //#CHANGES_3P1
    char num[4];
	memset(num,0,4)	;
    char *p   = NULL;
    char *p2  = NULL;
    char *s   = NULL;
    
    tGsmPortSendCmdAndCheckResp("AT+CMGF=1\r\n","OK\r\n",0);
    delay_ms(1000);
	//sprintf(cmd,"AT+CMGR=%d\r\n",messageIndex);
    //tGsmPortSendCmd(cmd);
	tGsmPortSendCmd("AT+CMGR=");
	itoa(messageIndex, num, 10);
	tGsmPortSendCmd(num);
	tGsmPortSendCmd("\r\n");
    tGsmPortCleanBuffer(gprsBuffer,SMS_BUFFER_SIZE); //#CHANGES_3P1
    tGsmPortReadData(gprsBuffer,SMS_BUFFER_SIZE,5);  //#CHANGES_3P1
	serial_write_ln(gprsBuffer);
    if(NULL != ( s = strstr(gprsBuffer,"+CMGR:"))){
        // Extract phone number string
        p  = strstr(s,",");
        p2 = p + 2; //We are in the first phone number character
        p  = strstr((char *)(p2), "\"");
        if (NULL != p) {
            i = 0;
            while (p2 < p) {
                phone[i++] = *(p2++);
			}
            phone[i] = '\0';  
			serial_write_ln("--phoneNumberPrinting--");     
			serial_write_ln(phone);     
		}
        // Extract date time string
        p = strstr((char *)(p2),",");
        p2 = p + 1; 
        p = strstr((char *)(p2), ","); 
        p2 = p + 2; //We are in the first date time character
        p = strstr((char *)(p2), "\"");
        if (NULL != p) {
            i = 0;
            while (p2 < p) {
                datetime[i++] = *(p2++);
			}
            datetime[i] = '\0';
		}        
        if(NULL != ( s = strstr(s,"\r\n"))){
            i = 0;
            p = s + 2;
            while((*p != '\r')&&(i < length-1)) {
                message[i++] = *(p++);
			}
            message[i] = '\0';
		}
        return true;
	}
    return false;    
}
bool deleteSMS(int index) {

	char num[4] = {'\0'};
	
	tGsmPortSendCmd("AT+CMGD=");
	itoa(index, num, 10);
	tGsmPortSendCmd(num);
	return tGsmPortSendCmdAndCheckResp("\r\n","OK\r\n",0);
}

bool getDateTime(char *buffer) {
	//AT+CCLK?						--> 8 + CR = 9
	//+CCLK: "14/11/13,21:14:41+04"	--> CRLF + 29+ CRLF = 33
	//								
	//OK							--> CRLF + 2 + CRLF =  6
	
    uint8_t i = 0;
    char gprsBuffer[GSMBUFFER50] =  {0};
    char *p  = NULL;
    char *s  = NULL;
	
	tGsmPortFlushSerial();
    tGsmPortSendCmd("AT+CCLK?\r\n");
    tGsmPortCleanBuffer(gprsBuffer,GSMBUFFER50);
    tGsmPortReadData(gprsBuffer,GSMBUFFER50,DEFAULTTIMEOUT);
    
    if(NULL != ( s = strstr(gprsBuffer,"+CCLK:"))) {
        s = strstr((char *)(s),"\"");
        s = s + 1;  //We are in the first phone number character 
        p = strstr((char *)(s),"\""); //p is last character """
        if ((NULL != s)  && (NULL != p)) {
            i = 0;
            while (s < p) {
				buffer[i++] = *(s++);
			}
            buffer[i] = '\0';            
		}
        return true;
	}  
    return false;
}

bool getSIMRegistration(int *networkType) {
	//AT+CREG=1						--> ENABLE NETWORK REGISTRATION
	//OK
	//AT+CREG?						--> CHECK REGISTRATION STATUS
	//+CREG:1,<NETWORK TYPE>		--> NW TYPE IS HOME OR ROAMING
	
	char gprsBuffer[GSMBUFFER30] = {0};
	uint8_t i = 0;
	char *p = NULL;
	char *s = NULL;
	char buffers[4] = {0};
		
	tGsmPortFlushSerial();
	
	if(!tGsmPortSendCmdAndCheckResp("AT+CREG=1\r\n","OK\r\n",0)) {
		return false;
	}
	tGsmPortFlushSerial();
	tGsmPortSendCmd("AT+CREG?\r");
	tGsmPortCleanBuffer(gprsBuffer, GSMBUFFER30);
	tGsmPortReadData(gprsBuffer, GSMBUFFER30, DEFAULTTIMEOUT);
	
	if (NULL != (s = strstr(gprsBuffer, "+CREG:"))) {
		s = strstr((char *)(s), ",");
		s = s + 1;  //We are in the first phone number character
		p = strstr((char *)(s), "\r"); //p is last character """
		if ((NULL != s) && (NULL != p)) {
			i = 0;
			while (s < p) {
				buffers[i++] = *(s++);
			}
			buffers[i] = '\0';
		}
		*networkType = atoi(buffers);
		return true;
	}
	return false;
}

bool getProviderName(char *buffer) {
	//AT+CSPN?
	//+CSPN: "!dea", 0
	uint8_t i = 0;
	char gprsBuffer[GSMBUFFER50] = {0};
	char *p = NULL;
	char *s = NULL;
	
	tGsmPortFlushSerial();
	tGsmPortSendCmd("AT+CSPN?\r\n");
	tGsmPortCleanBuffer(gprsBuffer,GSMBUFFER50);
	tGsmPortReadData(gprsBuffer,GSMBUFFER50,DEFAULTTIMEOUT);
	
	if(NULL != ( s = strstr(gprsBuffer,"+CSPN:"))) {
		s = strstr((char *)(s),"\"");
		s = s + 1;  //We are in the first phone number character
		p = strstr((char *)(s),"\""); //p is last character """
		if ((NULL != s) && (NULL != p)) {
			while (s < p) {
				buffer[i++] = *(s++);
			}
			buffer[i] = '\0';
		}
		return true;
	}
	return false;
}

bool getSignalStrength(int *buffer) {
		//AT+CSQ						--> 6 + CR = 10
		//+CSQ: <rssi>,<ber>			--> CRLF + 5 + CRLF = 9
		//OK							--> CRLF + 2 + CRLF =  6
		
		uint8_t i = 0;
		char gprsBuffer[GSMBUFFER30] = {0};
		char *p = NULL;
		char *s = NULL;
		char buffers[4] = {0};
		
		tGsmPortFlushSerial();
		tGsmPortSendCmd("AT+CSQ\r\n");
		tGsmPortCleanBuffer(gprsBuffer, GSMBUFFER30);
		tGsmPortReadData(gprsBuffer, GSMBUFFER30, DEFAULTTIMEOUT);
		
		if (NULL != (s = strstr(gprsBuffer, "+CSQ:"))) {
			s = strstr((char *)(s), " ");
			s = s + 1;  //We are in the first phone number character
			p = strstr((char *)(s), ","); //p is last character """
			if ((NULL != s) && (NULL != p)) {
				i = 0;
				while (s < p) {
					buffers[i++] = *(s++);
				}
				buffers[i] = '\0';
			}
			*buffer = ZERO_DBM_VALUE + (2 * atoi(buffers));
			return true;
		}
		return false;
}

bool join(char *apn) {
	uint16_t i = 0;
	char *p =  NULL, *s = NULL;
	
	//Select multiple connection
	//tGsmPortSendCmdAndCheckResp("AT+CIPMUX=1\r\n","OK",DEFAULTTIMEOUT,CMD);
	
	//set APN. OLD VERSION
	//snprintf(cmd,sizeof(cmd),"AT+CSTT=\"%s\",\"%s\",\"%s\"\r\n",_apn,_userName,_passWord);
	//tGsmPortSendCmdAndCheckResp(cmd, "OK\r\n", DEFAULTTIMEOUT,CMD);
	tGsmPortSendCmd("AT+CSTT=\"");
	tGsmPortSendCmd(apn);
	tGsmPortSendCmd("\",\"");
	tGsmPortSendCmd("\",\"");
	tGsmPortSendCmdAndCheckResp("\"\r\n", "OK\r\n", 0);
	
	//Brings up wireless connection
	tGsmPortSendCmdAndCheckResp("AT+CIICR\r\n","OK\r\n",0);
	
	//Get local IP address
	tGsmPortSendCmd("AT+CIFSR\r\n");
	tGsmPortCleanBuffer(ipAddr,32);
	tGsmPortReadData(ipAddr,32,5);
	//Response:
	//AT+CIFSR\r\n       -->  8 + 2
	//\r\n				 -->  0 + 2
	//10.160.57.120\r\n  --> 15 + 2 (max)   : TOTAL: 29
	//Response error:
	//AT+CIFSR\r\n
	//\r\n
	//ERROR\r\n
	if (NULL != strstr(ipAddr,"ERROR")) {
		return false;
	}
	s = ipAddr + 2;
	p = strstr((char *)(s),"\r\n"); //p is last character \r\n
	if (NULL != s) {
		i = 0;
		while (s < p) {
			ip_string[i++] = *(s++);
		}
		ip_string[i] = '\0';
	}
	_ip = strToIP(ip_string);
	if(_ip != 0) {
		return true;
	}
	return false;
}

void disconnect() {
	tGsmPortSendCmd("AT+CIPSHUT\r\n");
}

bool connect(int ptl,const char * host, int port) {
	
	char num[4] = {0};
	char resp[96] = {0};
	
	if(ptl == 0) {
		tGsmPortSendCmd("AT+CIPSTART=\"TCP\",\"");
		tGsmPortSendCmd(host);
		tGsmPortSendCmd("\",");
		itoa(port, num, 10);
		tGsmPortSendCmd(num);
		tGsmPortSendCmd("\r\n");
	} 
	else if(ptl == 1) {
		tGsmPortSendCmd("AT+CIPSTART=\"UDP\",\"");
		tGsmPortSendCmd(host);
		tGsmPortSendCmd("\",");
		itoa(port, num, 10);
		tGsmPortSendCmd(num);
		tGsmPortSendCmd("\r\n");
	} 
	else {
		return false;
	}
	
	tGsmPortReadData(resp, 96, 5);
	if(NULL != strstr(resp,"CONNECT")) {
		return true;
	}
	return false;
}

bool isConnected(void) {
	char resp[96] = {0};
		
	tGsmPortSendCmd("AT+CIPSTATUS\r\n");
	tGsmPortReadData(resp,sizeof(resp),DEFAULTTIMEOUT);
	
	if(NULL != strstr(resp,"CONNECTED")) {
		//+CIPSTATUS: 1,0,"TCP","216.52.233.120","80","CONNECTED"
		return true;
	} 
	else {
		//+CIPSTATUS: 1,0,"TCP","216.52.233.120","80","CLOSED"
		//+CIPSTATUS: 0,,"","","","INITIAL"
		return false;
	}
}

bool close(void) {
	if (!isConnected()) {
		return true;
	}
	return tGsmPortSendCmdAndCheckResp("AT+CIPCLOSE=1\r\n", "CLOSE OK\r\n",0);
}

int send(const char * str, int len) {
	char num[4] = {0};
	
	if(len > 0) {
		tGsmPortSendCmd("AT+CIPSEND=");
		itoa(len, num, 10);
		tGsmPortSendCmd(num);
		if(!tGsmPortSendCmdAndCheckResp("\r\n",">",0)) {
			return 0;
		}
	
		delay_ms(500);
		tGsmPortSendCmd(str);
		delay_ms(500);
		tGsmPortSendEndMark();
		if(!tGsmPortWaitForResponse("SEND OK\r\n", 1, DEFAULTTIMEOUT * 10)) {
			return 0;
		}        
	}
	return len;
}

int recv(char* buf, int len) {
	tGsmPortCleanBuffer(buf,len);
	tGsmPortReadData(buf,len,5);   
	return strlen(buf);
}

uint32_t strToIP(const char* str) {
	uint32_t ip = 0;
	char* p = (char*)str;
	for(int i = 0; i < 4; i++) {
		ip |= atoi(p);
		p = strchr(p, '.');
		if (p == NULL) {
			break;
		}
		ip <<= 8;
		p++;
	}
	return ip;
}

bool getLocation(char *apn,char *longitude, char *latitude) {
	
	int i = 0;
	char gprsBuffer[GSMBUFFER90] = {0};
	char buffer[20] = {0};
	char *s = NULL;
		
	//send AT+SAPBR=3,1,"Contype","GPRS"
	tGsmPortSendCmdAndCheckResp("AT+SAPBR=3,1,\"Contype\",\"GPRS\"\r","OK\r\n",0);
	//serial_write("AT+SAPBR=3,1,APN \n");
	//sen AT+SAPBR=3,1,"APN","GPRS_APN"
	tGsmPortSendCmd("AT+SAPBR=3,1,\"APN\",\"");
	
	//serial_write("AT+SAPBR=3,1,");
	tGsmPortSendCmd(apn);
	
	tGsmPortSendCmdAndCheckResp("\"\r","OK\r\n",0);
	//serial_write("AT+SAPBR=1,1 \n");
	//send AT+SAPBR =1,1
	tGsmPortSendCmdAndCheckResp("AT+SAPBR=1,1\r","OK\r\n",0);
	
	//AT+CIPGSMLOC=1,1
	tGsmPortFlushSerial();
	//serial_write("AT+CIPGSMLOC=1,1 \n");
	tGsmPortSendCmd("AT+CIPGSMLOC=1,1\r");
	tGsmPortCleanBuffer(gprsBuffer,GSMBUFFER90);
	tGsmPortReadDataVaringTime(gprsBuffer,GSMBUFFER90,5*DEFAULTTIMEOUT,15000UL);
	//Serial.println(gprsBuffer);
	//serial_write("Gsm BUffer   :");
	//serial_write(gprsBuffer);
	//serial_write("\r\n");
	if(NULL != ( s = strstr(gprsBuffer,"+CIPGSMLOC:"))) {
		s = strstr((char *)s, ",");
		//s = s+1;
		//Serial.println(*s);
		
	if(NULL != (strstr(gprsBuffer,","))) {
		i=0;
		while(*(++s) !=  ',') {
			buffer[i++]=*s;
		}
		buffer[i] = 0;
		strcpy(longitude,buffer);
		
		i=0;
		while(*(++s) !=  ',') {
			buffer[i++]=*s;
		}
		buffer[i] = 0;
		strcpy(latitude,buffer);
		httpClose();
		delay_ms(1000);
		return true;
		}
		else{
			httpClose();
			return false;
		}
		
	}
	httpClose();
	delay_ms(1000);
	return false;
}

bool httpInit(char *apn) {
	int  i = 0;
	char *p = NULL, *s = NULL;
	char ipAddress[GSMBUFFER40] = {0};

	tGsmPortSendCmdAndCheckResp("AT+SAPBR=3,1,\"Contype\",\"GPRS\"\r\n","OK\r\n",0);
	tGsmPortSendCmd("AT+SAPBR=3,1,\"APN\",\"");
	tGsmPortSendCmd(apn);
	tGsmPortSendCmdAndCheckResp("\"\r\n", "OK\r\n",0);
	
	//Brings up wireless connection
	tGsmPortSendCmdAndCheckResp("AT+SAPBR =1,1\r\n","OK\r\n",0);
	
	//Get local IP address
	tGsmPortSendCmd("AT+SAPBR=2,1\r\n");
	tGsmPortCleanBuffer(ipAddress,GSMBUFFER40);
	tGsmPortReadData(ipAddress,GSMBUFFER40,5);		//+SAPBR: 1,1,"100.120.204.132" OK
	//serial_write(ipAddress);
	//serial_write("\n");
	//Response:
	//+SAPBR:r\n       -->  7 + 2
	//\r\n				 -->  0 + 2
	//10.160.57.120\r\n  --> 15 + 2 (max)   : TOTAL: 29
	//Response error:
	//AT+SAPBR=2,1\r\n
	//\r\n
	//ERROR\r\n
	if (NULL != strstr(ipAddress,"ERROR")) {
		return false;
	}
	if (NULL != (s=strstr(ipAddress,"+SAPBR:"))) {
		s = strstr((char *)(s),"\"")+1;
		p = strstr((char *)(s),"\""); //p is last character \r\n
		if (NULL != s) {
			i = 0;
			while (s < p) {
				ip_string[i++] = *(s++);
			}
			ip_string[i] = '\0';
		}
		//_ip = str_to_ip(ip_string);
	}
	return (isGprsConnectionStarted()?true:false);
}

/***************************************************************************************/
//send data through GET method to specified URL
/***************************************************************************************/
uint16_t httpSend(char * URL_str,char *URL_DATA,int Request_type,unsigned int *Received_File_size,int char_timeout_secs) {
	char gprsBuffer[80] = {0};
	char buffer[5] = {0},buffer_1[5] = {0};
	uint16_t num_buffer = 0;
	char *s = NULL,*p = NULL;
	//AT+HTTPINIT
	//AT+HTTPPARA="CID",1 --->Start by setting up the HTTP bearer profile identifier
	//AT+HTTPPARA="URL","http://thingtronics.in/testout.php" ---->Set the url to the address of the webpage you want to access
	//AT+HTTPACTION=0 ----->Start the HTTP GET session
	//response +HTTPACTION:0,200,654
	
	wdt_reset_count();//reset wdt count
	
	tGsmPortSendCmdAndCheckResp("AT+HTTPINIT\r\n","OK\r\n",0);
	tGsmPortSendCmdAndCheckResp("AT+HTTPPARA=\"CID\",1\r\n","OK\r\n",0);
	tGsmPortSendCmd("AT+HTTPPARA=\"URL\",\"");
	tGsmPortSendCmd(URL_str);
	tGsmPortSendCmdAndCheckResp("\"\r\n","OK\r\n",0);
	
	if (Request_type == 1) {
		tGsmPortSendCmd("AT+HTTPPARA=\"CONTENT\",\"application/json\"");
		tGsmPortSendCmdAndCheckResp("\r\n","OK\r\n",0);
		tGsmPortSendCmd("AT+HTTPDATA=");
		num_buffer = strlen(URL_DATA);
		itoa(num_buffer,buffer,10);
		tGsmPortSendCmd(buffer);
		tGsmPortSendCmd(",");
		tGsmPortSendCmd("10000");
		tGsmPortSendCmdAndCheckResp("\r\n", "DOWNLOAD\r\n",0);
		tGsmPortSendCmd(URL_DATA);
		tGsmPortSendCmdAndCheckResp("\"\r\n", "OK\r\n",0);
	}
	
	if (Request_type == 1) {
		tGsmPortSendCmd("AT+HTTPACTION=1\r\n");
	}
	else {
		tGsmPortSendCmd("AT+HTTPACTION=0\r\n");
	}
	tGsmPortCleanBuffer(gprsBuffer,sizeof(gprsBuffer));
	tGsmPortReadDataVaringTime(gprsBuffer,sizeof(gprsBuffer),35,char_timeout_secs*1000UL);
	num_buffer = 0;
	memset(buffer,0,5);
	if(NULL != ( s = strstr(gprsBuffer,"+HTTPACTION:"))) {			//+HTTPACTION:0,200,654
		char *st = NULL,*en = NULL;
		s = strstr((char *)s, ",");
		s = s + 1;
		p = strstr((char *)s, ",");
		st = p + 1;
		en = strstr((char *)p, "\r\n");
		if (extract_string_using_adds(s,p,buffer))
		{
			if (extract_string_using_adds(st,en,buffer_1))
			{
				*Received_File_size = atoi(buffer_1);
				num_buffer = atoi(buffer);
				return num_buffer;
			}
			else
				return 0;		
		}
		else
			return 0;
	
	}
	else
		return 0;

}	

/**************************************************************************/
//terminate http connection
void httpClose(void) {
	
	tGsmPortSendCmdAndCheckResp("AT+HTTPTERM\r\n","OK\r\n",0);
	delay_ms(2000);
	tGsmPortSendCmdAndCheckResp("AT+SAPBR=0,1\r\n","OK\r\n",0);
	delay_ms(2000);
	
}

#ifdef ENABLE_OTA
	int startOTAUpdate(uint32_t filesize,char *server_binFile_checksum){
		char gprsBuffer[OTA_BUFFER_SIZE] = {0};
		uint32_t j = 0;
		uint32_t File_size = 0;
		char j_ch[20]= {'\0'};
		char buff[100]= {'\0'};
		uint32_t curr_prog_addr;
		curr_prog_addr = APP_ADDR;
#if CRC32_ENABLED
	    unsigned long current_crc32 = 0;
		unsigned long flash_CRC = 0;
#endif	
	
	
		for(j = 0; j < filesize; j = j + BIN_DATA_SIZE) {
			wdt_reset_count();							//reset wdt count
			itoa1(j,j_ch);
			serial_debug("START ADDS:",STR,OTA_DEBUG_LEVEL);
			serial_debug_ln(j,DEC,OTA_DEBUG_LEVEL);
			
			tGsmPortCleanBuffer(buff,sizeof(buff));
			sprintf(buff,"AT+HTTPREAD=%s,%ld\r\n", j_ch, min(BIN_DATA_SIZE, (filesize - j)));
			serial_debug(buff,STR,OTA_DEBUG_LEVEL);
			tGsmPortSendCmd(buff);			
			tGsmPortCleanBuffer(gprsBuffer,sizeof(gprsBuffer));
			tGsmPortReadDataVaringTime(gprsBuffer, sizeof(gprsBuffer), DEFAULTTIMEOUT * 3, 2000);
			uint8_t gprsBuffer_flash[BIN_DATA_SIZE] = {0};
			int stAdds = 0, enAdds = 0, flag = 0;
			
			tGsmPortCleanBuffer(buff,sizeof(buff));
			sprintf(buff,"+HTTPREAD: %ld\r\n", min(BIN_DATA_SIZE, (filesize - j)));
			serial_debug(buff,STR,OTA_DEBUG_LEVEL);
			
			if(search_char_in_binary(buff, gprsBuffer, (filesize - j), &stAdds, 0, 0)) {
				if (search_char_in_binary("\r\nOK\r\n", gprsBuffer, (filesize - j), &enAdds, 1, stAdds)) {
					flag = 1;
				}
			}
		
			if (flag == 1) {
								
				uint16_t a = 0;
				for (int m = stAdds; m < enAdds; m++) {
					gprsBuffer_flash[a] = gprsBuffer[m];
					File_size++;
					a++;
				}
				uint8_t temp_c = 0;
				serial_debug("Received BYTES:",STR,OTA_DEBUG_LEVEL);
				serial_debug_ln(a,DEC,OTA_DEBUG_LEVEL);
#if CRC32_ENABLED				
				/*current_crc32 = Crc32_ComputeBuf(current_crc32,gprsBuffer_flash,a);
				serial_write("CRC32 FROM GSM:");
				serial_write_int_ln(current_crc32);	*/			
#endif				
				while(!program_memory(curr_prog_addr,gprsBuffer_flash,a)){
					if (temp_c > 3) {
						return OTA_FLASH_ERROR;
						break;
					}
					delay_ms(500);
					temp_c++;
				}
#if CRC32_ENABLED
				//read written data from flash and calculate crc32B				
				calculate_crc32b_from_flash(curr_prog_addr,&flash_CRC,a);
#endif				
				curr_prog_addr = curr_prog_addr + a;
			}
		}
		serial_write("ActualFileSize =");
		serial_write_int_ln(filesize);
		serial_write("WrittenFileSize =");
		serial_write_int_ln(File_size);
		
#if CRC32_ENABLED
		char string_crc32b_flash[10] = {0};
		//print crc32b code in hex format
		sprintf(string_crc32b_flash,"%08lx",flash_CRC);
		serial_write("CRC32B of bin file: ");
		serial_write_ln(server_binFile_checksum);		
		serial_write("CRC32B of flash   : ");
		serial_write_ln(string_crc32b_flash);	
		if (!strcmp(string_crc32b_flash,server_binFile_checksum)) {
			return OTA_UPDATE_SUCCESS;
		}
		else
			return OTA_FILE_CORRUPT_ERROR;	
#else			
		if (File_size == filesize) {
			return OTA_UPDATE_SUCCESS;
		}
		else
			return OTA_FILE_CORRUPT_ERROR;
#endif
			
	}

	bool startGprsConnection(char *apn) {
		
		char *p = NULL, *s = NULL;
		char ip_string_local[32] = {0};
		tGsmPortSendCmd("AT+CSTT=\"");
		tGsmPortSendCmd(apn);
		tGsmPortSendCmd("\",\" \",\" ");
		tGsmPortSendCmdAndCheckResp("\"\r\n", "OK\r\n",0);
		serial_write("AT+CSTT=\"");
		serial_write_ln(apn);
	
		//Brings up wireless connection
		tGsmPortSendCmdAndCheckResp("AT+CIICR\r\n","OK\r\n",0);
		tGsmPortSendCmd("AT+CIFSR\r\n");
		tGsmPortCleanBuffer(ipAddr,sizeof(ipAddr));
		tGsmPortReadDataVaringTime(ipAddr,sizeof(ipAddr),DEFAULTTIMEOUT,5000UL);
		serial_write_ln("AT+CIICR");
		serial_write("AT+CIFSR:");
		serial_write_ln(ipAddr);
			//Response:
			//AT+CIFSR\r\n       -->  8 + 2
			//\r\n				 -->  0 + 2
			//10.160.57.120\r\n  --> 15 + 2 (max)   : TOTAL: 29
			//Response error:
			//AT+CIFSR\r\n
			//\r\n
			//ERROR\r\n
		if (NULL != strstr(ipAddr,"ERROR")) {
			return false;
		}
		int i = 0;
		 s = ipAddr + 12;
		 p = strstr((char *)(s),"\r\n"); //p is last character \r\n
		 if ((NULL != s)&&(NULL != p)) {
			 i = 0;
			 while (s < p) {
				 ip_string_local[i++] = *(s++);
			 }
			 ip_string_local[i] = '\0';
			 serial_write_ln(ip_string_local);
		 }
		 else {
			 return false;
		 }
		 return true;
	}
	
	bool startTCPconnection(char *hostName,unsigned int port) {
		char buffer[50] ={0};
		char num[8] = {0};
		serial_write_ln(hostName);
		tGsmPortSendCmd("AT+CIPSTART=\"TCP\",\"");
		tGsmPortSendCmd(hostName);	
		tGsmPortSendCmd("\",");
		itoa(port,num,10);
		tGsmPortSendCmd(num);
		tGsmPortSendCmd("\r\n");
		tGsmPortReadDataVaringTime(buffer,sizeof(buffer),DEFAULTTIMEOUT * 2,TCP_URL_GET_TIMEOUT * 1000UL);
		if(NULL != strstr(buffer,"CONNECT")) { //ALREADY CONNECT or CONNECT OK
			return true;
		}
		return false;
	}

	bool sendData(char *Data,char *recvedData,char *server_checksum) {
		char num[4] = {0};
		char buffer[FIRMWARE_UPDATE_CHECK_BUFFER] = {0};
		char *s = NULL,*p = NULL;
		
		//feed wdt
		wdt_reset_count();
		
		serial_write_ln(Data);
		serial_write_int_ln(strlen(Data));
		tGsmPortSendCmd("AT+CIPSEND=");
		itoa(strlen(Data), num, 10);
		tGsmPortSendCmd(num);
		if (!tGsmPortSendCmdAndCheckResp("\r\n",">",0)) {
			return false;
		}
		tGsmPortSendCmd(Data);
		delay_ms(500);
		tGsmPortSendEndMark();
		tGsmPortReadDataVaringTime(buffer,sizeof(buffer),DEFAULTTIMEOUT*3,10000UL);
		serial_write("sent data buffer:");
		serial_write_ln(buffer);
		
		if (NULL != (s=strstr(buffer,"firmware\":"))) {
			s = s + strlen("firmware\":");
			s = strstr((char *)(s),"\"");
			s = s+1;
			p = strstr((char *)(s),"\"");
	
			if (extract_string_using_adds(s,p,recvedData))
			{
		
#if CRC32_ENABLED		
				if (NULL != (s=strstr(buffer,"checksum\":"))) {
					s = s + strlen("checksum\":");
					s = strstr((char *)(s),"\"");
					s = s+1;
					p = strstr((char *)(s),"\"");
					return extract_string_using_adds(s,p,server_checksum);
				}
				else{
					serial_write_ln("checksum\": not found");
					return false;
				}
	
#else
				return true;
				#endif
		
			}
			else
			return false;	
		}
		else {
			serial_write_ln("firmware not found");
			return false;
		}
	}

	void TCPclose(void) {
		tGsmPortSendCmdAndCheckResp("AT+CIPCLOSE\r\n","CLOSE OK\r\n",0);
		tGsmPortSendCmdAndCheckResp("AT+CIPSHUT\r\n","OK\r\n",0);
	}

	bool program_memory(uint32_t address, uint8_t *buffer, uint16_t len) {
		
		struct nvm_config config;
		nvm_get_config_defaults(&config);
		config.manual_page_write = false;
		nvm_set_config(&config);
		
		if (len <= 0) {
			serial_write_ln("data len <= 0");
			return true;
		}
			
		/* Check if length is greater than Flash page size */
		if (len > NVMCTRL_PAGE_SIZE) {
			uint32_t offset = 0;
			while (len > NVMCTRL_PAGE_SIZE) {
				/* Check if it is first page of a row */
				if ((address & 0xFF) == 0) {
					/* Erase row */
					nvm_erase_row(address);
				}
				/* Write one page data to flash */
				nvm_write_buffer(address, buffer + offset, NVMCTRL_PAGE_SIZE);
				/* Increment the address to be programmed */
				address += NVMCTRL_PAGE_SIZE;
				/* Increment the offset of the buffer containing data */
				offset += NVMCTRL_PAGE_SIZE;
				/* Decrement the length */
				len -= NVMCTRL_PAGE_SIZE;
			}
			/* Check if there is data remaining to be programmed*/
			if (len > 0) {
				/* Write the data to flash */
				if(STATUS_OK != nvm_write_buffer(address, buffer + offset, len)) {
				serial_write_ln("error! writing into flash_1");
				return false;
				}
			}
			else
				return true;
		}
		else {
				/* Check if it is first page of a row) */
				if ((address & 0xFF) == 0){
					/* Erase row */
					nvm_erase_row(address);
				}
				/* Write the data to flash */
				if(STATUS_OK != nvm_write_buffer(address, buffer, len)) {
					serial_write_ln("error! writing into flash_2");
					return false;
				}
			}
		return true;
		}
		
	bool calculate_crc32b_from_flash(uint32_t start_address,unsigned long *flash_CRC,uint16_t len_flash_read) {
		struct nvm_config config;
		nvm_get_config_defaults(&config);
		config.manual_page_write = false;
		nvm_set_config(&config);
		uint16_t buf_st_len = len_flash_read;
		uint8_t buffer_local[1024];
		if (len_flash_read <= 0) {
			return true;
		}
		
		/* Check if length is greater than Flash page size */
		if (len_flash_read > NVMCTRL_PAGE_SIZE) {
			uint32_t offset = 0;
			while (len_flash_read > NVMCTRL_PAGE_SIZE) {
				/* Write one page data to flash */
				nvm_read_buffer(start_address, buffer_local + offset, NVMCTRL_PAGE_SIZE);
//status_code nvm_read_buffer(address,buffer + offset,NVMCTRL_PAGE_SIZE)				
				/* Increment the address to be programmed */
				start_address += NVMCTRL_PAGE_SIZE;
				/* Increment the offset of the buffer containing data */
				offset += NVMCTRL_PAGE_SIZE;
				/* Decrement the length */
				len_flash_read -= NVMCTRL_PAGE_SIZE;
			}
			/* Check if there is data remaining to be programmed*/
			if (len_flash_read > 0) {
				/* Write the data to flash */
				if(STATUS_OK != nvm_read_buffer(start_address, buffer_local + offset, len_flash_read)) {
					serial_write_ln("error! reading from flash_1");
					return false;
				}
			}
			else{
#if CRC32_ENABLED
				*flash_CRC = Crc32_ComputeBuf(*flash_CRC,buffer_local,buf_st_len);
				serial_write_hex_ln(*flash_CRC);
#endif
				return true;
			}
				
		}
		else {
 
			/* Write the data to flash */
			if(STATUS_OK != nvm_read_buffer(start_address, buffer_local, len_flash_read)) {
				serial_write_ln("error! reading from flash_2");
				return false;
			}
		}
#if CRC32_ENABLED
		*flash_CRC = Crc32_ComputeBuf(*flash_CRC,buffer_local,buf_st_len);
		serial_write("CRC32: ");
		serial_write_hex_ln(*flash_CRC);
#endif
		return true;
	}		
#endif

bool sendHttpData(char * URL_str,char *URL_DATA,int Request_type,int sending_type,unsigned int *Received_File_size,int char_timeout_secs) {
	
	int retry_count = 0;
	while(retry_count < 3) {
		int http_response = httpSend(URL_str,URL_DATA,Request_type, &Received_File_size,char_timeout_secs);
		if (HTTPSENDSUCCESS == http_response){
			return true;
			break;
		}
		else{
			serial_write("DataSending Failed!,response: ");
			serial_write_int_ln(http_response);
			if(retry_count >=2) {
				send_failure_sms(sending_type,http_response);
				
				if (http_response == 603){	
					serial_write_ln("switching down the gsm");
					delay_ms(3000);
					gsmPowerUpDown(GSM_PWR_KEY,REBOOT);
				}
			
				return false;
			}
		}
		delay_ms(2000);
		retry_count++;
	}

}

bool search_char_in_binary(char *SearchChar,char *BinaryString,unsigned long int EFilesize,int *index_foundAt,int stEnFlag,int starting_flag) {
	int i = 0, st_c = 0, flag = 0;
	int count = strlen(SearchChar);
	for (i = 0; i <= OTA_BUFFER_SIZE - count; i++) {
		for (st_c = i; st_c < i + count; st_c++) {
			flag = 1;
			if (BinaryString[st_c] != SearchChar[st_c - i]) {
				flag = 0;
				break;
			}
		}
		if (flag == 1) {
			if (stEnFlag == 0) {
				if ((i <= 20)) {
					break;
				}
			}
			else if (stEnFlag == 1) {
				if ((i >= (BIN_DATA_SIZE + starting_flag)) || (EFilesize <= BIN_DATA_SIZE)) {
					break;
				}
			}
		}
	}
	if (flag) {
		if (!stEnFlag) {
			*index_foundAt = i + count;
		}
		else {
			*index_foundAt = i;
		}
	}
	else {
		*index_foundAt = 0;
	}
	
	return flag;
}


void send_failure_sms(int sending_type,int response){
	
#if SEND_ERROR_SMS	
	char failure_sms[100] ={0};
	char timestamp[TIMEBUFFERSIZE] = {0};
	tRTCGetTimeStamp(timestamp,POST);
	
	switch (sending_type)
	{
		case BOOTUPDATA:
			sprintf(failure_sms,"BOOTUPDATA FAILED!,CODE:%d,time:%s",response,timestamp);
			serial_write_ln(failure_sms);
			sendSmsWithDeviceID(gatewaySendNumber,failure_sms);
			break;
		case LIVEDATA:
			sprintf(failure_sms,"LIVEDATA FAILED!,CODE:%d,time:%s",response,timestamp);
			sendSmsWithDeviceID(gatewaySendNumber,failure_sms);
			serial_write_ln(failure_sms);
			break;	
		default:
			serial_write_ln("invalid failure response!");
			break;
	}
#endif
}

bool isSimRegistered(void){
	int  networkType = 0;
	getSIMRegistration(&networkType);
	
	if ( networkType == LOCALSIM || networkType == ROAMINGSIM) {
		serial_write_ln("SIM is registered");
		
		if(networkType == LOCALSIM ) {
			serial_write_ln("NETWORK type is LOCAL (1)");
		}
		else if(networkType == ROAMINGSIM) {
			serial_write_ln("NETWORK type is ROAMING (5)");
		}
		return true;
	}
	else {
		serial_write_ln("SIM is not registered");
		return false;
	}
}

bool isGprsConnectionStarted(void){
		//AT+CGATT?						--> check gprs connection established or not
		//+CGATT:<1 or 0>				--> 1 for GPRS conn attached,0 for not attached
		
		char gprsBuffer[GSMBUFFER30] = {0};
		char *p = NULL;
		char *s = NULL;
		char buffers[4] = {0};
		int cgatt_status = 0;
		tGsmPortFlushSerial();
		tGsmPortSendCmd("AT+CGATT?\r\n");
		tGsmPortCleanBuffer(gprsBuffer, GSMBUFFER30);
		tGsmPortReadData(gprsBuffer, GSMBUFFER30, DEFAULTTIMEOUT);
		if (NULL != (s = strstr(gprsBuffer, "+CGATT: "))) {
			s  = strstr((char *)(s), ":");
			s = s + 2;  //We are in the first phone number character
			p = strstr((char *)(s), "\r\n"); //p is last character """
			//serial_write_ln("inside +CGATT");
			if (extract_string_using_adds(s,p,buffers))
			{
				cgatt_status = atoi(buffers);
			}						
		}
		return (cgatt_status?true:false);
}
bool extract_string_using_adds(char *start_adds,char *end_adds,char *extracted_string){
	if ((NULL != start_adds)&&(NULL != end_adds)) {
		int i = 0;
		while (start_adds < end_adds) {
			extracted_string[i++] = *(start_adds++);
		}
		extracted_string[i] = '\0';
		return true;
	}
	else
		serial_write_ln("start and end adresses are NULL");
		return false;

}

int checkIfNumberAuthorized(char *mobileNumber){
	char *startPointer = NULL,*endPointer=NULL,index[4]={0},gprsBuffer[GSMBUFFER60]={0};
	uint8_t i=0;
	
	tGsmPortFlushSerial();
	delay_ms(20);
	tGsmPortSendCmd("AT+CPBF=\"");
	tGsmPortSendCmd(mobileNumber);
	tGsmPortSendCmd("\"\r\n");
	tGsmPortCleanBuffer(gprsBuffer,GSMBUFFER60);
	tGsmPortReadData(gprsBuffer,GSMBUFFER60,DEFAULTTIMEOUT);
	 if(NULL != ( startPointer = strstr(gprsBuffer,"+CPBF:"))){
		 startPointer += 7;
		 endPointer = strstr((char *)(startPointer), ",");
		 
		 if((NULL != startPointer) && (NULL != endPointer)){
			i = 0;
			while (startPointer < endPointer) {
				index[i++] = *(startPointer++);
			}
			index[i] = '\0';
			
			serial_write("     (given number ");
			serial_write(mobileNumber);
			serial_write_ln(" is authorized )");
			return atoi(index);
		 }
		 serial_write("     (given number ");
		 serial_write(mobileNumber);
		 serial_write_ln(" is not authorized )");
		 return NOT_AUTHORIZED;
		 
	 }
	 else {
		 serial_write("     (given number ");
		 serial_write(mobileNumber);
		 serial_write_ln(" is not authorized )");
		 return NOT_AUTHORIZED;
		 
	 }
	
}

bool isMobileNumberValid(char *msg , uint8_t startPoint, uint8_t endPoint){
	int i = 0, count = 0;
	for (i=startPoint;i<=endPoint;i++){
		
	}
}


/*
	bool sendData(char *Data,char *recvedData,char *server_checksum) {
		char num[4] = {0};
		char buffer[FIRMWARE_UPDATE_CHECK_BUFFER] = {0};
		char *s = NULL,*p = NULL;
		serial_write_ln(Data);
		serial_write_int_ln(strlen(Data));
		tGsmPortSendCmd("AT+CIPSEND=");
		itoa(strlen(Data), num, 10);
		//serial_write("num:");
		//serial_write_ln(num);
		tGsmPortSendCmd(num);
		if (!tGsmPortSendCmdAndCheckResp("\r\n",">",0)) {
			return false;
		}
		tGsmPortSendCmd(Data);
		delay_ms(500);
		tGsmPortSendEndMark();
		tGsmPortReadDataVaringTime(buffer,sizeof(buffer),DEFAULTTIMEOUT*3,10000UL);
		serial_write("sent data buffer:");
		serial_write_ln(buffer);
		
		if (NULL != (s=strstr(buffer,"firmware\":"))) {
			s = s + strlen("firmware\":");
			s = strstr((char *)(s),"\"");
			s = s+1;
			p = strstr((char *)(s),"\"");
			
			if (extract_string_using_adds(s,p,recvedData))
			{
				
				#if CRC32_ENABLED
				
				if (NULL != (s=strstr(buffer,"checksum\":"))) {
					s = s + strlen("checksum\":");
					s = strstr((char *)(s),"\"");
					s = s+1;
					p = strstr((char *)(s),"\"");
					return extract_string_using_adds(s,p,server_checksum);
				}
				else{
					serial_write_ln("checksum\": not found");
					return false;
				}
				
				
				#else
				return true;
				#endif
				
			}
			else
			return false;
			
			
		}
		else {
			serial_write_ln("firmware not found");
			return false;
		}
	}
*/