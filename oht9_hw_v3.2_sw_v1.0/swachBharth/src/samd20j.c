//#include "main.h"
#include "samd20j.h"



bool EEPROM_write(uint16_t offset, uint16_t length, uint8_t *data) {
	enum status_code error_code;
	error_code = eeprom_emulator_write_buffer(offset, data, length);
	eeprom_emulator_commit_page_buffer();
	return (error_code == STATUS_OK) ? true : false ;
}

bool EEPROM_read_num(uint16_t offset,uint8_t *data) {
	uint8_t buf[8];
	enum status_code error_code;
	error_code = eeprom_emulator_read_buffer(offset, buf, 2);
	*data = atoi(buf);
	return (error_code == STATUS_OK) ? true : false ;
}

bool EEPROM_read_char(uint16_t offset,uint16_t length,uint8_t *data) {
	enum status_code error_code;
	error_code =  eeprom_emulator_read_buffer(offset, data, length);
	return (error_code == STATUS_OK) ? true : false ;
}

bool EEPROM_clear(uint16_t offset,uint16_t length) {
	enum status_code error_code;
	const char data[2] = " ";
	for (int loopCount = 0; loopCount < length; loopCount++) {
		error_code = eeprom_emulator_write_buffer(offset, data, strlen(data));	//#CHANGES_3P1
		//eeprom_emulator_commit_page_buffer();
		if(error_code != STATUS_OK) {
			return false;	
		}
		offset++;
	}
	return (error_code == STATUS_OK) ? true : false;
}



void wdt_reset(void) {
	/* Create a new configuration structure for the Watchdog settings and fill
	* with the default module settings. */
	//! [setup_1]
	struct wdt_conf config_wdt;
	//! [setup_1]
	//! [setup_2]
	wdt_get_config_defaults(&config_wdt);
	//! [setup_2]

	/* Set the Watchdog configuration settings */
	//! [setup_3]
	config_wdt.always_on      = false;
#if !((SAML21) || (SAMC21) || (SAML22) || (SAMR30))
	config_wdt.clock_source   = GCLK_GENERATOR_4;
#endif
	
	config_wdt.timeout_period       = WDT_PERIOD_8CLK;
	//! [setup_3]

	/* Initialize and enable the Watchdog with the user settings */
	//! [setup_4]
	wdt_set_config(&config_wdt);
	while(1);
}


/* This Function will Get Current Time From RTC,
 * and Format it according to the required form
 * example:  2017-08-02%2007:17:12 for GET request
 * example:  2017-08-02 07:17:12 for POST request
 */
void tRTCGetTimeStamp(char *timestamp,int r_type) {
	rtc_calendar_get_time(&rtc_instance, &time);
	if (r_type) {
		//post request
		sprintf(timestamp,"%d-%02d-%02d %02d:%02d:%02d",time.year,time.month,time.day,time.hour,time.minute,time.second);
	}
	else
		sprintf(timestamp,"%d-%02d-%02d%%20%02d:%02d:%02d",time.year,time.month,time.day,time.hour,time.minute,time.second);
	
	serial_write("Timestamp :");
	serial_write_ln(timestamp);
}