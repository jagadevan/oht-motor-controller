/*
 * adc_drivers.h
 *
 * Created: 1/10/2018 12:58:00 PM
 *  Author: bomma
 */ 


#ifndef ADC_DRIVERS_H_
#define ADC_DRIVERS_H_

#include "asf.h"
#define ADC_SERIAL_DEBUG		1
#if ADC_SERIAL_DEBUG
	#include "uart_drivers.h"
#endif
// ADC module structures
struct adc_module adc_instance_0;
struct adc_module adc_instance_1;
struct adc_module adc_instance_4;
struct adc_module adc_instance_5;
struct adc_module adc_instance_6;
struct adc_module adc_instance_7;
struct adc_module adc_instance_16;
struct adc_module adc_instance_17;
struct adc_module adc_instance_18;
struct adc_module adc_instance_19;
struct   adc_module adc_instance;


void		adc_init_setup(int pin);
uint16_t	adc_read_value(int pin);

void		adc_callback_setup(void);
void		adc_read_complete(struct adc_module *const module);

#endif /* ADC_DRIVERS_H_ */