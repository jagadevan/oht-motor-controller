

//#include "main.h"
#include "GsmPortSetup.h"

struct usart_module usart_instance_sim900;

/*This Function is Used To Configure Uart , so that Gsm can be Connected to that Port
 * Baud Rate	: 9600
 * RX pin		: PA16
 * TX pin       : PA17 
 */ 

void  tGsmPortInit(void) {
	struct usart_config config_usart_sim900;
	usart_get_config_defaults(&config_usart_sim900);
	config_usart_sim900.baudrate    = GSMBAUDRATE;
	config_usart_sim900.mux_setting = USART_RX_1_TX_0_XCK_1;
	config_usart_sim900.pinmux_pad0 = PINMUX_PA16C_SERCOM1_PAD0 ;
	config_usart_sim900.pinmux_pad1 = PINMUX_PA17C_SERCOM1_PAD1 ;
	config_usart_sim900.pinmux_pad2 = PINMUX_UNUSED;
	config_usart_sim900.pinmux_pad3 = PINMUX_UNUSED;
	
	while (usart_init(&usart_instance_sim900,
	SERCOM1, &config_usart_sim900) != STATUS_OK) {
	}
	usart_enable(&usart_instance_sim900);
}

/* This Function Is Used To Check if Any Data is available in Gsm Uart */
bool  tGsmPortCheckDataAvailable(void) {
	return (usart_read_available(&usart_instance_sim900) == STATUS_OK) ? true : false ;
}

/* This Function is Used To Clean the Buffer with Specified No of Bytes */
void  tGsmPortCleanBuffer(char* buffer, int numberOfBytes) {
	memset(buffer,'\0',numberOfBytes);
}

/* This Function Will Send Command To GSM Module
 * Function parameters:
 * 	 1. UART instance
 * 	 2. Data to be Sent to GSM module
 * 	 3. Length of the Data to be Sent
 */ 
bool  tGsmPortSendCmd(const char *cmd) {
	#if GSM_DBG_LEVEL <= DEBUG_LEVEL
		serial_debug(cmd,STR,GSM_DBG_LEVEL);
	#endif
	return (usart_write_buffer_wait(&usart_instance_sim900, cmd,strlen(cmd))) ;
}

/*This Function is Send One Character Data to gsm Module*/
void  tGsmPortSendChar(char c) {

	while (usart_write_wait(&usart_instance_sim900, c) != STATUS_OK);
}

/*This Function Is used To Indicate That data Transmission is done */
void  tGsmPortSendEndMark(void) {
	tGsmPortSendByte(0X1A);
}

/* This Function reads Incoming One Char data From Gsm */
uint16_t tGsmPortReadchar(void) {
	uint16_t temp;
	usart_read_job(&usart_instance_sim900, &temp);
	return temp;
}

/* This Function is Used to Send OneByte of data to Gsm */
void  tGsmPortSendByte(uint8_t data) {
	while (usart_write_wait(&usart_instance_sim900, data) != STATUS_OK);
}

/* This Function waits until previous data transaction is finished */
void  tGsmPortFlushSerial(void) {
	char dummyVariable;
	while (tGsmPortCheckDataAvailable()) {
		//usart_read_wait(&usart_instance_sim900, &dummyVariable);
		usart_read_job(&usart_instance_sim900, &dummyVariable);
	}
}

/* This Function is Used To read data from the Gsm
 * Parameters are: 
 * 	1)Buffer                : in this variable Output is saved
 *  2)number of bytes       : Number of Bytes to be read from Gsm
 *  3) Total Operation time : this indicates Total time of Operation  
 */
void  tGsmPortReadData(char* buffer,int numberOfBytes,unsigned int totalOperationTime) {
	int noOfCharaterRead 			   = 0;
	unsigned long OpertionStartTime    = 0;
	unsigned long prevCharReadTime     = 0;
	//unsigned long characterTimeout	   = DEFAULTCHARTIMEOUT;
	OpertionStartTime 				   = xTaskGetTickCountFromISR();
	while(true) {
		while (tGsmPortCheckDataAvailable()) {
			char c						 = tGsmPortReadchar();
			prevCharReadTime 			 = xTaskGetTickCountFromISR();
			buffer[noOfCharaterRead++] 	 = c;
			if(noOfCharaterRead >= numberOfBytes) {
				break;
			}
		}
		
		if (noOfCharaterRead >= numberOfBytes) {
			break;
		}
		
		if ((unsigned long)(xTaskGetTickCountFromISR() - OpertionStartTime) > totalOperationTime * 1000UL) {
			break;
		}
		
		if (((unsigned long)(xTaskGetTickCountFromISR() - prevCharReadTime) > DEFAULTCHARTIMEOUT ) && (prevCharReadTime != 0)) {
			break;
		}
	}
	buffer[noOfCharaterRead]='\0';
	#if GSM_DBG_LEVEL <= DEBUG_LEVEL
	serial_debug(buffer,STR,GSM_DBG_LEVEL);
	#endif
}

/* This Function is used to Read the Response for previously executed Command ,
 * Parameters  :
 * 	1) Expected response for executed command 
 *  2) type indicates it is for Data or Cmd (here we use only Cmd )
 *  3) Total time : this indicates total time expected for reading operation 
 */
bool tGsmPortWaitForResponse(const char* expectedResponse, int type, unsigned int totalOperationTime) {
	int newResponseLength 				= 0;
	unsigned long OpertionStartTime		= 0;
	unsigned long prevCharReadTime		= 0;
	int expectedResponseLength 			= strlen(expectedResponse);
	OpertionStartTime					= xTaskGetTickCountFromISR();
	//unsigned int characterTimeout    	= DEFAULTCHARTIMEOUT;
	while(true) {
		if(tGsmPortCheckDataAvailable()) {
			char c              =  tGsmPortReadchar();
			prevCharReadTime  	=  xTaskGetTickCountFromISR();
			newResponseLength  	=  (c == expectedResponse[newResponseLength])? newResponseLength + 1 : 0;
			if(newResponseLength == expectedResponseLength) {
				break;
			}
		}
		if ((unsigned long) (xTaskGetTickCountFromISR() - OpertionStartTime) > totalOperationTime * 1000UL) {
			return false;
		}
		
		if (((unsigned long) (xTaskGetTickCountFromISR() - prevCharReadTime) > DEFAULTCHARTIMEOUT) && (prevCharReadTime != 0)) {
			return false;
		}
	}
	
	if(type == 0) 
		tGsmPortFlushSerial();
	
	return true;
}

/* This Function is will Send the Command to Gsm And Compare Output with expected response
 * if both are same then return true else false
 */

bool tGsmPortSendCmdAndCheckResp(const char* cmd, const char *resp,int type) {
	tGsmPortSendCmd(cmd);
	//unsigned int expectedOperationTimeout = DEFAULTTIMEOUT;
	return tGsmPortWaitForResponse(resp,type,DEFAULTTIMEOUT);
}

/* This Function is used to Read the Response for previously executed Command ,
 * Parameters  :
 * 	1) Expected response for executed command 
 *  2) type indicates it is for Data or Cmd (here we use only Cmd )
 *  3) Total time : this indicates total time expected for reading operation 
 *  4) Character time: this indicates timeout Between two character
 */
void  tGsmPortReadDataVaringTime(char* buffer,int numberOfBytes,unsigned int totalOperationTime,unsigned int totalCharTimeout) {
	int noOfCharaterRead 			   = 0;
	unsigned long OpertionStartTime    = 0;
	unsigned long prevCharReadTime     = 0;
	unsigned long characterTimeout	   = totalCharTimeout;
	OpertionStartTime 				   = xTaskGetTickCountFromISR();
	while(true) {
		while (tGsmPortCheckDataAvailable()) {
			char c			 = tGsmPortReadchar();
			prevCharReadTime = xTaskGetTickCountFromISR();
			buffer[noOfCharaterRead++] 	 = c;
			if(noOfCharaterRead >= numberOfBytes) {
				break;
			}
		}
		if (noOfCharaterRead >= numberOfBytes) {
			break;
		}
		
		if ((unsigned long)(xTaskGetTickCountFromISR() - OpertionStartTime) > totalOperationTime * 1000UL) {
			break;
		}

		if (((unsigned long)(xTaskGetTickCountFromISR() - prevCharReadTime) > characterTimeout) && (prevCharReadTime != 0)) {
			break;
		}
	}
	buffer[noOfCharaterRead]='\0';
	#if GSM_DBG_LEVEL <= DEBUG_LEVEL
	serial_debug(buffer,STR,GSM_DBG_LEVEL);
	#endif
}

