/*
 * adc_drivers.c
 *
 * Created: 1/10/2018 12:58:31 PM
 *  Author: bomma
 */ 

#include "adc_drivers.h"




	
	struct adc_config config_adc_0;		//PA02
	struct adc_config config_adc_1;		//PA03
	struct adc_config config_adc_4;		//PA04
	struct adc_config config_adc_5;		//PA05
	struct adc_config config_adc_6;		//PA06
	struct adc_config config_adc_7;		//PA07
	struct adc_config config_adc_16;	//PA08
	struct adc_config config_adc_17;	//PA09
	struct adc_config config_adc_18;	//PA10
	struct adc_config config_adc_19;	//PA11
void adc_init_setup(int pin)
{

	
	
	switch(pin)
	{
		case ADC_POSITIVE_INPUT_PIN0:
			adc_get_config_defaults(&config_adc_0);
			//SERIAL_DEBUG("entered adc 0 channel");
			config_adc_0.positive_input        = ADC_POSITIVE_INPUT_PIN0 ;
			adc_init(&adc_instance_0, ADC, &config_adc_0);
			adc_enable(&adc_instance_0);
			break;
		case ADC_POSITIVE_INPUT_PIN1:
			adc_get_config_defaults(&config_adc_1);
			//SERIAL_DEBUG("entered adc1 channel");
			config_adc_1.pin_scan.offset_start_scan    = ADC_POSITIVE_INPUT_PIN1;
			config_adc_1.pin_scan.inputs_to_scan       = 1;
			config_adc_1.positive_input        = ADC_POSITIVE_INPUT_PIN1 ;
			adc_init(&adc_instance_1, ADC, &config_adc_1);
			adc_enable(&adc_instance_1);
			break;
		case ADC_POSITIVE_INPUT_PIN4:
			adc_get_config_defaults(&config_adc_4);
			config_adc_4.positive_input        = ADC_POSITIVE_INPUT_PIN4 ;
			adc_init(&adc_instance_4, ADC, &config_adc_4);
			adc_enable(&adc_instance_4);
			break;
		case ADC_POSITIVE_INPUT_PIN5:
			adc_get_config_defaults(&config_adc_5);
			config_adc_5.positive_input        = ADC_POSITIVE_INPUT_PIN5 ;
			adc_init(&adc_instance_5, ADC, &config_adc_5);
			adc_enable(&adc_instance_5);
			break;
		case ADC_POSITIVE_INPUT_PIN6:
			adc_get_config_defaults(&config_adc_6);
			config_adc_6.positive_input        = ADC_POSITIVE_INPUT_PIN6 ;
			adc_init(&adc_instance_6, ADC, &config_adc_6);
			adc_enable(&adc_instance_6);
			break;
		case ADC_POSITIVE_INPUT_PIN7:
			adc_get_config_defaults(&config_adc_7);
			config_adc_7.positive_input        = ADC_POSITIVE_INPUT_PIN7 ;
			adc_init(&adc_instance_7, ADC, &config_adc_7);
			adc_enable(&adc_instance_7);
			break;
		case ADC_POSITIVE_INPUT_PIN16:
			adc_get_config_defaults(&config_adc_16);
			config_adc_16.positive_input        = ADC_POSITIVE_INPUT_PIN16 ;
			adc_init(&adc_instance_16, ADC, &config_adc_16);
			adc_enable(&adc_instance_16);
			break;
		case ADC_POSITIVE_INPUT_PIN17:
			adc_get_config_defaults(&config_adc_17);
			config_adc_17.positive_input        = ADC_POSITIVE_INPUT_PIN17 ;
			adc_init(&adc_instance_17, ADC, &config_adc_17);
			adc_enable(&adc_instance_17);
			break;
		case ADC_POSITIVE_INPUT_PIN18:
			adc_get_config_defaults(&config_adc_18);
			//adc_set_positive_input(&config_adc_18, ADC_POSITIVE_INPUT_PIN18);
			config_adc_18.positive_input        = ADC_POSITIVE_INPUT_PIN18;
			adc_init(&adc_instance_18, ADC, &config_adc_18);
			adc_enable(&adc_instance_18);
			break;
		case ADC_POSITIVE_INPUT_PIN19:
			adc_get_config_defaults(&config_adc_19);
			config_adc_19.positive_input        = ADC_POSITIVE_INPUT_PIN19 ;
			adc_init(&adc_instance_19, ADC, &config_adc_19);
			adc_enable(&adc_instance_19);
			break;
		default:
			#if	ADC_SERIAL_DEBUG
				serial_write_ln("No Adc Channel Found!");
			#endif
			break;
	}
}

uint16_t adc_read_value(int pin)
{
	uint16_t result;
	switch(pin)
	{
		case ADC_POSITIVE_INPUT_PIN0:
			adc_set_positive_input(&adc_instance_0, ADC_POSITIVE_INPUT_PIN0);
			adc_start_conversion(&adc_instance_0);
			do {
				/* Wait for conversion to be done and read out result */
			} while (adc_read(&adc_instance_0, &result) == STATUS_BUSY);
			//adc_read_buffer_job(&adc_instance, adc_result_buffer, ADC_SAMPLES);
			return result;
			break;
		case ADC_POSITIVE_INPUT_PIN1:
			adc_set_positive_input(&adc_instance_0, ADC_POSITIVE_INPUT_PIN1);
			adc_start_conversion(&adc_instance_1);
			do {
				/* Wait for conversion to be done and read out result */
			} while (adc_read(&adc_instance_1, &result) == STATUS_BUSY);
			return result;
			break;
		case ADC_POSITIVE_INPUT_PIN4:
			adc_set_positive_input(&adc_instance_0, ADC_POSITIVE_INPUT_PIN4);
			adc_start_conversion(&adc_instance_4);
			do {
				/* Wait for conversion to be done and read out result */
			} while (adc_read(&adc_instance_4, &result) == STATUS_BUSY);
			return result;
			break;
		case ADC_POSITIVE_INPUT_PIN5:
			adc_set_positive_input(&adc_instance_0, ADC_POSITIVE_INPUT_PIN5);
			adc_start_conversion(&adc_instance_5);
			do {
				/* Wait for conversion to be done and read out result */
			} while (adc_read(&adc_instance_5, &result) == STATUS_BUSY);
			return result;
			break;
		case ADC_POSITIVE_INPUT_PIN6:
			adc_set_positive_input(&adc_instance_0, ADC_POSITIVE_INPUT_PIN6);
			adc_start_conversion(&adc_instance_6);
			do {
				/* Wait for conversion to be done and read out result */
			} while (adc_read(&adc_instance_6, &result) == STATUS_BUSY);
			return result;
			break;
		case ADC_POSITIVE_INPUT_PIN18:
			adc_set_positive_input(&adc_instance_18, ADC_POSITIVE_INPUT_PIN18);
			adc_start_conversion(&adc_instance_18);
			do {
				/* Wait for conversion to be done and read out result */
			} while (adc_read(&adc_instance_18, &result) == STATUS_BUSY);
			return result;
			break;
		case ADC_POSITIVE_INPUT_PIN19:
			adc_set_positive_input(&adc_instance_19, ADC_POSITIVE_INPUT_PIN19);
			adc_start_conversion(&adc_instance_19);
			do {
				/* Wait for conversion to be done and read out result */
			} while (adc_read(&adc_instance_19, &result) == STATUS_BUSY);
			return result;
			break;			
		default:		
			#if	ADC_SERIAL_DEBUG
				serial_write_ln("No Adc Channel Found!");
			#endif
			return 0;
			break;
	}
}


