#ifndef GSMPORTSETUP_H
	#define GSMPORTSETUP_H
	#include "asf.h"
	#include "uart_drivers.h"
	
	#define  DEFAULTTIMEOUT     		 5      //seconds
	#define  DEFAULTCHARTIMEOUT 	     1500   //miliseconds
	#define  GSMBAUDRATE				 9600
	#define GET								0
	#define POST							1
	#define GSM_DBG_LEVEL				 3
	
	void  tGsmPortInit(void) ;
	bool  tGsmPortCheckDataAvailable(void);
	void  tGsmPortCleanBuffer(char* buffer, int numberOfBytes);
	bool  tGsmPortSendCmd(const char *cmd);
	void  tGsmPortSendChar(char c);
	void  tGsmPortSendEndMark(void);
	void  tGsmPortSendByte(uint8_t data);
	void  tGsmPortFlushSerial(void);
	void  tGsmPortReadData(char* buffer,int numberOfBytes,unsigned int totalOperationTime) ;
	bool  tGsmPortWaitForResponse (const char* expectedResponce, int type, unsigned int totalOperationTime) ;
	bool  tGsmPortSendCmdAndCheckResp(const char* cmd, const char *resp,int type);
	void  tGsmPortReadDataVaringTime(char* buffer,int numberOfBytes,unsigned int totalOperationTime,unsigned int totalCharTimeout);
	uint16_t tGsmPortReadchar(void);
	

#endif
