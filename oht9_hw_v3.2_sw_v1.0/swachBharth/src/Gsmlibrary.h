/*************************************
	* Project: Swachh Bharat (Feedback System)
	* Version: 1.0
	* Author: Manohar, Jagadevan
	* Created: 14/06/2017 12:49:20 PM
*************************************/


#ifndef GPRS_SHIELD_SAMD20_H_
	#define GPRS_SHIELD_SAMD20_H_
	#include "conf_ota.h"
	#include "conf_ota.h"
	#include "GsmPortSetup.h"
	#include "jsmn.h"
	
	#define  GSM_PWR_KEY				 PIN_PA27
	#define  REBOOT           	2
	
	
	#define GSMBUFFER30 		30
	#define GSMBUFFER40			40
	#define GSMBUFFER50 		50
	#define GSMBUFFER60			60
	#define GSMBUFFER90			90
	#define GSMBUFFER150		150
	#define GSMBUFFER220		220
	#define IMEILENGTH			15
	#define IMSILENGTH			15
	#define DELETEALLSMS		0
	#define ZERO_DBM_VALUE		-113
	#define HTTPSENDSUCCESS		200
	
	
	#define  LOCALSIM    	    1
	#define  ROAMINGSIM         5
	#define  GPSLOCATIONERROR   0
	#define  BOOTUPSUCCESS		1
	#define  SERVERERROR	    2
	#define  INTERNETERROR      3
	#define  TIMEBUFFERSIZE     30
	#define  UPLOADIMMEDIATE    1
	#define  UPLOADLATER        0
	#define  SENDINGSUCCESS     1
	#define  SENDINGFAIL		0
	#define  MESSAGE_LENGTH	    120
	
	#define APN_BUFFER_SIZE				50
	#define URL_BUFFER_SIZE				100
	#define SETTIME_BUFFER_SIZE         5
	#define SMS_BUFFER_SIZE				200
	#define MOBILE_NUMBER_SIZE			16
	
	// IMEI details
	#define IMEINOTFOUND    false
	#define IMEIFOUND       true
	#define IMEI_SIZE		16

	// IMSI details
	#define IMSI_SIZE		16
	
	//HTTP RESPONSE CODES
	#define HTTP_SUCCESS			1
	#define HTTP_INIT_ERROR			10
	#define HTTP_SEND_ERROR		    11
	//Live Data send timeout
	#define LIVE_DATA_SEND_TIMEOUT	10	//in sec's this is the timeout 
	#define  OFF              	0
	#define  ON               	1
	/************************************************************************/
	/*           SIM RELATED ERROR CODES -->70 - 79                         */
	/************************************************************************/
	#define CPIN_ERROR				70
	#define SIM_REG_ERROR			71
	
	//error messages sending to SMS GW disable(1)/enable(1)
	#define SEND_ERROR_SMS			0
	
	#define NOT_AUTHORIZED          -1
	
	uint8_t GSM_init(void);
	bool checkSIMStatus(void);
	void deleteAllSMS(int x);
	bool sendSmsWithDeviceID(char* number, const char* data);
	int8_t isSMSunread(void);
	bool readSMS(int messageIndex, char *message, int length, char *phone, char *datetime); 
	bool deleteSMS(int index);
	bool getSubscriberNumber(char *number);        
    bool getDateTime(char *buffer);
	bool getSignalStrength(int *buffer);	
	bool getProviderName(char *buffer);
	bool getSIMRegistration(int *networkType);
	bool join(char *apn );
    void disconnect(void);
    bool connect(int ptl, const char * host, int port);
	bool isConnected(void);
    int  readable(void);
    int  send(const char * str, int len);
    int  recv(char* buf, int len);
	bool getImsi(char *imsi_no);
	bool getLocation(char *apn,char *longitude, char *latitude);
	char* getIPAddress(void);
	bool httpInit(char *apn);
    bool close(void);
	void getIPAddr(char *ipaddr);
	bool getImei(char *imei_no);
	uint16_t httpSend(char * URL_str,char *URL_DATA,int Request_type,unsigned int *Received_File_size,int char_timeout_secs);//#MOTA
	void httpClose(void);
	bool gsmCheckPowerUp(void);
	void gsmPowerUpDown(uint8_t pin, uint8_t mode);
	bool sendSMSNormal(char *number, const char *data);
	void send_failure_sms(int sending_type,int response);
	bool isSimRegistered(void);
	
	// sending_type is like 
	bool sendHttpData(char * URL_str,char *URL_DATA,int Request_type,int sending_type,unsigned int *Received_File_size,int char_timeout_secs);

	void 	 complete_system_reset(void);
	bool	 search_char_in_binary(char *SearchChar, char *BinaryString, unsigned long int EFilesize, int *index_foundAt,int stEnFlag,int starting_flag);
	bool	 program_memory(uint32_t address, uint8_t *buffer, uint16_t len);
	bool isGprsConnectionStarted(void);
	bool extract_string_using_adds(char *start_adds,char *end_adds,char *extracted_string);//

#ifdef ENABLE_OTA
	int startOTAUpdate(uint32_t Filesize,char *server_binFile_checksum);	//#MOTA
	bool startGprsConnection(char *apn);			//#MOTA
	bool startTCPconnection(char *hostName,unsigned int port);//#MOTA
	bool sendData(char *Data,char *recvedData,char *server_checksum);//#MOTA
	void TCPclose(void); //#MOTA	
	bool calculate_crc32b_from_flash(uint32_t start_address,unsigned long *flash_CRC,uint16_t len_flash_read); 
#endif	

	int  checkIfNumberAuthorized(char *mobileNumber);

#endif /* GPRS_SHIELD_SAMD20_H_ */	
