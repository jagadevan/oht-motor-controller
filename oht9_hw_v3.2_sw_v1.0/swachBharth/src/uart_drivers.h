/*
 * uart_drivers.h
 *
 * Created: 1/10/2018 3:29:12 PM
 *  Author: bomma
 */ 


#ifndef UART_DRIVERS_H_
#define UART_DRIVERS_H_
	#include "asf.h"
	#define DEBUG_LEVEL 2	//debug levels 0 to 4
	
	#define BAUD_RATE 9600
	#define DEC		10
	#define FLOAT	12
	#define HEX		16
	#define STR		20
	
	void	serial_number(int  num);
	void    serial_read_buffer(char* buffer, int count,  unsigned int timeout_buf);
	int     power(int base,int exp);
	void    reverse(char *str, int len);
	int     intToStr(int x, char str[], int d);
	void    ftoa(float n, char *res, int afterpoint);
	void 	debug_uart_init(void);
	bool 	serial_available(void);
	void  	serial_write(const char* cmd);
	void	serial_debug(void *string_buf,uint8_t format,uint8_t debug_level);
	void	serial_debug_ln(void *string,uint8_t format,uint8_t debug_level);
	void    serial_write_char(uint16_t c);
	void	serial_write_float(float num);
	void	serial_write_float_ln(float num);
	
	//#MOTA
	void	 serial_write_int_ln(uint32_t num);
	void	 serial_write_int(uint32_t num);
	uint16_t serial_read_char(void);
	void	 serial_write_ln(char * string_buf);
	void	 itoa1(uint32_t n, char s[]);
	void	 reverse1(char s[]);
	void serial_write_hex(uint32_t num);
	void serial_write_hex_ln(uint32_t num);



#endif /* UART_DRIVERS_H_ */