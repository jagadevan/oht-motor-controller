#include "RingBuffer.h"

int gFb_index = 0;

struct ring_buffer
{
	char fb_ts[FEEDBACK_ENTRY_SIZE];
	char button_type;
	
}fb_stack[FB_STACK_SIZE] = {0};

void cleanFbBuffer(int number_of_fbs) {
	int buf_index = gFb_index;
	
	for (int i = buf_index - 1; i >= (buf_index - number_of_fbs); i--) {
		memset(fb_stack[i].fb_ts,0,FEEDBACK_ENTRY_SIZE);
		gFb_index --;
	}
}


bool pushIntoFbStack(int button_type) {

	char ts[TIMEBUFFERSIZE] = {0};

	//get timestamp from RTC	
	tRTCGetTimeStamp(ts,POST);

	//feedback entry format "r|2018-01-04 14:50:08"//		
	if(gFb_index != FB_STACK_SIZE) {
		switch(button_type) {
			case RED:
				strcpy(fb_stack[gFb_index].fb_ts,ts);
				fb_stack[gFb_index++].button_type = 'r';				
				break;
			case YELLOW:
				strcpy(fb_stack[gFb_index].fb_ts,ts);
				fb_stack[gFb_index++].button_type = 'y';
				break;
			case GREEN:
				strcpy(fb_stack[gFb_index].fb_ts,ts);
				fb_stack[gFb_index++].button_type = 'g';
				break;
			default:
				serial_write_ln("Invalid button type");
				return false;
				break;
		}
		printRingIndex();
		return true;
	}
	else{
		serial_write_ln("buffer is full");
		printRingIndex();
		return false;
	}
}

void printRingBuffer(void) {
	
	char buffer[JSON_DATA_BUFFER_SIZE] = {0};
	for (int count = getRingBufferIndex()-1; count >= 0; count--) {
		createJsonData(count,buffer);
		serial_write(buffer);
		serial_write("  ->");
		serial_write_int_ln(count);
	}
   printRingIndex();
}

bool createJsonData(int index,char* json_data) {
	
	memset(json_data,0,JSON_DATA_BUFFER_SIZE);
	
	if ( (fb_stack[index].button_type == 'r') || (fb_stack[index].button_type == 'y' ) || (fb_stack[index].button_type == 'g')){
		sprintf(json_data,"{\"feedback\":\"%c\",\"ts\":\"%s\"}",fb_stack[index].button_type,fb_stack[index].fb_ts);
		return true;
	}
	else{
		serial_write("No data at index:");
		serial_write_int_ln(index);
		return false;
	}
	
}

void printRingIndex(void) {

	serial_write("ring index: ");
	serial_write_int_ln(getRingBufferIndex());
}

int getRingBufferIndex(void) {
	return gFb_index;
}