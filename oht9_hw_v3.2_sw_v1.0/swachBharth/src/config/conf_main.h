/*
 * conf_main.h
 *
 * Created: 10-01-2018 14:32:35
 *  Author: lovel
 */ 


#ifndef CONF_MAIN_H_
#define CONF_MAIN_H_



/********* configurations ***************/
//#define HARDWARE_VERSION_2P0	1
//#define HARDWARE_VERSION_3P0	1
#define HARDWARE_VERSION_3P1	1
//#define HARDWARE_VERSION_3P2	1
#define BATTERY_TYPE			1		// 0 for lead acid battery and 1 for li-ion battery



#endif /* CONF_MAIN_H_ */