/**

 * @brief			OTA configurations are included
 * - author			thingtronics Innovations Pvt.Ltd 
 * - developers		manohar,Lovelesh Patel.
 * - created			10/4/2017 4:25:40 PM
 * - last Updated		03-04-2018
 * 
 * 
 * 
 * NOTE: 
 * -----
 *  in samd20e18_flash.ld file changes needs to be done as per present application
 *	- if PRESENT application is 0x00022000,set addresses rom    (rx)  : ORIGIN = 0x00022000, LENGTH = 0x0003B000
 *  - if PRESENT application is 0x00009000,set addresses rom    (rx)  : ORIGIN = 0x00009000, LENGTH = 0x00022000
 *
 *  OTA SHORT NOTES:
 *  ----------------
 * - Bootloader starts		: 0x00000000
 * - Bootloader ends		: 0x00008FFF
 * - 1st application starts	: 0x00009000
 * - 1st application ends	: 0x00021FFF
 * - 2nd application starts	: 0x00022000
 * - 2nd application ends	: 0x0003FBFF
 *
 *
 * 
 */ 


#ifndef CONF_OTA_H_
#define CONF_OTA_H_

	
#define ENABLE_OTA						1
#include "samd20j.h"
/**
 * \defgroup OTA Configurations
 *
 * \ingroup OTA Configurations
 *
 * @{
 */
/** crc32b enable(1) or disable(0)*/
#define CRC32_ENABLED					1	
#if CRC32_ENABLED
	#include "crc32_table.h"
#endif
/** * OTA debug level from (0 to 3)*/
#define OTA_DEBUG_LEVEL					2	
/** * binary Data block size to read from GSM*/
#define BIN_DATA_SIZE					1024				
/** * buffer size while reading bin Data from GSM*/
#define OTA_BUFFER_SIZE					BIN_DATA_SIZE + 60	
/** * present application address*/
#define PRESENT_APP_ADDR				0x00009000	
/** * EEProm address, this location will be same in bootloader*/		
#define OTA_UPDATE_FLAG_LOCATION		5					
/** * reset mechanism after OTA succesful */				
#define OTA_RESET						wdt_reset()		
/** * bin file url path max size to be downloaded*/	
#define NEW_FIRMWARE_URL_SIZE			300			
/** * crc32b check sum size (size is fixed because 32bit)
 *  * ex: "b4e44ed1" 
*/		
#define CHECKSUM_SIZE					10	
/** * min bin file size in bytes that has to be */
#define OTA_MIN_BIN_FILE_SIZE			20 * 1024
/** * max bin file size in bytes that has to be*/			
#define OTA_MAX_BIN_FILE_SIZE			100 * 1024		
/** * bin file download timeout in sec's from server
 *  * it will depends on server latency and file size
*/	
#define	FILE_DOWNLOAD_TIMEOUT			25	
/** * firmware update check timeout in sec's 
*/				
#define TCP_URL_GET_TIMEOUT				10				
/** * firmware update buffer in bytes */	
#define	FIRMWARE_UPDATE_CHECK_BUFFER	NEW_FIRMWARE_URL_SIZE + 300		
/** 
 * * present firmware device details size min 106
 *
 * * exe: "GET /ota/server/php/public/index.php HTTP/1.0\nimei:863987032485611\nproj:feedback-gsm\nhw_v:3.1.1\nsw_v:3.3.1\nb_addr:9000\n\r" 
 */
#define PRESENT_FIRMWARE_DETAILS_SIZE	200 

#if PRESENT_APP_ADDR == 0x00022000
	/**  * new firmware saving addr(APP_ADDR)--> 0x00009000*/
	#define APP_ADDR					0x00009000		//Next application Address
	/**  * present app adds(PRESENT_ADDR)--> 22000*/	
	#define PRESENT_ADDR				22000
	/**  * update success flag(value 9) into eeprom*/
	#define OTA_EEPROM_UPDATE_SUCCESS			EEPROM_write(OTA_UPDATE_FLAG_LOCATION,2,"9")
	/**  * update failure flag(value 2) into eeprom*/
	#define OTA_EEPROM_UPDATE_FAILED			EEPROM_write(OTA_UPDATE_FLAG_LOCATION,2,"2")	
#elif PRESENT_APP_ADDR == 0x00009000
	 /**  * new firmware saving addr(APP_ADDR)--> 0x00022000*/
	#define APP_ADDR					0x00022000	
	/**  * present app adds(PRESENT_ADDR)--> 9000*/		
	#define PRESENT_ADDR				9000
	/**  * update success flag(value 2) into eeprom*/
	#define OTA_EEPROM_UPDATE_SUCCESS			EEPROM_write(OTA_UPDATE_FLAG_LOCATION,2,"2")	
	/**  * update failure flag(value 9) into eeprom*/
	#define OTA_EEPROM_UPDATE_FAILED			EEPROM_write(OTA_UPDATE_FLAG_LOCATION,2,"9")
#else 
	#error "Please configure PRESENT_APP_ADDR in conf_ota.h" 
#endif

/** * OTA update success code*/
#define  OTA_UPDATE_SUCCESS			30
/** * if binFile size is not within the limit this error will return*/
#define  OTA_FILE_SIZE_ERROR		31
/** * error code, if get error writing into flash*/
#define  OTA_FLASH_ERROR			33
/** * error code will return following cases
  * * if checksum(crc32b) not matched,when crc checking enabled,
  * * if downloaded bin data not equals to written bin data into flash*/
#define  OTA_FILE_CORRUPT_ERROR		34


/** @} */

#endif /* CONF_OTA_H_ */