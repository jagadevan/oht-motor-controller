/* PROJECT NAME			  : GMR TEMPERATURE 
 * Version				  : 2.0	
 * Modified Date          : 04-06-2018
 * Author				  : thingTronics Innovations PvtLtd
 */ 

#include "main.h"


const char hardwareVersion[] = "3.2";
const char softwareVersion[] = "1.0";


#ifdef ENABLE_OTA
const char ota_project_name[] = "temp-sensor-gsm";
const char ota_server[]       = "ota.thingtronics.org";
#endif

const char projectName[]     = "OTH MOTOR CONTROL SYSTEM";
const char companyName[]     = "Thingtronics Innovations Pvt Ltd";
const char vendorName[]		 = "NIL";
const char apiVersion[]      = "2.0";

// DEFAULT SETTINGS
const char project_server[]      = "www.thingtronics.net";		//"52.42.225.35";
const char apn[]	             = "iot.thingtronics.com";

const char gatewayReceiveNumber[]     = "THNGTR";		//4744525
const char gatewayReceiveNumber_1[]   = "4744525";
const char gatewayReceiveNumber_2[]   = "9611502103";
const char gatewaySendNumber[]	      = "9891024477";

#ifdef ENABLE_OTA
//#MOTA
const char firmware_update_check_url[ ]  = "GET /ota/server/php/public/index.php HTTP/1.0\n";
#endif

//#MOTA
#ifdef ENABLE_OTA
	#if PRESENT_APP_ADDR == 0x00022000
		const char* appAddress      =  "0x00022000";
	#elif PRESENT_APP_ADDR == 0x00009000
		const char* appAddress      =  "0x00009000";
	#else
	#error "Please configure PRESENT_APP_ADDS in conf_ota.h"
	#endif
#endif



// Global variables ( Flags)
bool g_InitHardwareFlag		= false;
bool g_SIMCardFlag		   	= false;
bool g_SMSServiceFlag	   	= false;
bool g_RTCCallbackFlag     	= false;
bool g_InterruptsEnabled    = true;
bool g_DeviceConfigFlag     = false;
bool g_SendBootupFlag 		= false;
bool g_IMEIvalidFlag		= false;
volatile bool AdcReadDone	= false;

// Global Variables

char gImeiNo[IMEI_SIZE]		 = {0};
char g_ImsiNo[IMSI_SIZE]     = {0};  // can be made local

char longitude[12]			 = {0};
char latitude[12]			 = {0};
unsigned int g_resetCause    = 0;

uint16_t adc_result_buffer[ADC_SAMPLES];
unsigned long  g_msgReadTimeInterval	= 0;	// Interval to read SMS from GSM module
unsigned long  g_RtcBackUpTimer         = 0;	//#CHANGES_3P2 //can be removed
unsigned long  g_bootupTimeInterval     = 0;
unsigned int   g_simErrorCode	= 0;


double  AvgTempC=0, tempC = 0,tempCelArrray[TEMP_READ_SAMPLE]={0};
double  AvgtempAdcVoltage=0, tempAdcVoltage = 0,tempAdcVoltageArrray[TEMP_READ_SAMPLE]={0};
	
uint8_t g_tempSampleCounter = 0;
// ADC Related Variables
struct dateStruct time_gsm;
xTaskHandle TaskHandle_1;


int main(void) {
	
	system_init();
	debug_uart_init();
	tGsmPortInit();
	xTaskCreate(main_task,(const char *)"Main task",configMINIMAL_STACK_SIZE + ADDITIONAL_STACK ,NULL,tskIDLE_PRIORITY + 1 ,&TaskHandle_1);
	vTaskStartScheduler();
	
}

 void main_task(void *params) {
	
int  networkRegistrationType = 0;
	
enum system_reset_cause reset_cause = system_get_reset_cause();
g_resetCause = reset_cause;

serial_write("\r\nSYSTEM RESET REASON      : ");

switch (g_resetCause)
{
	/** The system was last reset by a software reset */
	
	case SYSTEM_RESET_CAUSE_SOFTWARE:
		serial_write("SOFTWARE RESET");
		serial_write_int_ln(SYSTEM_RESET_CAUSE_SOFTWARE);
		break;
	case SYSTEM_RESET_CAUSE_WDT:
	/** The system was last reset by the watchdog timer */
		serial_write("WATCHDOG TIMER RESET");
		serial_write_int_ln(SYSTEM_RESET_CAUSE_WDT);
		break;
	case SYSTEM_RESET_CAUSE_EXTERNAL_RESET:
		serial_write("EXTERNAL PIN WAS PULLED LOW");
		serial_write_int_ln(SYSTEM_RESET_CAUSE_EXTERNAL_RESET);
		break;
	case SYSTEM_RESET_CAUSE_BOD33:
		serial_write("BOD33,");
		serial_write_int_ln(SYSTEM_RESET_CAUSE_BOD33);
		break;
	case SYSTEM_RESET_CAUSE_BOD12:
		serial_write("BOD12,");
		serial_write_int_ln(SYSTEM_RESET_CAUSE_BOD12);
		break;
	case SYSTEM_RESET_CAUSE_POR:
		serial_write("POR (Power on reset)");
		serial_write_int_ln(SYSTEM_RESET_CAUSE_POR);
		break;
	default:
		serial_write_ln("UNKNOWN");
		
	}	
	
	tDeviceInfo();	
	configure_wdt();
		
	/* Hardware initialization Function */
	if(tInitHardware()) {					
		g_InitHardwareFlag = true;
		serial_write_ln("HARDWARE INTIALIZATION   : SUCCESSFUL\r\n");
		delay_ms(1000);	
		//synchronizes GSM clock with provider
		tgsmClkSync();
		//read imei
		tReadImei(gImeiNo);
		serial_write("DEVICE IMEI NO           : ");
		serial_write_ln(gImeiNo);
		/* Sim Card Configuration Function 	 */
			g_SIMCardFlag = true;
			
		if(tSimCardSetup()) {  
			wdt_reset_count();//reset the WDT
			/* SMS Configuration Function 	*/
			serial_write_ln("SIMCARD SETUP            : SUCCESSFUL");
			if(tSmsServiceSetup()) {
				g_SMSServiceFlag = true ;
				serial_write_ln("SMS SERVICE SETUP        : SUCCESSFUL");
			}
			else {
				g_SMSServiceFlag = false;
				serial_write_ln("SMS SERVICE SETUP         : FAILED");
			}
		}
		else {
			g_SIMCardFlag = false;
			serial_write_ln("SIMCARD SETUP            : FAILED");
		}
	}
	else {
		serial_write_ln("GSM HARDWARE INTIALIZATION : FAILED");
	}
	
 
	/* EEPROM Configuration */	
	if(tEEPROMSetup()){
		serial_write_ln("EEPROM INITIAL SETUP     : SUCCESSFUL");
		
		/*Device Configuration Setup*/
		if (tDeviceConfigSetup()) {
			g_DeviceConfigFlag = true;
			serial_write_ln("DEVICE CONFIGURATION SETUP: SUCCESS");
		}
		else {
			serial_write_ln("DEVICE CONFIGURATION SETUP: FAILED");
		}
		
	}
	else {
		serial_write_ln("EEPROM INITIAL SETUP     : FAILED");		
	}
	// RTC Configuration 
	tRTCInit();	
	
	wdt_reset_count();		
/*	
	uint8_t bootUpReturnValue = tSendBootupData();
	if( bootUpReturnValue == BOOTUPSUCCESS ) {
		g_SendBootupFlag  = false;
		serial_write_ln("Boot up Data Successfully Sent");
	}
	else {
		g_SendBootupFlag  = true;
		serial_write_ln("Failed! sending Bootupdata");
		

	}
*/	
	// feed watchdog timer
	wdt_reset_count();
	

	g_bootupTimeInterval = millis();
#ifdef ENABLE_OTA
// Check for OTA after bootup
 // checkOTAUpdate();
#endif
    
	BUZZER_ON
	delay_ms(500);
	BUZZER_OFF

	/* Main Loop */
	while(true)	{
							
		wdt_reset_count();	
		
		// Handle unread SMS
		if ( millis() - g_msgReadTimeInterval > MSGREADINTERVAL) {
			
			int8_t messageIndex = isSMSunread();
			g_msgReadTimeInterval = millis();
			if (messageIndex > 0) {
				serial_write_ln("CHECKING FOR NEW MSG     : MSG FOUND");
				BUZZER_OFF;
				tHandleSMS(messageIndex);
			}
			else {
				serial_write_ln("CHECKING FOR NEW MSG     : NO MSG FOUND");
			}
		}
		
		
		
		
		/*
		// backup timer for RTC in case RTC alarm fails to hit 
		if (millis() - g_RtcBackUpTimer > ((DATA_SEND_TIME * MINUTE) + (10 * MINUTE)) ){	
			serial_write_ln("Rtc Back UP Running");
			g_RTCCallbackFlag = true;
			g_RtcBackUpTimer  = millis();
		}
	
		if(g_RTCCallbackFlag) {
			g_RTCCallbackFlag = false;
			g_RtcBackUpTimer = millis();
			BUZZER_OFF;
			tSendLiveData();
			tRTCSetAlarmTime(SETNORMALTIME);	
		}
		*/
		
		// REBOOT section		
		if (millis() - g_bootupTimeInterval > DAY  ) {
			// reboot the system every 24 hrs
			complete_system_reset();
		}	
		
	}
}

/* GPIO related configuration are here  
 * PIN NUMBER   : NAME			 : PIN DIRECTION
 * PIN_PA07 	: REDBUTTON      : INPUT 
 * PIN_PA06		: YELLOWBUTTON   : INPUT
 * PIN_PA05		: GREENBUTTON    : INPUT
 * PIN_PA03		: POWER KEY      : INPUT
 * PIN_PA04		: BUZZER         : OUTPUT
 * PIN_PA28 	: GSM POWER KEY  : OUTPUT */





/* This Function will give Current Battery value,
 * here Reference Voltage is 3.3 Volts
 * Total Resolution is 12bit 4096
 * i,e 4096 unit is for 3.3 volt , similarly for 1 unit it will be (3.3 /  4096)
 * ADC value multiplied by (3.3/4096) ADC input in terms of Voltage
 * Fully charged Battery Gives 2.8 volts
 * Lets Assume 2.8 as out 100% battery percent
 * Obtained Voltage multiplied by 100 and divided by 2.8 gives 
 * Battery Voltage in terms of Percentage */

 

/* This Function Will Load One time Configuration into EEPROM,
 * if Already Loaded then Read the Configuration From the EEPROM */
bool tDeviceConfigSetup (void) { 

	tWriteInitialConfig();
	return true;
}

/* This Function Is used Setup EEPROM MEMORY 
 *  if Memory Not allocated Return Error*/
bool tEEPROMSetup(void) {
	
	uint8_t EepromRetryCnt = 0;				
	enum status_code error_code;

	while (EepromRetryCnt < 3) {				
		error_code = eeprom_emulator_init();
		if (error_code == STATUS_ERR_NO_MEMORY) {
			serial_write_ln("EEPROM MEMORY            : NOT FOUND");
		}
		else if (error_code != STATUS_OK) {
			eeprom_emulator_erase_memory();
			error_code = eeprom_emulator_init();
			if (error_code == STATUS_OK)
				break;
		}
		else if( error_code == STATUS_OK) {
			break;			
		}
		EepromRetryCnt++;
	}

	struct bod_config config_bod33;
	bod_get_config_defaults(&config_bod33);
	config_bod33.action = BOD_ACTION_RESET; // check with reset BOD_ACTION_RESET
	/* BOD33 threshold level is about 3.2V */
	config_bod33.level = BODLEVEL;			
	bod_set_config(BOD_BOD33, &config_bod33);
	bod_enable(BOD_BOD33);
	// below should not be required for reset
	/*
	SYSCTRL->INTENSET.reg = SYSCTRL_INTENCLR_BOD33DET;
	system_interrupt_enable(SYSTEM_INTERRUPT_MODULE_SYSCTRL);
	*/
	serial_write_ln("EEPROM MEMORY SETUP      : DONE");
	return (error_code == STATUS_OK) ? true : false ;
}

/* This Function Writes Default Configuration to EEPROM 
 * this will be Done Only Device is booted up for 1st time
 * or When device is restored . */ 
bool tWriteInitialConfig (void) {
							
	//delete all SMS
	deleteAllSMS(0);
	serial_write_ln("SMS DELECTING            : SUCCESSFUL");
	return true;

}


/* This Function Includes RTC Initial Configuration Setting
 * it gets the time from GSM and Updates its Variable
 * Sets the Alarm
 * RTC Configures Call Back function */

void tRTCInit(void) {
	rtc_calendar_get_time_defaults(&time);
	
	// Get time From GSM
	if(tGsmTime() == true) {
		time.year   = time_gsm.year + 2000;
		time.month  = time_gsm.month;
		time.day    = time_gsm.day;
		time.hour   = time_gsm.hour;
		time.minute = time_gsm.minute;
		time.second = time_gsm.second;
	}
	else {
	// Condition will occur when GSM is not Powered, we go with default time and date  
		time.year   = DEFAULT_YEAR;
		time.month  = DEFAULT_MONTH;
		time.day    = DEFAULT_DAY;
		time.hour   = DEFAULT_HOUR;
		time.minute = DEFAULT_MINUTE;
		time.second = DEFAULT_SECOND;
	}
	tRTCSetIntialAlarmTime();
	tRTCCallbackConfiguration();
	rtc_calendar_set_time(&rtc_instance, &time);
}

void tRTCSetIntialAlarmTime(void) {
	int temp_minute = 0;
	char alarm_time_local[50] = {0};
	
	struct rtc_calendar_config config_rtc_calendar;
	rtc_calendar_get_config_defaults(&config_rtc_calendar);
	
	
	alarm.time.year      = time.year;
	alarm.time.month     = time.month;
	alarm.time.day       = time.day;
	alarm.time.hour      = time.hour;
	alarm.time.minute    = time.minute;
	alarm.time.second    = time.second;
	
	alarm.time.second    =  0;
	alarm.time.minute    += 2;
	temp_minute			 =  alarm.time.minute;
	alarm.time.minute    =  (alarm.time.minute % 60);
	alarm.time.hour      += (temp_minute / 60);
	if(alarm.time.hour > 23) {
		alarm.time.hour=0;
	}
	config_rtc_calendar.clock_24h     = true;
	config_rtc_calendar.alarm[0].time = alarm.time;
	config_rtc_calendar.alarm[0].mask = RTC_CALENDAR_ALARM_MASK_HOUR;

	rtc_calendar_init(&rtc_instance, RTC, &config_rtc_calendar);

	rtc_calendar_enable(&rtc_instance);
	sprintf(alarm_time_local,"NEXT ALARM TIME          : %d : %d :%d \n",alarm.time.hour,alarm.time.minute,alarm.time.second);
	serial_write(alarm_time_local);
}

/* This Function Will be called to Update the Alarm Time,
 * if AlarmPeriodicityFlag is True  then alarm is set to Actual time specified in configuration
 * if AlarmPeriodicityFlag is False then alarm is set to Reduced time ( 1/10)th of the Specified Time */
void tRTCSetAlarmTime (bool AlarmPeriodicityFlag) {	
	int setHoutInt = 0;
	char alarm_time_local[50] = {0};
	
	serial_write_ln("RTC CallBack");
	
	setHoutInt = DATA_SEND_TIME;
		
	int temp_minute = 0;
	alarm.mask = RTC_CALENDAR_ALARM_MASK_HOUR;
	alarm.time.second   =  0;
	alarm.time.minute   += setHoutInt ;
	temp_minute			=  alarm.time.minute;
	alarm.time.minute   =  (alarm.time.minute % 60);
	alarm.time.hour     += (temp_minute / 60) ;
	if(alarm.time.hour > 23) {
		alarm.time.hour=0;
	}
		
	rtc_calendar_set_alarm(&rtc_instance, &alarm, RTC_CALENDAR_ALARM_0);	
	sprintf(alarm_time_local,"NEXT ALARM TIME          : %d : %d :%d \n",alarm.time.hour,alarm.time.minute,alarm.time.second);
	serial_write(alarm_time_local);
}

/* This Function Configure The Callback Mechanism of RTC
 * And Enable the RTC Callback Function */
 void tRTCCallbackConfiguration(void) {
	/* Registering Callback with tRTCCallBackFunction function */
	rtc_calendar_register_callback(&rtc_instance, tRTCCallBackFunction, RTC_CALENDAR_CALLBACK_ALARM_0);
	/* Enabling The RTC callback */
	rtc_calendar_enable_callback(&rtc_instance, RTC_CALENDAR_CALLBACK_ALARM_0);
}

/* This Function is used to update RTC Time
 * Like , When you want to sink Rtc with Networ
 .3333k timing 
 * Date & Time Parameters Are passed to this function to Updation */
void tRTCUpdateTime(unsigned int year,unsigned int month,unsigned int day,unsigned int hour,unsigned int minute,unsigned int second) {
	time.year   = year;
	time.month  = month;
	time.day    = day;
	time.hour   = hour;
	time.minute = minute;
	time.second = second;
	rtc_calendar_set_time(&rtc_instance, &time);
}


/* This Function  will called when ever RTC callback occurs
 * and makes a Rtc global variable High indicating call back occurred */
void tRTCCallBackFunction(void) {
	g_RTCCallbackFlag = true;
}



bool tSimCardSetup(void) {	 
	// AT+CSPN?
	serial_write_ln("\r\nSIM CARD SETTING");	
	
	tGsmPortSendCmdAndCheckResp("AT+CPBS=\"ME\"\r\n","OK\r\n",0);
	
	int  rssi = 0;
	char rssiInChar[5] = {0};
	
	// sent AT+CSQ
	getSignalStrength(&rssi);
	serial_write("SIGNAL STRENGTH          : ");
	itoa(rssi,rssiInChar,10);
	serial_write(rssiInChar);
	serial_write_ln("dbm");
	// Check for SIM registration
	// Send AT+CREG?
	int  networkType = 0;
	getSIMRegistration(&networkType);
	if( networkType == LOCALSIM || networkType == ROAMINGSIM) {
		serial_write_ln("IS SIM REGISTRED         : YES");
		if(networkType == LOCALSIM ) {
			serial_write_ln("NETWORK TYPE             : LOCAL (1)");
		}
		else if(networkType == ROAMINGSIM) {
			serial_write_ln("NETWORK TYPE             : ROAMING (5)");
		}
	}
	else {
		serial_write_ln("IS SIM REGISTRED         : NO");
		return false;
	}
	
	// Set the clock to update from Service Provider
	// AT+CLTS=1
	tGsmPortSendCmdAndCheckResp("AT+CLTS=1\r\n","OK\r\n",0);
	delay_ms(2000);
	return true;
}

bool tSmsServiceSetup(void) {
	
	// Set SMS input mode
	//serial_write("Setting input mode\n\r");
	
	// send AT+CMGF=1
	// This is used to Select Text Mode or Data Mode
	// Text is 1
	// Data is 0
	tGsmPortSendCmdAndCheckResp("AT+CMGF=1\r\n","OK\r\n",0);
	
	// serial_write("Setting character mode: GSM\n\r");
	// Send AT+CSCS="GSM" 
	// This Command will make Module to work in GSM 7 bit default alphabet ( other available option other than GSM are Hexadecimal ,ISO characters etc
	tGsmPortSendCmdAndCheckResp("AT+CSCS=\"GSM\"\r\n","OK\r\n",0);
	
	// Set SMS mode
	// serial_write_ln("Setting SMS message indications");
	
	/* Send AT+CNMI=2,1,0,0,0
	* This Command Lets us How a new SMS indication alert to be done
	* 2,1,0,0,0 indicates SMS to be Stored in Memory
	* when SMS comes GSM will Alert us using
	* +CMTI : message index */ 
	tGsmPortSendCmdAndCheckResp("AT+CNMI=2,1,0,0\r\n","OK\r\n",0);
	
	/* Additional SMS Text mode Setting */
	tGsmPortSendCmdAndCheckResp(("AT+CSMP =17,255,0,0\r\n"),"OK\r\n",0);
	
	// Send AT+CSAS=0
	// Save the settings for SMS
	// serial_write("Saving settings to memory\n\r");
	tGsmPortSendCmdAndCheckResp("AT+CSAS=0\r\n","OK\r\n",0);
	
	// serial_write("SMS Service Ready\n\r");
	tGsmPortSendCmdAndCheckResp("AT+CPBS=\"ME\"\r\n","OK\r\n",0);
	
	deleteAllSMS(ALL_SMS);

	return true;
}

bool tSendBootupData(void) {	
   uint8_t retry = 0;
     
   while ( retry < 3) {
		getLocation(apn,longitude,latitude);
		
		if(!(tCheckGpsLatLonValidity(longitude,latitude))) {
			if(retry == 2 ) {
				serial_write_ln("\r\nGPS LOCATION FETCHING    : FAILED");
				//send_failure_sms(BOOTUPDATA,INVALID_LAT_LONG);
				//return GPSLOCATIONERROR;
				strcpy(longitude,"0.0");
				strcpy(latitude,"0.0");
				break;
			}
		}
		else {
			break;
		}
		retry++;
	}
	
	serial_write_ln("\r\nGPS LOCATION  ");
	serial_write("  LATITUDE               : ");
	serial_write_ln(latitude);
	serial_write("  LONGITUDE              : ");
	serial_write_ln(longitude);
	
	if(tSendDataToCloud(BOOTUPDATA)) {
		serial_write_ln("Boot up Data Was Successfully sent");
		return BOOTUPSUCCESS;
	}
	else {
		serial_write_ln("Error : Sending Bootup Failed");
		return INTERNETERROR;
	}	
}

bool tSendLiveData(void) {
	int networkRegistrationType = 0;
	getSIMRegistration(&networkRegistrationType);
	if( networkRegistrationType == LOCALSIM || networkRegistrationType == ROAMINGSIM )  {
	  }else{
		
		if (!g_simErrorCode)
		{
			char sim_error_char[4]={0};
			if(networkRegistrationType == 0){
				itoa(SIM_REG_ERROR,sim_error_char,10);
				EEPROM_write(SIM_ERROR_LOG_LOCATION,SIM_ERROR_LEN,sim_error_char);
			}//SIM_REG_ERROR
			else{
				itoa(networkRegistrationType,sim_error_char,10);
				EEPROM_write(SIM_ERROR_LOG_LOCATION,SIM_ERROR_LEN,sim_error_char);
			}
		}

		
	}
	
	if (gsmCheckPowerUp() && isSimRegistered()) {
		g_InitHardwareFlag = true;
		//EEPROM_write(SIM_ERROR_LOG_LOCATION,2,networkRegistrationType);
	}	
	else {
		gsmPowerOnOff(REBOOT);
	}
		
	if (!isSimRegistered()) {
		serial_write_ln("registration Failed!");
		return false;
	}
		
	if(tSendDataToCloud(LIVEDATA) == SENDINGSUCCESS) {
			serial_write_ln("Data is Uploaded Successfully");
			return SENDINGSUCCESS;
		}
	else {
		serial_write_ln("Error in Sending data to Cloud");
		return SENDINGFAIL; 
	}
}

bool tSendDataToCloud(uint8_t data) {
	char ipaddr[16] = {0};
	char urlLink[300] = {0};
	int  rssi = 0;
	char timestamp[GSMBUFFER40] = {0};
	int received_payload_size = 0;
	int retry_count = 0;
	wdt_reset_count();//reset wdt count
	// get RSSI
	getSignalStrength(&rssi);
	
	if (!g_IMEIvalidFlag) {
		if(!tReadImei(gImeiNo)){
			return false;	
		}
	}
	if (isGprsConnectionStarted())
	{
		httpClose();
	}
	while (!httpInit(apn)) {
		serial_write_ln("HTTP init error");
		retry_count ++;
		delay_s(1);
		wdt_reset_count();//reset wdt count
		if (retry_count >= 3) {
			send_failure_sms(data,HTTP_INIT_ERROR);
			return false;
		}
	}
	
	serial_write_ln("HTTP init success");
	serial_write("IP Address is ");
	getIPAddr(ipaddr);
	serial_write_ln(ipaddr);
	char log_post_payload[120] = {0};
	switch(data) {
		case ERROR_LOG_DATA:				
				tRTCGetTimeStamp(timestamp,POST);
				sprintf(urlLink,"http://%s/swachhbharat/simcause",project_server);
				serial_write_ln(urlLink);
				char gsm_init_char[4] = {0};
				EEPROM_read_char(SIM_ERROR_LOG_LOCATION,SIM_ERROR_LEN,gsm_init_char);
				sprintf(log_post_payload,"{\"data\":{\"imei\":\"%s\",\"rc\":\"%d\",\"se\":\"%d\",\"ts\":\"%s\"}}",gImeiNo,g_resetCause,atoi(gsm_init_char),timestamp);
				serial_write_ln(log_post_payload);
				serial_write_ln(urlLink);
				if(sendHttpData(urlLink,log_post_payload, POST,ERROR_LOG_DATA, &received_payload_size, BOOTUP_TIMEOUT)) {
					serial_write_ln("sending success!");
					httpClose();
					return true;
				}
				else {
					serial_write_ln("sending failed!");
					httpClose();
					return false;
				}
				break;
		case BOOTUPDATA:
			getImsi(g_ImsiNo);
			serial_write("IMSI NO :");
			serial_write_ln(g_ImsiNo);
			tRTCGetTimeStamp(timestamp,POST);
			char post_bootup_payload[POST_PAYLOAD_SIZE] = {0};
			sprintf(urlLink,"http://%s/sensor/bootup",project_server);
			serial_write_ln(urlLink);
			memset(post_bootup_payload, 0, POST_PAYLOAD_SIZE);
			sprintf(post_bootup_payload,"{\"data\":{\"imei\":\"%s\",\"imsi\":\"%s\",\"lat\":\"%s\",\"lng\":\"%s\",\"type\":\"temperature\",\"hw_v\":\"%s\",\"sw_v\":\"%s\",\"ts\":\"%s\"}}", gImeiNo,g_ImsiNo, latitude, longitude,hardwareVersion, softwareVersion, timestamp);
			serial_write_ln("post payload:");
			serial_write_ln(post_bootup_payload);
			// send data
			if(sendHttpData(urlLink, post_bootup_payload, POST,LIVEDATA, &received_payload_size,LIVE_DATA_SEND_TIMEOUT)) {
				serial_write_ln("sending success");
				httpClose();
				return true;
			}
			else {
				httpClose();
				return false;
				
			}
			
			break;
				
		case LIVEDATA:
			tRTCGetTimeStamp(timestamp,POST);
			char post_payload[POST_PAYLOAD_SIZE] = {0};
			sprintf(urlLink,"http://%s/sensor/live",project_server);
			serial_write_ln(urlLink);
			char tempAdcBuff[10]={0},tempCinBuff[10]={0};
			ftoa(AvgtempAdcVoltage,tempAdcBuff,2);
			ftoa(AvgTempC,tempCinBuff,2);
			
			
			memset(post_payload, 0, POST_PAYLOAD_SIZE);
			sprintf(post_payload,"{\"data\":{\"imei\":\"%s\",\"rssi\":\"%d\",\"ts\":\"%s\"},\"sensor\":[{\"type\":\"temperature\",\"adc_volt\":\"%s\",\"value\":\"%s\",\"units\":\"C\"}]} ", gImeiNo, rssi,timestamp,tempAdcBuff,tempCinBuff);
			serial_write_ln("post payload:");
			serial_write_ln(post_payload);
			// send data
			if(sendHttpData(urlLink, post_payload, POST,LIVEDATA, &received_payload_size,LIVE_DATA_SEND_TIMEOUT)) {
				serial_write_ln("sending success");
				
			}
			else {
				httpClose();
				return false;
				break;
			}
		
		httpClose();
		return true;
			break;
			
		default:
			serial_write_ln("sending type Not matched!");
			return false;
	}

}

/* do static memory allocation */
void tHandleSMS(int8_t messageIndex) {
	
	// Get the SMS content
	char message[SMS_BUFFER_SIZE];
	memset(message, 0 , SMS_BUFFER_SIZE);
	char mobileNumber[MOBILE_NUMBER_SIZE + 5];
	memset(mobileNumber,0, MOBILE_NUMBER_SIZE +5);
	char dateTime[35];
	memset(dateTime, 0 , 35 * sizeof(char));
	
	readSMS(messageIndex, message, SMS_BUFFER_SIZE, mobileNumber,dateTime);		//#CHANGES_3P1
	strupr(message);
	
	serial_write("     FROM                : ");
	serial_write_ln(mobileNumber);
	
	serial_write("     MSG CONTENT         : ");
	serial_write_ln(message);
	
	deleteSMS(messageIndex);

	delay_ms(100);
	
	//const char* gatewayReceiveNumber   = "RM-WAYSMS";
	//const char* gatewaySendNumber	     = "9891024477";
	
	// checking the content of the message
	if(NULL != strstr(message,"STATUS")) {
		// Should give the status of the device
		char sendMsgPtr[SMS_BUFFER_SIZE] = {0}; //#CHANGES_3P1
		int  rssi = 0;
		char rssiInChar[10] = {0};
			
		// sent AT+CSQ
		getSignalStrength(&rssi);
		serial_write("Signal Strength is ");
			
		// construct the status string LAT, LONG, IMEI, RSSI, APN, Set time, Batt status, button count
		sprintf(sendMsgPtr,"LAT:%s\nLONG:%s\nRSSI:%d\nAPN:%s\nURL:%s\n",latitude,longitude,rssi,apn,project_server);
		serial_write_ln("Msg Content :");
		serial_write_ln(sendMsgPtr);
		if((NULL != strstr(mobileNumber,gatewayReceiveNumber)) || (NULL != strstr(mobileNumber,gatewayReceiveNumber_1))) {	//#CHANGES_3P1
			serial_write_ln("Sending SMS to Gateway");
			sendSmsWithDeviceID(gatewaySendNumber,sendMsgPtr); //#CHANGES_3P1
		}
		else {
			serial_write_ln("Sending SMS to Other Number");
			sendSmsWithDeviceID(mobileNumber,sendMsgPtr); //#CHANGES_3P1
		}
			
		tGsmPortCleanBuffer(mobileNumber, MOBILE_NUMBER_SIZE );
		serial_write_ln("Sending device status");
	}
	else if((NULL != strstr(mobileNumber,gatewayReceiveNumber)) || (NULL != strstr(mobileNumber,gatewayReceiveNumber_1)) ||(NULL != strstr(mobileNumber,gatewayReceiveNumber_2))) {			//#SMSG
		char sms_content[SMS_BUFFER_SIZE] = {0};
		char timestamp[TIMEBUFFERSIZE] = {0};
		tRTCGetTimeStamp(timestamp,POST);
		if(NULL != strstr(message,"REBOOT")) {

			sprintf(sms_content,"REBOOTED,MOB:%s,ts:%s",mobileNumber,timestamp);
			sendSmsWithDeviceID(gatewaySendNumber,sms_content);
			serial_write_ln("Restarting The System");
			complete_system_reset();
		}			
		else {
			sprintf(sms_content,"INVALID CMD,MOB:%s,ts:%s",mobileNumber,timestamp);
			sendSmsWithDeviceID(gatewaySendNumber,sms_content);
			serial_write_ln("Invalid Command");
			delay_ms(3000);
		}		
	}
	serial_write_ln("SMS handling done");
}

bool tGsmTime(void) {
	char year_temp[3]	= {0},
		 month_temp[3]	= {0},
		 day_temp[3]	= {0},
		 hour_temp[3]	= {0},
		 minute_temp[3] = {0},
		 sec_temp[3]	= {0};
	char date[GSMBUFFER40] = {0};
	
	if ( true == getDateTime(date)) {
		serial_write("GSM TIME                 : ");
		serial_write_ln(date);
		year_temp[0]  = date[0];   // Year
		year_temp[1]  = date[1];
		year_temp[2]  = '\0';
		time_gsm.year = atoi(year_temp);
		
		month_temp[0]  = date[3];   // month
		month_temp[1]  = date[4];
		month_temp[2]  = '\0';
		time_gsm.month = atoi(month_temp);
		
		day_temp[0]  = date[6];   // day
		day_temp[1]  = date[7];
		day_temp[2]  = '\0';
		time_gsm.day = atoi(day_temp);

		hour_temp[0]  = date[9];   // hour
		hour_temp[1]  = date[10];
		hour_temp[2]  = '\0';
		time_gsm.hour = atoi(hour_temp);
		
		minute_temp[0]  = date[12];   // minute
		minute_temp[1]  = date[13];
		minute_temp[2]  = '\0';
		time_gsm.minute = atoi(minute_temp);

		if(date[14]>= '0' && date[14] <= '9') {
			sec_temp[0]= date[14];
		}
		else {
			sec_temp[0] = '0';
		}
		sec_temp[1] = date[15];
		sec_temp[2] = '\0';
		time_gsm.second = atoi(sec_temp);
		return true;
	}
	else
		return false;
}


bool tInitHardware(void) {
	
	tButtonConfiguration();
	tEEPROMSetup();
	//EEPROM_Write(SIM_ERROR_LOG_LOCATION,TIMEBUFFERSIZE,"");
	int init_config_loc = 0;
	//checking the init configuration done are not
	EEPROM_read_num(INIT_CONFIG_LOCATION,&init_config_loc);
	if (init_config_loc != 9)
	{
		serial_write_ln("WRITING INTIAL CONFIG    : SUCCESSFUL");
		EEPROM_write(INIT_CONFIG_LOCATION,2,"9");
		EEPROM_clear(SIM_ERROR_LOG_LOCATION,SIM_ERROR_LEN);
		EEPROM_write(SIM_ERROR_LOG_LOCATION,2,"0");
	}

	char gsm_init_char[4] = {0};
	EEPROM_read_char(SIM_ERROR_LOG_LOCATION,SIM_ERROR_LEN,gsm_init_char);
	//EEPROM_read_num(SIM_ERROR_LOG_LOCATION,SIM_ERROR_LEN,gsm_init_char);
	serial_write("SIM ERROR LOG CODE       : ");
	serial_write_int_ln(atoi(gsm_init_char));	
	adc_init_setup(BAT_ADC_PIN);//adc initial setup for BATTERY read
	
	adc_init_setup(TEMP_ADC_PIN);
	serial_write_ln("ADC CONFIGURATION        : SUCCESSFUL");
	if ((g_resetCause == SYSTEM_RESET_CAUSE_EXTERNAL_RESET) && gsmCheckPowerUp())
	{
		serial_write_ln("         (turning OFF the GSM)");
		gsmPowerOnOff(OFF);
	}
	return tGsmHardwareInit();
}

bool tGsmHardwareInit(void)	 {	
	uint8_t retry = 0;
	
	while(retry < 3) {
		 if(!gsmCheckPowerUp()) {
			 serial_write_ln("         (trying to power ON the GSM)");
			 gsmPowerUpDown(GSM_PWR_KEY,ON);
		 }
		 else {
			 break;
		 }
		 retry++;
		 if(retry == 3)
			return false;
	 }

	 uint8_t gsm_retry = 0;
	 uint8_t gsm_init_status = 0;
	 gsm_init_status = GSM_init();
	 
	 while(gsm_init_status == 0 ||(gsm_init_status == CPIN_ERROR) ) {
		 gsm_init_status = GSM_init();
		  gsm_retry++;
		 serial_write_ln("     error: gsm initialization failed. Retrying ...");
		 delay_ms(2000);
		 
		 if(gsm_retry >= 3) {
			 serial_write_int_ln(gsm_init_status);
			 char num[4] = {0};
			 itoa(gsm_init_status,num,10);
			 g_simErrorCode = 1;
			 EEPROM_write(SIM_ERROR_LOG_LOCATION,SIM_ERROR_LEN,num);//	
			 		
			 serial_write_ln("GSM INITIALIZATION       : FAILED");
			 return false;
		 }
		 		
	 }
	 serial_write_ln("GSM INITIALIZATION       : SUCCESSFUL");
	 return true;
}

void tButtonConfiguration(void) {

	struct port_config config_port_pin;
	port_get_config_defaults(&config_port_pin);
	
	// POWER KEY
	//config_port_pin.direction = PORT_PIN_DIR_INPUT;
	//config_port_pin.input_pull = PORT_PIN_PULL_NONE;
	//port_pin_set_config(POWERPIN, &config_port_pin);

	// LED PIN
	config_port_pin.direction = PORT_PIN_DIR_OUTPUT;//#MOTA
	port_pin_set_config(LED, &config_port_pin);
	port_pin_set_output_level(LED,false);
	
	// BUZZER Config
	config_port_pin.direction = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(BUZZER, &config_port_pin);
	port_pin_set_output_level(BUZZER,false);

	// GSM PWR KEY
	config_port_pin.direction = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(GSM_PWR_KEY, &config_port_pin);
	port_pin_set_output_level(GSM_PWR_KEY,false);
	
	// MOTOR PIN 
	config_port_pin.direction = PORT_PIN_DIR_OUTPUT;
	port_pin_set_config(MOTOR_PIN, &config_port_pin);
	port_pin_set_output_level(MOTOR_PIN,false);
	
}
  
void gsmPowerOnOff(uint8_t mode) {
	
	gsmPowerUpDown(GSM_PWR_KEY,mode);
	
	if(mode == ON || mode == REBOOT) {	
		g_SIMCardFlag      = false;
		g_SMSServiceFlag   = false;
		g_InitHardwareFlag = false;
		
		if(tGsmHardwareInit()) {
			g_InitHardwareFlag = true;
			serial_write_ln("GSM HARDWARE INTIALIZATION : SUCCESSFUL");
	
			if(tSimCardSetup()) {
				g_SIMCardFlag = true;
				serial_write_ln("SIMCARD SETUP            : SUCCESSFUL");
				
				if(tSmsServiceSetup()) {
					g_SMSServiceFlag = true ;
					serial_write_ln("SMS SERVICE SETUP         : SUCCESSFUL");
				}
				else {
					g_SMSServiceFlag = false;
					serial_write_ln("SMS SERVICE SETUP         : FAILED");
				}
			}
			else {
				g_SIMCardFlag = false;
				serial_write_ln("SIMCARD SETUP            : FAILED");
			}
		}
		else {
			serial_write_ln("GSM HARDWARE INTIALIZATION : FAILED");
		}
	}
}

bool tCheckGpsLatLonValidity(char *longitude_temp , char *latitude_temp) {
	
  if((strlen(longitude_temp) > 5) && (strlen(latitude_temp) > 5)) {
    return true;
  }
  else
    return false;
}

int tcheckNumberValid(char *msg, int From, int To) {	
	int i = 0,c = 0;
	
	for(i= From; i <= To; i++) {
		int k = msg[i] - '0';
		if(k >= 0 && k < 10) {
			c++;
		}
	}

	if(c == (To - From + 1)) {
#ifdef DEBUG
		serial_write_ln("IS NUMBER VALID          : YES");
#endif
		return 1;
	}
	else {
#ifdef DEBUG
		serial_write_ln("IS NUMBER VALID          : NO");
#endif
		return 0;
	}
}

void tDeviceInfo(void) {
	serial_write("PRODUCT NAME             : ");
	serial_write_ln(projectName);
	serial_write("COMPANY NAME             : ");
	serial_write_ln(companyName);
	serial_write("SOFTWARE VERSION         : ");
	serial_write_ln(softwareVersion);
	serial_write("HARDWARE VERSION         : ");
	serial_write_ln(hardwareVersion);
	serial_write("API VERSION              : ");
	serial_write_ln(apiVersion);
#ifdef ENABLE_OTA
	serial_write("PRESENT ADDRESS          : ");
	serial_write_ln(appAddress);
	serial_write("TOTAL RAM SIZE           : ");
	serial_write_int_ln(USED_TOTAL_RAM_SIZE);
#endif
	serial_write_ln("");
}

// why config_wdt is here it should be in samd20j.c
void configure_wdt(void) {
	/* Create a new configuration structure for the Watchdog settings and fill
	 * with the default module settings. */
	//! [setup_1]
	struct wdt_conf config_wdt;
	//! [setup_1]
	//! [setup_2]
	wdt_get_config_defaults(&config_wdt);
	//! [setup_2]

	/* Set the Watchdog configuration settings */
	//! [setup_3]
	config_wdt.always_on      = false;				// why false?
#if !((SAML21) || (SAMC21) || (SAML22) || (SAMR30))
	config_wdt.clock_source   = GCLK_GENERATOR_4;
#endif
	//! [setup_3]
	/* Initialize and enable the Watchdog with the user settings */
	//! [setup_4]
	wdt_set_config(&config_wdt);
	//! [setup_4]
}

#ifdef  ENABLE_OTA
	int updateToFlash(char *path,char *server_binFile_checksum) {
		// check if below variables are required
		/*
		uint32_t curr_prog_addr;
		uint32_t disk_dev_num;
		*/
		int http_retry = 0;
		// Check if nvm_config is required?
		struct nvm_config config;
	
		while (!httpInit(apn)) {
			serial_write_ln("http init error");

			if (http_retry > 4) {
				serial_write_ln("http init Failed");
				return HTTP_INIT_ERROR;
				break;
			}
			http_retry++;
			delay_ms(2000);
		}
	
		serial_write_ln("http init success");
		int retry_count = 0;
		unsigned int file_size = 0;
		while(retry_count < 4) {	//download file from path
			uint16_t http_response = httpSend(path," ",GET, &file_size, FILE_DOWNLOAD_TIMEOUT);
			if (200 == http_response)
			{
				break;
			}
			else{
				serial_write_ln("Receiving Failed!");
				if(retry_count > 2) {
					return http_response;
					break;
				}
			}
			delay_ms(2000);
			retry_count++;
		}
		if(file_size > OTA_MIN_BIN_FILE_SIZE && file_size < OTA_MAX_BIN_FILE_SIZE) { //file size should be more than 30kb and less than 100kb
			serial_write_ln("Receive success");
			serial_write("file size:");
			serial_write_int_ln(file_size);
		}
		else {
			httpClose();
			serial_write_ln("Receive Failed");			
			return OTA_FILE_SIZE_ERROR;
		}
		retry_count = 0;
		while (retry_count<4) {
			int ota_response = startOTAUpdate(file_size,server_binFile_checksum);
			if (OTA_UPDATE_SUCCESS == ota_response)
			{
				httpClose();
				serial_write_ln("SUCCESS! writing Success");
				return OTA_UPDATE_SUCCESS;
				break;
			}
			else{
				serial_write_ln("OTA update failed!");
				if(retry_count > 2) {
					httpClose();
					return ota_response;
					break;
				}
			}		
			retry_count ++;
		}
	}

	uint8_t checkForFirmwareUpdate(char *receivedPath,char *server_binFile_checksum) {
		
		int tempCount = 0;
		while(!startGprsConnection(apn)){
			
			// feed watchdog timer
			wdt_reset_count();
			serial_write_ln("GPRS connection Failed");
			if (tempCount > 3) {
				
				TCPclose();
				return false;
				break;
				
			}
			delay_ms(2000);
			tempCount++;
		}
	
		wdt_reset_count();
		serial_write_ln("GPRS connection Success!");
		tempCount = 0;
	
		while(!startTCPconnection(ota_server,80)) {
			serial_write_ln("TCP connection Failed");
			//feed wdt
			wdt_reset_count();
			if (tempCount > 3) {
				TCPclose();
				return false;
				break;
			}
			tempCount++;
		}

		serial_write_ln("TCP connection success");

		char Data[PRESENT_FIRMWARE_DETAILS_SIZE] = {0};
		sprintf(Data,"%simei:%s\nproj:%s\nhw_v:%s\nsw_v:%s\nb_addr:%d\r\n\r\n",firmware_update_check_url,gImeiNo,ota_project_name,hardwareVersion,softwareVersion,PRESENT_ADDR);
		
		//feed wdt
		wdt_reset_count();
	
		if(!sendData(Data,receivedPath,server_binFile_checksum)) {
			serial_write_ln("No firmware update");
			TCPclose();
			return false;
		}
	
		serial_write_ln("firmware update available url:");
		serial_write_ln(receivedPath);
		serial_write("checksum:");
		serial_write_ln(server_binFile_checksum);
		TCPclose();
		return true;
	}

	void checkOTAUpdate(void) {		
		
			char Newfirmware[NEW_FIRMWARE_URL_SIZE] = {0};
			char server_binFile_checksum[CHECKSUM_SIZE] ={0};
			if(checkForFirmwareUpdate(Newfirmware,server_binFile_checksum)) {
				char ota_update_status_sms[30] ={0};
				int ota_update_status = updateToFlash(Newfirmware,server_binFile_checksum);
				switch (ota_update_status)
				{	
					case OTA_UPDATE_SUCCESS:
						//if present application is 0x00022000 update with "2",else we have to update with "9"
						if(OTA_EEPROM_UPDATE_SUCCESS) {
							//EEPROM_write(INIT_CONFIG_LOCATION,2,"0");
							sendSmsWithDeviceID(gatewaySendNumber,"OTA UPDATE SUCCESS!");
							serial_write_ln("OTA UPDATE SUCCESS!");
							delay_ms(1000);
							serial_write_ln("Wait for 10s!!! restarting!");
							//delay_ms(1000);
							OTA_RESET;
						}
						else {
							sendSmsWithDeviceID(gatewaySendNumber,"OTA FAILED!,EEPROM write ERROR");
							serial_write_ln("OTA UPDATE FAILED!,EEPROM write ERROR");
						}
						break;
					case HTTP_INIT_ERROR:
						 sendSmsWithDeviceID(gatewaySendNumber,"OTA UPDATE FAILED!,HTTP INIT ERROR!");
						 OTA_EEPROM_UPDATE_FAILED;
						 serial_write_ln("OTA UPDATE FAILED!,HTTP INIT ERROR!");
						 
						 break;
					default:						
						sprintf(ota_update_status_sms,"OTA UPDATE FAILED!,CODE:%d",ota_update_status);
						sendSmsWithDeviceID(gatewaySendNumber,ota_update_status_sms);
						
						OTA_EEPROM_UPDATE_FAILED;
						serial_write_ln(ota_update_status_sms);	
										
						break;						 
				}
			}
		}
			
#endif

bool tReadImei(char *imei){
	int retry_count = 0;
	while(retry_count++ < 4){
		getImei(imei);
		if (tcheckNumberValid(imei,0,14)) {
			g_IMEIvalidFlag = true;
			return true;
		}
	}
	g_IMEIvalidFlag = false;
	return false;
}


bool tgsmClkSync(void) {
	char gprsBuffer[GSMBUFFER40] = {0};
	
	tGsmPortSendCmd("AT+CCLK?\r\n");
	tGsmPortCleanBuffer(gprsBuffer,GSMBUFFER40);
	tGsmPortReadData(gprsBuffer,GSMBUFFER40,DEFAULTTIMEOUT);
	char *startPtr1=NULL,*endPtr1=NULL;
	char tempDateBuffer1[10]={0};
	startPtr1=strstr(gprsBuffer,"\"");
	endPtr1=strstr(gprsBuffer,",");

	if ( (startPtr1 )!= NULL && (endPtr1) !=NULL ) {
		uint8_t i =0;
		startPtr1++;
		while(startPtr1 != endPtr1){
			tempDateBuffer1[i++]=*startPtr1++;
		}
		tempDateBuffer1[i]='\0';
	}
	serial_debug_ln(tempDateBuffer1,STR,GSM_DBG_LEVEL);

	char dateVariable1[3][3];
	char *tokenTime1 = strtok(tempDateBuffer1,"/");
	uint8_t i1=0;
	while (tokenTime1 != NULL) {
		strcpy(dateVariable1[i1++],tokenTime1);
		tokenTime1 = strtok(NULL,"/");
	}
	serial_write("YEAR                     : ");
	serial_write_ln(dateVariable1[0]);
	serial_write("MONTH                    : ");
	serial_write_ln(dateVariable1[1]);
	serial_write("DATE                     : ");
	serial_write_ln(dateVariable1[2]);

	if ((atoi(dateVariable1[0])+2000) >= DEFAULT_YEAR)  {
		serial_write_ln("CLOCK SYNCHRONIZATION    : SUCCESSFUL");
		return true;
	}
	tGsmPortSendCmd("AT+CLTS?\r\n");
	tGsmPortCleanBuffer(gprsBuffer,GSMBUFFER40);
	tGsmPortReadData(gprsBuffer,GSMBUFFER40,DEFAULTTIMEOUT);

	tGsmPortSendCmd("AT+CCLK?\r\n");
	tGsmPortCleanBuffer(gprsBuffer,GSMBUFFER40);
	tGsmPortReadData(gprsBuffer,GSMBUFFER40,DEFAULTTIMEOUT);
	serial_write("AT+CCLK?");
	serial_write_ln(gprsBuffer);

	tGsmPortSendCmd("AT+CLTS=1;&W\r\n");
	tGsmPortCleanBuffer(gprsBuffer,GSMBUFFER40);
	tGsmPortReadData(gprsBuffer,GSMBUFFER40,DEFAULTTIMEOUT);
	serial_write("AT+CLTS?");
	serial_write_ln(gprsBuffer);

	tGsmPortSendCmd("AT+CFUN=1,1\r\n");
	tGsmPortCleanBuffer(gprsBuffer,GSMBUFFER40);
	tGsmPortReadData(gprsBuffer,GSMBUFFER40,DEFAULTTIMEOUT);
	serial_write("AT+CFUN=1,1");
	serial_write_ln(gprsBuffer);
	gsmPowerUpDown(GSM_PWR_KEY,ON);
	int retry = 0;
	while(retry < 3)
	{
		if(!gsmCheckPowerUp()) {
			serial_write_ln("     (trying to power ON GSM)");
			gsmPowerUpDown(GSM_PWR_KEY,ON);
			delay_s(2);
		}
		else {
			break;
		}
		retry++;
		if(retry == 3)
		break;
	}
	delay_ms(5000);
	tGsmPortSendCmd("AT+CCLK?\r\n");
	tGsmPortCleanBuffer(gprsBuffer,GSMBUFFER40);
	tGsmPortReadData(gprsBuffer,GSMBUFFER40,DEFAULTTIMEOUT);
	serial_write("AT+CCLK?");
	serial_write_ln(gprsBuffer);
	char *startPtr=NULL,*endPtr=NULL;
	char tempDateBuffer[10]={0};
	startPtr=strstr(gprsBuffer,"\"");
	endPtr=strstr(gprsBuffer,",");

	if ( (startPtr )!= NULL && (endPtr) !=NULL ) {
		uint8_t i =0;
		startPtr++;
		while(startPtr != endPtr){
			tempDateBuffer[i++]=*startPtr++;
		}
		tempDateBuffer[i]='\0';
	}
	serial_write_ln(tempDateBuffer);

	char dateVariable[3][3];
	char *tokenTime = strtok(tempDateBuffer,"/");
	uint8_t i=0;
	while (tokenTime != NULL) {
		strcpy(dateVariable[i++],tokenTime);
		tokenTime = strtok(NULL,"/");
	}
	serial_write("YEAR                     : ");
	serial_write_ln(dateVariable[0]);
	serial_write("MONTH                    :");
	serial_write_ln(dateVariable[1]);
	serial_write("DATE                     :");
	serial_write_ln(dateVariable[2]);

	if ((atoi(dateVariable[0])+2000) >= DEFAULT_YEAR)  {
		
		serial_write_ln("CLOCK SYNCHRONIZATION    : SUCCESSFUL");
		return true;
	}
	else {
		serial_write_ln("CLOCK SYNCHRONIZATION    : FAILED");
		sendSmsWithDeviceID(gatewaySendNumber,"time Sync Failed!");
		return false;
	}


}


bool sendSmsWithRetry(char *number, const char *sms_content){
	int count = 0;
	while (!sendSmsWithDeviceID(number,sms_content)){	
		serial_write_ln("sms sending Failed!");
		delay_ms(3000);
		if (count>2){
			return false;
			break;
		}
		count ++;
	}
	return true;
}

void complete_system_reset(void) {
	
	
	
	serial_write_ln("GSM powering down...");
	gsmPowerUpDown(GSM_PWR_KEY,OFF);

	serial_write_ln("control resetting...");
	delay_ms(2000);
	system_reset();
}

bool tempReading(void) {
	
	long  adcRawValue=0,  avgAdcRawValue = 0;
	
	uint8_t i=0;
	double  r2=0 , r1 = 4700.0 , a1 = 3.354016E-03 , b1 = 2.569850E-04 , c1 = 2.620131E-06 , d1 = 6.383091E-08 ,  tempF, tempK;
	char buffer[30]={0};
	for(i=0;i<TEMP_READ_SAMPLE;i++){
		adcRawValue += adc_read_value(TEMP_ADC_PIN);
	}
	avgAdcRawValue = adcRawValue / TEMP_READ_SAMPLE;
	char buff[10]={0};
	tempAdcVoltage = (float)avgAdcRawValue * ( VREF_VOLTAGE / ADC_RESOLUTION);
	ftoa(tempAdcVoltage,buff,2);
	serial_write("\r\nTEMP ADC VOLTAGE         : ");
	serial_write_ln(buff);
	
	r2			=  (tempAdcVoltage*r1)/(VREF_VOLTAGE-tempAdcVoltage) ;
	ftoa(r2,buffer,6);
	serial_write("RESISTANCE               : ");
	serial_write_ln(buffer);
	
	tempK = 1.0 / (a1 + (b1 * (log(r2 / 5000.0))) + (c1 * pow(log(r2 / 5000.0), 2.0)) + (d1 * pow(log(r2 / 5000.0), 3.0)));
	ftoa(tempK,buffer,6);
	//serial_write("TEMPERATURE IN KELVIN    : ");
	//serial_write(buffer);
	//serial_write_ln(" K");
	
	tempC  = (tempK - 273.15);
	ftoa(tempC,buffer,6);
	serial_write("TEMPERATURE IN CELSIUS   : ");
	serial_write(buffer);
	serial_write_ln(" C");
	
	tempF  = (tempC * 1.8) + 32.0;
	ftoa(tempF,buffer,6);
	//serial_write("TEMPERATURE IN FAHRENHEIT: ");
	//serial_write(buffer);
	//serial_write_ln(" F");
	
	return true;
		
}

void avgTemperatureVoltageReadings(void){
	tempReading();
	char buffer[30]={0};
	tempAdcVoltageArrray[g_tempSampleCounter]=tempAdcVoltage;
	tempCelArrray[g_tempSampleCounter]=tempC;
	g_tempSampleCounter++;
	if(g_tempSampleCounter>=TEMP_READ_SAMPLE){
		g_tempSampleCounter = 0;
	}
	AvgTempC = 0;
	AvgtempAdcVoltage = 0;
	for(uint8_t tempCount = 0;tempCount<TEMP_READ_SAMPLE;tempCount++) {
		AvgtempAdcVoltage +=tempAdcVoltageArrray[tempCount];
		AvgTempC+=tempCelArrray[tempCount];	
	}
	
	AvgTempC = AvgTempC / TEMP_READ_SAMPLE;
	AvgtempAdcVoltage = AvgtempAdcVoltage  / TEMP_READ_SAMPLE;
	
	ftoa(AvgTempC,buffer,6);
	serial_write("TEMPERATURE IN CELSIUS Av: ");
	serial_write(buffer);
	serial_write_ln(" C");
	
	ftoa(AvgtempAdcVoltage,buffer,6);
	serial_write("TEMP ADC VOLTAGE Avg     : ");
	serial_write(buffer);
	serial_write_ln(" V");
}