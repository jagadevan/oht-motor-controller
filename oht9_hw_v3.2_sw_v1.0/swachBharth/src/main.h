/**
 @mainpage			main.h file
 * - author			thingtronics Innovations Pvt.Ltd
 * - developers		jagadevan, poornima, manohar, Lovelesh Patel.
 * - created			24-06-2017 06:38:59
 * - last Updated		03-04-2018
 * 
 *  SOFTWARE VERSIONS AND CHANGES
 * --------------------------------
 * 1. SWB_SWV_3.7
 * ------------------
 * * changes made date: 24-04-2018
 * * crc32B checksum added for OTA
 * * error sms stopped sending to SMS GW
 */


#ifndef MAIN_H_
#define MAIN_H_

#include "conf_main.h"
#include <asf.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include "Gsmportsetup.h"           //Add GSM Library
#include "Gsmlibrary.h"
#include "samd20j.h"                // Configuration file
#include <ctype.h>
#include <wchar.h>
#include "conf_ota.h"
#include "adc_drivers.h"

/** * EEprom location for saving sim related errors*/
#define SIM_ERROR_LOG_LOCATION			280
/** * length of sim error code in bytes*/
#define SIM_ERROR_LEN					3
/** * EEprom location for default configurations*/
#define INIT_CONFIG_LOCATION			270

/* Response codes Allocated as */
// HTTP		 - 10 to 19
// TCP       - 20 to 29
// OTA		 - 30 to 39
// EEPROM	 - 50 to 49
// BOOTUP	 - 60 to 69
// SIM_ERRORS- 70 to 79
/************************************************************************/
/* BOOTUP RESPONSE CODES                                                */
/************************************************************************/

#ifndef INVALID_LAT_LONG
	#define INVALID_LAT_LONG	60
#endif
/** * BOOTUP data sending timeout*/
#define BOOTUP_TIMEOUT		10 //in sec's'



#define  SETNORMALTIME   	0
#define  SETREDUCEDTIME   	1
#define  BOOTUPDATA		  	0
#define  ERROR_LOG_DATA     3
#define  LIVEDATA         	1
#define  PWRDATA            2



// timeout s
/** * periodic time(in minutes) to send feedback data to cloud */
#define	 DATA_SEND_TIME     5
/** * wait time(in millis) between conscutive button press*/			
#define  INTERRUPTTIMEOUT 	30000
/** * periodic time(in millis) to check sms in phonebook*/
#define  MSGREADINTERVAL      30  * ONE_SEC
/** * buzzer on time in millis */
#define BUZZER_ON_TIME              500


/** * additional stack size for freertos tasks*/
#define  ADDITIONAL_STACK   1500
/** * BOD33 threshold level is about 3.2V*/
#define  BODLEVEL			48

#define  ONE_SEC            1000
#define  HOURS_PER_DAY      24
#define  MINUTE				60   * ONE_SEC
#define  HOUR				60   * MINUTE
#define  DAY				HOUR * HOURS_PER_DAY




// PIN CONFIGURATIONs
/** * buzzer gpio pin */
#define BUZZER						PIN_PA04
/** * led gpio pin */
#define LED							PIN_PA18
/** * red button gpio pin */	

/** * mains power read pin */

/** * ldr gpio pin */

#define MOTOR_PIN                   PIN_PA28
#define MOTOR_ON				    port_pin_set_output_level(MOTOR_PIN,HIGH);
#define MOTOR_OFF				    port_pin_set_output_level(MOTOR_PIN,LOW);


#define	 TEMP_ADC_PIN	            ADC_POSITIVE_INPUT_PIN18




/** * make it buzzer pin high */
#define BUZZER_ON				    port_pin_set_output_level(BUZZER,HIGH);
/** * make it buzzer pin low */
#define BUZZER_OFF					port_pin_set_output_level(BUZZER,LOW);
/** * disable all interrupt buttons */




/** * will return number of tick counts(in millis) from program started*/
#define millis()                    xTaskGetTickCountFromISR()

#define DEFAULT_YEAR   2017
#define DEFAULT_MONTH  01
#define DEFAULT_DAY    01
#define DEFAULT_HOUR   12
#define DEFAULT_MINUTE 00
#define DEFAULT_SECOND 00


/**  * ADC Configuration*/
/** * Number of samples has to read */
#define  ADC_SAMPLES      		128
/** * battery save mode cutoff percentage voltage */
#define  CUTTOFF_PERCENT        15
/** * voltage divider factor 
  *	* this will be calculated as (R1+R2)/R1
  * * here R1 = 10K and  R2= 33k
*/
#define  DIVIDER_FACTOR			4.3
#if BATTERY_TYPE
/** *  max shutdown voltage for lipo battery device */
#define  MAX_BATT_VOLTAGE		4.3
/** *  min shutdown voltage for lipo battery device */
#define  MIN_BATT_VOLTAGE		2.8
#else
/** *  max shutdown voltage for leadAcid battery device */
#define  MAX_BATT_VOLTAGE		7.2
/** *  min shutdown voltage for leadAcid battery device */
#define  MIN_BATT_VOLTAGE		5.8
#endif
#define  BATT_VOLTAGE_RANGE		MAX_BATT_VOLTAGE - MIN_BATT_VOLTAGE
#define  VREF_VOLTAGE			3.3
#define  ADC_RESOLUTION			4096
#define  CUTTOFF_VALUE          ((((float)(BATT_VOLTAGE_RANGE ) * ((float)CUTTOFF_PERCENT) / 100) + MIN_BATT_VOLTAGE) / DIVIDER_FACTOR)  * (ADC_RESOLUTION/VREF_VOLTAGE)



#define POST_PAYLOAD_SIZE				200 

#define ALL_SMS                  0



#define	BAT_ADC_PIN				ADC_POSITIVE_INPUT_PIN0


#define TEMP_READ_SAMPLE        30

// Prototypes
bool tInitHardware(void);
bool tGsmHardwareInit(void);
void tButtonConfiguration(void);
void tExternalInterruptConfiguration(void);
void tButtonInterruptEnable(void);
void tButtonInterruptDisable(void);
void tButtonRedCallBack(void);
void tButtonYellowCallBack(void);
void tButtonGreenCallBack(void);


void tBatteryLowCallback(struct adc_module *const module);
int tGetBatteryValue(void);
void configure_wdt(void);

void tGSMConfiguration(void);
bool tSimCardSetup(void);
bool tSmsServiceSetup(void);
void tHandleSMS(int8_t messageIndex);

//  EEPROM CONFIGURATIONS
bool tEEPROMConfiguration(void);
bool tEEPROMSetup(void);


bool tDeviceConfigSetup (void);
bool tWriteInitialConfig(void);

void tRTCConfiguration(void);
void tRTCInit(void);
void tRTCCallbackConfiguration(void);
void tRTCSetAlarmTime(bool AlarmPeriodicityFlag);
void tRTCUpdateTime(unsigned int year,unsigned int month,unsigned int day,unsigned int hour,unsigned int minute,unsigned int second);
void tRTCCallBackFunction(void);
void tRTCSetIntialAlarmTime(void);

bool tSendBootupData(void);
bool tCheckGpsLatLonValidity(char *longitude , char *latitude);
bool tSendDataToCloud(uint8_t data);
bool tSendLiveData(void);
void gsmPowerOnOff(uint8_t mode);
int  tcheckNumberValid(char *msg,int From,int To);

bool tGsmTime(void);
bool tReadImei(char *imei);
bool tgsmClkSync(void);


void main_task(void *params);

void tDeviceInfo(void);
void checkNumOfContacts(int *NumContact);

void		adc_setup(void);
void configure_adc_callbacks(void);
bool sendSmsWithRetry(char *number, const char *sms_content);
	
#ifdef ENABLE_OTA
	int updateToFlash(char *path,char *server_binFile_checksum);//#MOTA
	uint8_t checkForFirmwareUpdate(char *receivedPath,char *server_binFile_checksum);//#MOTA
	void checkOTAUpdate(void);//#MOTA
#endif
bool tempReading(void);
void avgTemperatureVoltageReadings(void);

/*************************************/


#define MAX_RAM_ALLOCATE_APPLICATION	26624			//25K out of 32kb
#define APPLICATION_RAM_SIZE			2048			//expected ram size

/*****RAM MEMORY ALLOCATION ************/
/*	ota				- BIN_DATA_SIZE + OTA_BUFFER_SIZE + 300 ---> this is local
 *  ringbuffer		- POST_PAYLOAD_SIZE(local) + (FB_STACK_SIZE*FEEDBACK_ENTRY_SIZE)(global variable)
 *  Application(Expected) - 2048(2kb)
 */

#ifdef	ENABLE_OTA
	#define USED_TOTAL_RAM_SIZE			   (POST_PAYLOAD_SIZE + 300   + BIN_DATA_SIZE + OTA_BUFFER_SIZE + 300 + APPLICATION_RAM_SIZE)
#else
	#define USED_TOTAL_RAM_SIZE			   (POST_PAYLOAD_SIZE + 300   + APPLICATION_RAM_SIZE)
#endif

#if USED_TOTAL_RAM_SIZE > MAX_RAM_ALLOCATE_APPLICATION
	#error "Please reduce memory size in RingBuffer.h"
	#error "RAM overflowing more than 25kb"
#endif

struct dateStruct
{
	unsigned int year;
	uint8_t    month;
	uint8_t    day;
	uint8_t    hour;
	uint8_t    minute;
	uint8_t    second;
	
};

struct rtc_module				rtc_instance;
struct rtc_calendar_alarm_time	alarm;
struct rtc_calendar_time		time;

struct extint_events			extint_event_conf7;
struct extint_events			extint_event_conf6;
struct extint_events			extint_event_conf5;
struct   adc_module adc_instance;


#endif /* MAIN_H_ */
