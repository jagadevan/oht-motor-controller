/*
 * uart_drivers.c
 *
 * Created: 1/10/2018 3:28:53 PM
 *  Author: bomma
 */ 
#include "uart_drivers.h"

//samd20 serial port definition
struct usart_module usart_instance;

void debug_uart_init(void) {
	struct usart_config config_usart;
	usart_get_config_defaults(&config_usart);
	
	// UART  rx = PA09  tx = PA08
	config_usart.baudrate    = BAUD_RATE;
	config_usart.mux_setting = USART_RX_1_TX_0_XCK_1;
	config_usart.pinmux_pad0 = PINMUX_PA08C_SERCOM0_PAD0;
	config_usart.pinmux_pad1 = PINMUX_PA09C_SERCOM0_PAD1;
	config_usart.pinmux_pad2 = PINMUX_UNUSED;
	config_usart.pinmux_pad3 = PINMUX_UNUSED;
	while (usart_init(&usart_instance, SERCOM0, &config_usart) != STATUS_OK) {
	}	
	usart_enable(&usart_instance);
}

bool serial_available(void) {
	if (usart_read_available(&usart_instance) == STATUS_OK) {
		return true;
	}
	else {
		return false;
	}
}

void serial_write(const char * string_buf) {
	usart_write_buffer_wait(&usart_instance, string_buf, strlen(string_buf));
}

void serial_debug(void *string,uint8_t format,uint8_t debug_level) {
	switch (format)
	{
	case DEC:
		if(debug_level <= DEBUG_LEVEL)serial_write_int(string);			
		break;
	case STR:
		if(debug_level <= DEBUG_LEVEL)serial_write(string);
		break;
	case HEX:		
		if(debug_level <= DEBUG_LEVEL)serial_write_hex(string);
		break;
	default:
		serial_write(string);			
	}
}

void serial_debug_ln(void *string,uint8_t format,uint8_t debug_level) {
	serial_debug(string,format,debug_level);
	serial_debug("\n",STR,debug_level);	
}


void serial_write_char(uint16_t c) {
	while (usart_write_wait(&usart_instance, c) != STATUS_OK) {
	}
}

void serial_write_int(uint32_t num) {
	char string_buf[16] = {0};
	itoa(num,string_buf,10);
	serial_write(string_buf);
}

void serial_write_hex_ln(uint32_t num){
	serial_write_hex(num);
	serial_write("\n");
}

void serial_write_hex(uint32_t num) {
	char string_buf[16] = {0};
	sprintf(string_buf,"%08lx",num);
	serial_write(string_buf);
}

void serial_write_float(float num) {
	char string_buf[16] = {0};
	sprintf(string_buf,"%f",num);
	serial_write(string_buf);
}
void serial_write_float_ln(float num) {
	serial_write_float(num);
	serial_write("\n");
}

void serial_write_int_ln(uint32_t num) {
	serial_write_int(num);
	serial_write("\n");
}

void serial_write_ln(char * string_buf) {
	serial_write(string_buf);
	serial_write("\n");
}

void itoa1(uint32_t n, char s[]) {
	uint32_t i = 0, sign=0;
	
	if ((sign = n) < 0) {	// record sign
		n = -n;          // make n positive
	}
	
	i = 0;
	do {       /* generate digits in reverse order */
		s[i++] = n % 10 + '0';   /* get next digit */
		} while ((n /= 10) > 0);     /* delete it */
		
		if (sign < 0) {
			s[i++] = '-';
		}
		s[i] = '\0';
		reverse1(s);
	}

	void reverse1(char s[]) {
		uint32_t i, j;
		char c;
		
		for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
			c = s[i];
			s[i] = s[j];
			s[j] = c;
		}
	}

	uint16_t serial_read_char(void) {
		uint16_t c;
		usart_read_wait(&usart_instance, &c);
		return c;
	}

	void  Serial_read_buffer(char* buffer,int count,  unsigned int timeout_buf) {
		
		int i = 0;
		unsigned long int chartimeout = 1500;
		unsigned long int timerStart, prevChar;
		timerStart = xTaskGetTickCountFromISR();
		
		prevChar = 0;
		while(1) {
			while (serial_available()) {
				char c = serial_read_char();
				//delay_ms(5);
				prevChar = xTaskGetTickCountFromISR();
				buffer[i++] = c;
				
				if(i >= count) {
					break;
				}
			}
			if(i >= count) {
				break;
			}
			
			if ((xTaskGetTickCountFromISR() - timerStart) > timeout_buf*1000UL) {
				break;
			}
			//If interchar Timeout => return FALSE. So we can return sooner from this function. Not DO it if we dont recieve at least one char (prevChar <> 0)
			if (((xTaskGetTickCountFromISR() - prevChar) > chartimeout) && (prevChar != 0)) {
				break;
			}
		}
		buffer[i]='\0';
	}

	/* reverses a string 'str' of length 'len' */
	void reverse(char *str, int len) {
		int i = 0, j = len - 1, temp;
		while (i < j) {
			temp = str[i];
			str[i] = str[j];
			str[j] = temp;
			i++; j--;
		}
	}

	/* Converts a given integer x to string str[].  d is the number
	* of digits required in output. If d is more than the number
	* of digits in x, then 0s are added at the beginning. */
	int intToStr(int x, char str[], int d) {
		int i = 0;
		while (x) {
			str[i++] = (x % 10) + '0';
			x = x / 10;
		}
		
		// If number of digits required is more, then
		// add 0s at the beginning
		while (i < d)
		str[i++] = '0';
		
		reverse(str, i);
		str[i] = '\0';
		return i;
	}

	void ftoa(float n, char *res, int afterpoint) {
		// Extract integer part
		int ipart = (int)n;
		
		// Extract floating part
		float fpart = n - (float)ipart;
		
		// convert integer part to string
		int i = intToStr(ipart, res, 0);
		
		// check for display option after point
		if (afterpoint != 0) 	{
			res[i] = '.';  // add dot
			
			// Get the value of fraction part upto given no.
			// of points after dot. The third parameter is needed
			// to handle cases like 233.007
			fpart = fpart * power(10, afterpoint);
			
			intToStr((int)fpart, res + i + 1, afterpoint);
		}
	}

	int power(int base,int exp)	{
		if ( exp != 1 )
		return (base * power(base, exp - 1));
	}
