var searchData=
[
  ['bod_5faction_5finterrupt',['BOD_ACTION_INTERRUPT',['../group__asfdoc__sam0__bod__group.html#gga1d490011b1b996bf64713b2a4cc3f437ad4a02b7204d4cbcdbf079db59531c845',1,'bod_feature.h']]],
  ['bod_5faction_5fnone',['BOD_ACTION_NONE',['../group__asfdoc__sam0__bod__group.html#gga1d490011b1b996bf64713b2a4cc3f437a9110593f4ab3101936058213849e6397',1,'bod_feature.h']]],
  ['bod_5faction_5freset',['BOD_ACTION_RESET',['../group__asfdoc__sam0__bod__group.html#gga1d490011b1b996bf64713b2a4cc3f437a2ca89a0c2fad829e2f5710520ef91b8d',1,'bod_feature.h']]],
  ['bod_5fbod33',['BOD_BOD33',['../group__asfdoc__sam0__bod__group.html#ggacb41264956c0606d2ba37ba5ebb23096a775878c476794a1d63843e6c28677ebe',1,'bod_feature.h']]],
  ['bod_5fmode_5fcontinuous',['BOD_MODE_CONTINUOUS',['../group__asfdoc__sam0__bod__group.html#gga45678f71f46296884c7f64bb8283542faf08b3fac34c654c6a859ffde6bbd6d48',1,'bod_feature.h']]],
  ['bod_5fmode_5fsampled',['BOD_MODE_SAMPLED',['../group__asfdoc__sam0__bod__group.html#gga45678f71f46296884c7f64bb8283542fa0e4a6e4326564899bda055412a4c4460',1,'bod_feature.h']]],
  ['bod_5fprescale_5fdiv_5f1024',['BOD_PRESCALE_DIV_1024',['../group__asfdoc__sam0__bod__group.html#gga5a1d67e23851686cfaf6dca6e04c44c6a6297fb28402c39e36fbca965f26bcc1e',1,'bod_feature.h']]],
  ['bod_5fprescale_5fdiv_5f128',['BOD_PRESCALE_DIV_128',['../group__asfdoc__sam0__bod__group.html#gga5a1d67e23851686cfaf6dca6e04c44c6ae667a24509e6f3004c814a1935a687b3',1,'bod_feature.h']]],
  ['bod_5fprescale_5fdiv_5f16',['BOD_PRESCALE_DIV_16',['../group__asfdoc__sam0__bod__group.html#gga5a1d67e23851686cfaf6dca6e04c44c6a843b562b981db40c6492baf03d7805a3',1,'bod_feature.h']]],
  ['bod_5fprescale_5fdiv_5f16384',['BOD_PRESCALE_DIV_16384',['../group__asfdoc__sam0__bod__group.html#gga5a1d67e23851686cfaf6dca6e04c44c6a5da9c9a6a70e11503b558978173214a5',1,'bod_feature.h']]],
  ['bod_5fprescale_5fdiv_5f2',['BOD_PRESCALE_DIV_2',['../group__asfdoc__sam0__bod__group.html#gga5a1d67e23851686cfaf6dca6e04c44c6aa9c41aa4563ae77a6a9587ad9334e4cc',1,'bod_feature.h']]],
  ['bod_5fprescale_5fdiv_5f2048',['BOD_PRESCALE_DIV_2048',['../group__asfdoc__sam0__bod__group.html#gga5a1d67e23851686cfaf6dca6e04c44c6a20216b666c1a88bb4dbe3d99b9782474',1,'bod_feature.h']]],
  ['bod_5fprescale_5fdiv_5f256',['BOD_PRESCALE_DIV_256',['../group__asfdoc__sam0__bod__group.html#gga5a1d67e23851686cfaf6dca6e04c44c6a3a60b235fb1668f53407eb3d0a85f017',1,'bod_feature.h']]],
  ['bod_5fprescale_5fdiv_5f32',['BOD_PRESCALE_DIV_32',['../group__asfdoc__sam0__bod__group.html#gga5a1d67e23851686cfaf6dca6e04c44c6a22b52d36e9147071ed73dc94c82cb243',1,'bod_feature.h']]],
  ['bod_5fprescale_5fdiv_5f32768',['BOD_PRESCALE_DIV_32768',['../group__asfdoc__sam0__bod__group.html#gga5a1d67e23851686cfaf6dca6e04c44c6ac4c48b4ad00136b294e8b8c0ec56980c',1,'bod_feature.h']]],
  ['bod_5fprescale_5fdiv_5f4',['BOD_PRESCALE_DIV_4',['../group__asfdoc__sam0__bod__group.html#gga5a1d67e23851686cfaf6dca6e04c44c6a609d26cd78eccacd7435d9b33eb2dd4f',1,'bod_feature.h']]],
  ['bod_5fprescale_5fdiv_5f4096',['BOD_PRESCALE_DIV_4096',['../group__asfdoc__sam0__bod__group.html#gga5a1d67e23851686cfaf6dca6e04c44c6a201b2f7c4b303f628f204b5306c6ec79',1,'bod_feature.h']]],
  ['bod_5fprescale_5fdiv_5f512',['BOD_PRESCALE_DIV_512',['../group__asfdoc__sam0__bod__group.html#gga5a1d67e23851686cfaf6dca6e04c44c6a34f86765e5cfa51490029cf3f055c6b6',1,'bod_feature.h']]],
  ['bod_5fprescale_5fdiv_5f64',['BOD_PRESCALE_DIV_64',['../group__asfdoc__sam0__bod__group.html#gga5a1d67e23851686cfaf6dca6e04c44c6a5cbef177a7900eb7c6ee4af28006666f',1,'bod_feature.h']]],
  ['bod_5fprescale_5fdiv_5f65536',['BOD_PRESCALE_DIV_65536',['../group__asfdoc__sam0__bod__group.html#gga5a1d67e23851686cfaf6dca6e04c44c6a19bdcd9c1a7a3282b423b8dac446980b',1,'bod_feature.h']]],
  ['bod_5fprescale_5fdiv_5f8',['BOD_PRESCALE_DIV_8',['../group__asfdoc__sam0__bod__group.html#gga5a1d67e23851686cfaf6dca6e04c44c6abf8b42d19d642c2750d5c2c791d84fca',1,'bod_feature.h']]],
  ['bod_5fprescale_5fdiv_5f8192',['BOD_PRESCALE_DIV_8192',['../group__asfdoc__sam0__bod__group.html#gga5a1d67e23851686cfaf6dca6e04c44c6aa1a6f2049e178090158d989ed7d70089',1,'bod_feature.h']]]
];
