var searchData=
[
  ['nvm_5fbod33_5faction',['nvm_bod33_action',['../group__asfdoc__sam0__nvm__group.html#ga98adde44584801a5e7f1aa633a9bcf6b',1,'nvm.h']]],
  ['nvm_5fbootloader_5fsize',['nvm_bootloader_size',['../group__asfdoc__sam0__nvm__group.html#gaa6a6c87595735845d0c034c7eb9cd21b',1,'nvm.h']]],
  ['nvm_5fcache_5freadmode',['nvm_cache_readmode',['../group__asfdoc__sam0__nvm__group.html#gaf0287836387dd573cde1e51be45793cf',1,'nvm.h']]],
  ['nvm_5fcommand',['nvm_command',['../group__asfdoc__sam0__nvm__group.html#ga666662632e3d2f14f84e089c58881158',1,'nvm.h']]],
  ['nvm_5feeprom_5femulator_5fsize',['nvm_eeprom_emulator_size',['../group__asfdoc__sam0__nvm__group.html#ga89981456d60fc1ad13cae55abfe1a98e',1,'nvm.h']]],
  ['nvm_5ferror',['nvm_error',['../group__asfdoc__sam0__nvm__group.html#gada4cd44d7190d0cac9b47419af0b7685',1,'nvm.h']]],
  ['nvm_5fsleep_5fpower_5fmode',['nvm_sleep_power_mode',['../group__asfdoc__sam0__nvm__group.html#ga13e8fb07f4a86bd83ff7bddd0091ac39',1,'nvm.h']]],
  ['nvm_5fwdt_5fearly_5fwarning_5foffset',['nvm_wdt_early_warning_offset',['../group__asfdoc__sam0__nvm__group.html#ga4237bbf1a0742fd560dcc191252939a1',1,'nvm.h']]],
  ['nvm_5fwdt_5fwindow_5ftimeout',['nvm_wdt_window_timeout',['../group__asfdoc__sam0__nvm__group.html#ga84c85335551c1486a64978005c592cae',1,'nvm.h']]]
];
