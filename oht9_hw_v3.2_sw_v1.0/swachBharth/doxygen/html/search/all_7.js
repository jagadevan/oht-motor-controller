var searchData=
[
  ['gain',['GAIN',['../union_a_d_c___i_n_p_u_t_c_t_r_l___type.html#a2d3bd933621526a675c150151a67649b',1,'ADC_INPUTCTRL_Type::GAIN()'],['../union_s_y_s_c_t_r_l___x_o_s_c___type.html#a020d2ecb9b5aedc10ec553c50ac9f74a',1,'SYSCTRL_XOSC_Type::GAIN()']]],
  ['gain_5fcorrection',['gain_correction',['../structadc__correction__config.html#aed04179630c7b5b4cb1eb6bc216121b4',1,'adc_correction_config']]],
  ['gain_5ffactor',['gain_factor',['../structadc__config.html#a86e81bf17549a89b11c1bdfacd19ded9',1,'adc_config']]],
  ['gaincorr',['GAINCORR',['../union_a_d_c___g_a_i_n_c_o_r_r___type.html#a074c34704f956be5b839eadb2cbbf82e',1,'ADC_GAINCORR_Type::GAINCORR()'],['../struct_adc.html#a25c34c42fd09b099541c05b244f4acc5',1,'Adc::GAINCORR()']]],
  ['gclk',['Gclk',['../struct_gclk.html',1,'Gclk'],['../group___s_a_m_d20_e14__base.html#ga23f9186cfd6ee5e60c8485315183271f',1,'GCLK():&#160;samd20e14.h'],['../group___s_a_m_d20_e15__base.html#ga23f9186cfd6ee5e60c8485315183271f',1,'GCLK():&#160;samd20e15.h'],['../group___s_a_m_d20_e16__base.html#ga23f9186cfd6ee5e60c8485315183271f',1,'GCLK():&#160;samd20e16.h'],['../group___s_a_m_d20_e17__base.html#ga23f9186cfd6ee5e60c8485315183271f',1,'GCLK():&#160;samd20e17.h'],['../group___s_a_m_d20_e18__base.html#ga23f9186cfd6ee5e60c8485315183271f',1,'GCLK():&#160;samd20e18.h'],['../group___s_a_m_d20_g14__base.html#ga23f9186cfd6ee5e60c8485315183271f',1,'GCLK():&#160;samd20g14.h'],['../group___s_a_m_d20_g15__base.html#ga23f9186cfd6ee5e60c8485315183271f',1,'GCLK():&#160;samd20g15.h'],['../group___s_a_m_d20_g16__base.html#ga23f9186cfd6ee5e60c8485315183271f',1,'GCLK():&#160;samd20g16.h'],['../group___s_a_m_d20_g17__base.html#ga23f9186cfd6ee5e60c8485315183271f',1,'GCLK():&#160;samd20g17.h'],['../group___s_a_m_d20_g17_u__base.html#ga23f9186cfd6ee5e60c8485315183271f',1,'GCLK():&#160;samd20g17u.h'],['../group___s_a_m_d20_g18__base.html#ga23f9186cfd6ee5e60c8485315183271f',1,'GCLK():&#160;samd20g18.h'],['../group___s_a_m_d20_g18_u__base.html#ga23f9186cfd6ee5e60c8485315183271f',1,'GCLK():&#160;samd20g18u.h'],['../group___s_a_m_d20_j14__base.html#ga23f9186cfd6ee5e60c8485315183271f',1,'GCLK():&#160;samd20j14.h'],['../group___s_a_m_d20_j15__base.html#ga23f9186cfd6ee5e60c8485315183271f',1,'GCLK():&#160;samd20j15.h'],['../group___s_a_m_d20_j16__base.html#ga23f9186cfd6ee5e60c8485315183271f',1,'GCLK():&#160;samd20j16.h'],['../group___s_a_m_d20_j17__base.html#ga23f9186cfd6ee5e60c8485315183271f',1,'GCLK():&#160;samd20j17.h'],['../group___s_a_m_d20_j18__base.html#ga23f9186cfd6ee5e60c8485315183271f',1,'GCLK():&#160;samd20j18.h']]],
  ['gclk_2ec',['gclk.c',['../gclk_8c.html',1,'']]],
  ['gclk_2eh',['gclk.h',['../drivers_2system_2clock_2gclk_8h.html',1,'(Global Namespace)'],['../utils_2cmsis_2samd20_2include_2component_2gclk_8h.html',1,'(Global Namespace)'],['../utils_2cmsis_2samd20_2include_2instance_2gclk_8h.html',1,'(Global Namespace)']]],
  ['gclk_5f',['GCLK_',['../union_p_m___a_p_b_a_m_a_s_k___type.html#a7ba29e4e94e76e7eb80e5a8f71586bb9',1,'PM_APBAMASK_Type']]],
  ['gclk_5fclkctrl_5fclken_5fpos',['GCLK_CLKCTRL_CLKEN_Pos',['../group___s_a_m_d20___g_c_l_k.html#ga988366053ec34c5f63659a49e0f20d3e',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fgen_5fgclk0_5fval',['GCLK_CLKCTRL_GEN_GCLK0_Val',['../group___s_a_m_d20___g_c_l_k.html#gace6212456debec86346f66fdd9e345f0',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fgen_5fgclk1_5fval',['GCLK_CLKCTRL_GEN_GCLK1_Val',['../group___s_a_m_d20___g_c_l_k.html#ga272c6846fcd4c313d7cc25dd48c4e46c',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fgen_5fgclk2_5fval',['GCLK_CLKCTRL_GEN_GCLK2_Val',['../group___s_a_m_d20___g_c_l_k.html#ga01572700eb8f9b256b2823b08d413467',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fgen_5fgclk3_5fval',['GCLK_CLKCTRL_GEN_GCLK3_Val',['../group___s_a_m_d20___g_c_l_k.html#gac93b1c3cf93221ec87c773f5b2fa87e4',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fgen_5fgclk4_5fval',['GCLK_CLKCTRL_GEN_GCLK4_Val',['../group___s_a_m_d20___g_c_l_k.html#ga9cab1760fd8855cd9e9b6d78279dd090',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fgen_5fgclk5_5fval',['GCLK_CLKCTRL_GEN_GCLK5_Val',['../group___s_a_m_d20___g_c_l_k.html#ga14e18a1b5c8919f1a3e8c679b3b17f9d',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fgen_5fgclk6_5fval',['GCLK_CLKCTRL_GEN_GCLK6_Val',['../group___s_a_m_d20___g_c_l_k.html#gaffc6f215a6671305caaeb6f9e38264d6',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fgen_5fgclk7_5fval',['GCLK_CLKCTRL_GEN_GCLK7_Val',['../group___s_a_m_d20___g_c_l_k.html#gaf0ef8769af1146d5f756a515bba69892',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fgen_5fpos',['GCLK_CLKCTRL_GEN_Pos',['../group___s_a_m_d20___g_c_l_k.html#gae80d1faef3b42edf73de73feef846841',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fac_5fana_5fval',['GCLK_CLKCTRL_ID_AC_ANA_Val',['../group___s_a_m_d20___g_c_l_k.html#ga5a296324385e4617069890aa675632ff',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fac_5fdig_5fval',['GCLK_CLKCTRL_ID_AC_DIG_Val',['../group___s_a_m_d20___g_c_l_k.html#ga4f314ff01d05a73788706a3e0cf98363',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fadc_5fval',['GCLK_CLKCTRL_ID_ADC_Val',['../group___s_a_m_d20___g_c_l_k.html#gafa044a1103ba6b6a8bc6d7f7d44c4cea',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fdac_5fval',['GCLK_CLKCTRL_ID_DAC_Val',['../group___s_a_m_d20___g_c_l_k.html#ga1dc77a3784a3e8021336bbe6a492ebe0',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fdfll48m_5fval',['GCLK_CLKCTRL_ID_DFLL48M_Val',['../group___s_a_m_d20___g_c_l_k.html#ga8a08114914f77f4b5bffe9ee9af0d8ca',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5feic_5fval',['GCLK_CLKCTRL_ID_EIC_Val',['../group___s_a_m_d20___g_c_l_k.html#gad482e84c3781695e980c8d1bcb9300f6',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fevsys_5fchannel_5f0_5fval',['GCLK_CLKCTRL_ID_EVSYS_CHANNEL_0_Val',['../group___s_a_m_d20___g_c_l_k.html#ga65f1a74a9362714a9aec9d93936c2273',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fevsys_5fchannel_5f1_5fval',['GCLK_CLKCTRL_ID_EVSYS_CHANNEL_1_Val',['../group___s_a_m_d20___g_c_l_k.html#ga5d6a0ecec83dc6b9d41a8e11ef0376b6',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fevsys_5fchannel_5f2_5fval',['GCLK_CLKCTRL_ID_EVSYS_CHANNEL_2_Val',['../group___s_a_m_d20___g_c_l_k.html#ga1fc382fdf8c8ce92fa84ca650e65d1e3',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fevsys_5fchannel_5f3_5fval',['GCLK_CLKCTRL_ID_EVSYS_CHANNEL_3_Val',['../group___s_a_m_d20___g_c_l_k.html#ga4c52b580284d5800e4fcd3a2dc4265bd',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fevsys_5fchannel_5f4_5fval',['GCLK_CLKCTRL_ID_EVSYS_CHANNEL_4_Val',['../group___s_a_m_d20___g_c_l_k.html#ga1e137d168a4f64243dc23ecad590feba',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fevsys_5fchannel_5f5_5fval',['GCLK_CLKCTRL_ID_EVSYS_CHANNEL_5_Val',['../group___s_a_m_d20___g_c_l_k.html#gafff0abd82b45aa6c59536d5ac133667e',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fevsys_5fchannel_5f6_5fval',['GCLK_CLKCTRL_ID_EVSYS_CHANNEL_6_Val',['../group___s_a_m_d20___g_c_l_k.html#ga74fb359c1367673b132511e437dbeb5d',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fevsys_5fchannel_5f7_5fval',['GCLK_CLKCTRL_ID_EVSYS_CHANNEL_7_Val',['../group___s_a_m_d20___g_c_l_k.html#ga495f0b6047993691fac9b0eb917225f7',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fpos',['GCLK_CLKCTRL_ID_Pos',['../group___s_a_m_d20___g_c_l_k.html#ga882860b306b819b8bb342bebd55c23aa',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fptc_5fval',['GCLK_CLKCTRL_ID_PTC_Val',['../group___s_a_m_d20___g_c_l_k.html#gac44e99202775c88a813b1416a66c7469',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5frtc_5fval',['GCLK_CLKCTRL_ID_RTC_Val',['../group___s_a_m_d20___g_c_l_k.html#gaa79509ffa7f18da70d4ae344a3d3d70b',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fsercom0_5fcore_5fval',['GCLK_CLKCTRL_ID_SERCOM0_CORE_Val',['../group___s_a_m_d20___g_c_l_k.html#gaa73a1f52ac12d73c9b415d0cf7c129d8',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fsercom1_5fcore_5fval',['GCLK_CLKCTRL_ID_SERCOM1_CORE_Val',['../group___s_a_m_d20___g_c_l_k.html#ga687c5b5b6f9d24d699c20826dae07ea1',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fsercom2_5fcore_5fval',['GCLK_CLKCTRL_ID_SERCOM2_CORE_Val',['../group___s_a_m_d20___g_c_l_k.html#ga6c190884b0ecb6df1e282e9f52fc9625',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fsercom3_5fcore_5fval',['GCLK_CLKCTRL_ID_SERCOM3_CORE_Val',['../group___s_a_m_d20___g_c_l_k.html#gafd61fc115b2df35b8362117d56e585f8',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fsercom4_5fcore_5fval',['GCLK_CLKCTRL_ID_SERCOM4_CORE_Val',['../group___s_a_m_d20___g_c_l_k.html#ga6abe19ed53416097da5b95e3dca3d98d',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fsercom5_5fcore_5fval',['GCLK_CLKCTRL_ID_SERCOM5_CORE_Val',['../group___s_a_m_d20___g_c_l_k.html#gadf32722a43710ff062d0581131ed71df',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fsercomx_5fslow_5fval',['GCLK_CLKCTRL_ID_SERCOMX_SLOW_Val',['../group___s_a_m_d20___g_c_l_k.html#ga153a5b6024c662b8f816b368ccd1bdc6',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5ftc0_5ftc1_5fval',['GCLK_CLKCTRL_ID_TC0_TC1_Val',['../group___s_a_m_d20___g_c_l_k.html#gaff9a70024d6e7b9f71aa9d6b79e2a4fd',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5ftc2_5ftc3_5fval',['GCLK_CLKCTRL_ID_TC2_TC3_Val',['../group___s_a_m_d20___g_c_l_k.html#ga2f611b6d8fa3ae6e7bab28f6bb889594',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5ftc4_5ftc5_5fval',['GCLK_CLKCTRL_ID_TC4_TC5_Val',['../group___s_a_m_d20___g_c_l_k.html#ga6e2e800404f3b2e6948ced5f489aa557',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5ftc6_5ftc7_5fval',['GCLK_CLKCTRL_ID_TC6_TC7_Val',['../group___s_a_m_d20___g_c_l_k.html#ga540d3e213ae8c696000670dbc1ea3721',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fid_5fwdt_5fval',['GCLK_CLKCTRL_ID_WDT_Val',['../group___s_a_m_d20___g_c_l_k.html#gaf55824f940b869be173bf84b368d5107',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fmask',['GCLK_CLKCTRL_MASK',['../group___s_a_m_d20___g_c_l_k.html#ga7cd3bae1a32142ecb56cff34f18b856f',1,'gclk.h']]],
  ['gclk_5fclkctrl_5foffset',['GCLK_CLKCTRL_OFFSET',['../group___s_a_m_d20___g_c_l_k.html#ga8a266827d508cecf79c9b27ab8596f0f',1,'gclk.h']]],
  ['gclk_5fclkctrl_5fresetvalue',['GCLK_CLKCTRL_RESETVALUE',['../group___s_a_m_d20___g_c_l_k.html#gad11334da452920f4d446c1615751b8c8',1,'gclk.h']]],
  ['gclk_5fclkctrl_5ftype',['GCLK_CLKCTRL_Type',['../union_g_c_l_k___c_l_k_c_t_r_l___type.html',1,'']]],
  ['gclk_5fclkctrl_5fwrtlock_5fpos',['GCLK_CLKCTRL_WRTLOCK_Pos',['../group___s_a_m_d20___g_c_l_k.html#ga380347dc132352716e030880aba8b807',1,'gclk.h']]],
  ['gclk_5fctrl_5fmask',['GCLK_CTRL_MASK',['../group___s_a_m_d20___g_c_l_k.html#ga1149580344c769b492a3965710f14319',1,'gclk.h']]],
  ['gclk_5fctrl_5foffset',['GCLK_CTRL_OFFSET',['../group___s_a_m_d20___g_c_l_k.html#ga1e4155316409181e6ee6e88d12cdc90d',1,'gclk.h']]],
  ['gclk_5fctrl_5fresetvalue',['GCLK_CTRL_RESETVALUE',['../group___s_a_m_d20___g_c_l_k.html#ga83d8721ae78e319dd3b6ab2e350a189b',1,'gclk.h']]],
  ['gclk_5fctrl_5fswrst_5fpos',['GCLK_CTRL_SWRST_Pos',['../group___s_a_m_d20___g_c_l_k.html#ga4efebabb229142324a17a17d178c8494',1,'gclk.h']]],
  ['gclk_5fctrl_5ftype',['GCLK_CTRL_Type',['../union_g_c_l_k___c_t_r_l___type.html',1,'']]],
  ['gclk_5fgenctrl_5fdivsel_5fpos',['GCLK_GENCTRL_DIVSEL_Pos',['../group___s_a_m_d20___g_c_l_k.html#gae17e18184623ee038ce57b64c63e4f9a',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fgenen_5fpos',['GCLK_GENCTRL_GENEN_Pos',['../group___s_a_m_d20___g_c_l_k.html#gacf206c291132d560c6f857c74d042b09',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fid_5fgclk0_5fval',['GCLK_GENCTRL_ID_GCLK0_Val',['../group___s_a_m_d20___g_c_l_k.html#gaf1067a2b64bf6ca67b6f70c4e2c0bbf8',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fid_5fgclk1_5fval',['GCLK_GENCTRL_ID_GCLK1_Val',['../group___s_a_m_d20___g_c_l_k.html#gaf8aa50029819495e6dc104dc211165ce',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fid_5fgclk2_5fval',['GCLK_GENCTRL_ID_GCLK2_Val',['../group___s_a_m_d20___g_c_l_k.html#gacdc79708a66dae8927edfdb69f8c8288',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fid_5fgclk3_5fval',['GCLK_GENCTRL_ID_GCLK3_Val',['../group___s_a_m_d20___g_c_l_k.html#ga1ce8af93e1ca7a48374cdff949bb2f72',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fid_5fgclk4_5fval',['GCLK_GENCTRL_ID_GCLK4_Val',['../group___s_a_m_d20___g_c_l_k.html#gac13da8d77f9b64b664a3b2f71b731907',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fid_5fgclk5_5fval',['GCLK_GENCTRL_ID_GCLK5_Val',['../group___s_a_m_d20___g_c_l_k.html#ga0df35ba83c10ed2932abffc2a8e74780',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fid_5fgclk6_5fval',['GCLK_GENCTRL_ID_GCLK6_Val',['../group___s_a_m_d20___g_c_l_k.html#ga4cf2b921048f8f142e0d380eb363df64',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fid_5fgclk7_5fval',['GCLK_GENCTRL_ID_GCLK7_Val',['../group___s_a_m_d20___g_c_l_k.html#ga6e7348bbf9371fa011e0b04df5c44fbd',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fid_5fpos',['GCLK_GENCTRL_ID_Pos',['../group___s_a_m_d20___g_c_l_k.html#ga4a60c72f4513306209ee3853e022a8ec',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fidc_5fpos',['GCLK_GENCTRL_IDC_Pos',['../group___s_a_m_d20___g_c_l_k.html#gadd1c684492c64ddc0b937c7571028381',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fmask',['GCLK_GENCTRL_MASK',['../group___s_a_m_d20___g_c_l_k.html#gab6d617a227f5a570e43959099ae10b1d',1,'gclk.h']]],
  ['gclk_5fgenctrl_5foe_5fpos',['GCLK_GENCTRL_OE_Pos',['../group___s_a_m_d20___g_c_l_k.html#ga463727f09be9f9cf1f26c55e954eaf0e',1,'gclk.h']]],
  ['gclk_5fgenctrl_5foffset',['GCLK_GENCTRL_OFFSET',['../group___s_a_m_d20___g_c_l_k.html#ga93d991084be98b48eb3bd242d2114042',1,'gclk.h']]],
  ['gclk_5fgenctrl_5foov_5fpos',['GCLK_GENCTRL_OOV_Pos',['../group___s_a_m_d20___g_c_l_k.html#ga511f960dfd2a4afa6dc073dc909389ac',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fresetvalue',['GCLK_GENCTRL_RESETVALUE',['../group___s_a_m_d20___g_c_l_k.html#ga6fe333c528ab24313886414224566cfc',1,'gclk.h']]],
  ['gclk_5fgenctrl_5frunstdby_5fpos',['GCLK_GENCTRL_RUNSTDBY_Pos',['../group___s_a_m_d20___g_c_l_k.html#ga74904f6bd2b8ead6cfffae549a41b869',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fsrc_5fdfll48m_5fval',['GCLK_GENCTRL_SRC_DFLL48M_Val',['../group___s_a_m_d20___g_c_l_k.html#ga70357963bac751c47a0b097568f1ec34',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fsrc_5fgclkgen1_5fval',['GCLK_GENCTRL_SRC_GCLKGEN1_Val',['../group___s_a_m_d20___g_c_l_k.html#ga2774be4de7707cea940a2b7ec37f30e3',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fsrc_5fgclkin_5fval',['GCLK_GENCTRL_SRC_GCLKIN_Val',['../group___s_a_m_d20___g_c_l_k.html#ga4c279c59a26c988991b63eb2682e8acc',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fsrc_5fosc32k_5fval',['GCLK_GENCTRL_SRC_OSC32K_Val',['../group___s_a_m_d20___g_c_l_k.html#ga79a91b96132812e2d8fec7fb79dc6cde',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fsrc_5fosc8m_5fval',['GCLK_GENCTRL_SRC_OSC8M_Val',['../group___s_a_m_d20___g_c_l_k.html#ga7b00616bd4afa72d171e9ad317b208ac',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fsrc_5fosculp32k_5fval',['GCLK_GENCTRL_SRC_OSCULP32K_Val',['../group___s_a_m_d20___g_c_l_k.html#ga42e2c1ea6d7f04f75657e31791336ec6',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fsrc_5fpos',['GCLK_GENCTRL_SRC_Pos',['../group___s_a_m_d20___g_c_l_k.html#ga3ca6fb0d36a0f1194282d2517b22965f',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fsrc_5fxosc32k_5fval',['GCLK_GENCTRL_SRC_XOSC32K_Val',['../group___s_a_m_d20___g_c_l_k.html#ga087326ba1d0ac766f2835e5460c72ef0',1,'gclk.h']]],
  ['gclk_5fgenctrl_5fsrc_5fxosc_5fval',['GCLK_GENCTRL_SRC_XOSC_Val',['../group___s_a_m_d20___g_c_l_k.html#gad48c4f5ba6b5112eef4d978cf2ae73bc',1,'gclk.h']]],
  ['gclk_5fgenctrl_5ftype',['GCLK_GENCTRL_Type',['../union_g_c_l_k___g_e_n_c_t_r_l___type.html',1,'']]],
  ['gclk_5fgendiv_5fdiv_5fpos',['GCLK_GENDIV_DIV_Pos',['../group___s_a_m_d20___g_c_l_k.html#ga182a17bda5373cb761e1bee891a03477',1,'gclk.h']]],
  ['gclk_5fgendiv_5fid_5fgclk0_5fval',['GCLK_GENDIV_ID_GCLK0_Val',['../group___s_a_m_d20___g_c_l_k.html#gac28f55d74e999f7f44ec3aa9e733e99f',1,'gclk.h']]],
  ['gclk_5fgendiv_5fid_5fgclk1_5fval',['GCLK_GENDIV_ID_GCLK1_Val',['../group___s_a_m_d20___g_c_l_k.html#ga2dc3454e6649886ae6a09efa583677a0',1,'gclk.h']]],
  ['gclk_5fgendiv_5fid_5fgclk2_5fval',['GCLK_GENDIV_ID_GCLK2_Val',['../group___s_a_m_d20___g_c_l_k.html#gad7a8d7dc24a3a3bb735b080e47e39a1b',1,'gclk.h']]],
  ['gclk_5fgendiv_5fid_5fgclk3_5fval',['GCLK_GENDIV_ID_GCLK3_Val',['../group___s_a_m_d20___g_c_l_k.html#gad5f2b016165ee017a90a4f1710839398',1,'gclk.h']]],
  ['gclk_5fgendiv_5fid_5fgclk4_5fval',['GCLK_GENDIV_ID_GCLK4_Val',['../group___s_a_m_d20___g_c_l_k.html#gac51c2dcff94064ae67baab4e01daace2',1,'gclk.h']]],
  ['gclk_5fgendiv_5fid_5fgclk5_5fval',['GCLK_GENDIV_ID_GCLK5_Val',['../group___s_a_m_d20___g_c_l_k.html#gad920d68a16392ff4b23c96eacee4bef5',1,'gclk.h']]],
  ['gclk_5fgendiv_5fid_5fgclk6_5fval',['GCLK_GENDIV_ID_GCLK6_Val',['../group___s_a_m_d20___g_c_l_k.html#gaae2e509fc759c2812826e0b655184506',1,'gclk.h']]],
  ['gclk_5fgendiv_5fid_5fgclk7_5fval',['GCLK_GENDIV_ID_GCLK7_Val',['../group___s_a_m_d20___g_c_l_k.html#ga6db82014c6174b47329b88c3fc439db7',1,'gclk.h']]],
  ['gclk_5fgendiv_5fid_5fpos',['GCLK_GENDIV_ID_Pos',['../group___s_a_m_d20___g_c_l_k.html#ga26ff4637fd9d6b561a910aeee96a4f88',1,'gclk.h']]],
  ['gclk_5fgendiv_5fmask',['GCLK_GENDIV_MASK',['../group___s_a_m_d20___g_c_l_k.html#gaf693b6342bd310d59956220a059f6e4a',1,'gclk.h']]],
  ['gclk_5fgendiv_5foffset',['GCLK_GENDIV_OFFSET',['../group___s_a_m_d20___g_c_l_k.html#ga2922a2f237f943cf8a023d3311ca5128',1,'gclk.h']]],
  ['gclk_5fgendiv_5fresetvalue',['GCLK_GENDIV_RESETVALUE',['../group___s_a_m_d20___g_c_l_k.html#ga886387ce3e41b7ca629a0b6a2292a6c0',1,'gclk.h']]],
  ['gclk_5fgendiv_5ftype',['GCLK_GENDIV_Type',['../union_g_c_l_k___g_e_n_d_i_v___type.html',1,'']]],
  ['gclk_5fgenerator',['gclk_generator',['../structspi__master__vec__config.html#a4687f645d18bab490113bf3805abf7b3',1,'spi_master_vec_config::gclk_generator()'],['../group__asfdoc__sam0__system__clock__group.html#ga1ab9bb87560ad127ed982591b7d67311',1,'gclk_generator():&#160;gclk.h']]],
  ['gclk_5fgenerator_5f0',['GCLK_GENERATOR_0',['../group__asfdoc__sam0__system__clock__group.html#gga1ab9bb87560ad127ed982591b7d67311a66f60c7b34b5fc16a9845d83370f7231',1,'gclk.h']]],
  ['gclk_5finst_5fnum',['GCLK_INST_NUM',['../group___s_a_m_d20_e14__base.html#gae4b5dc7eb760330c53681cda768fe9a2',1,'GCLK_INST_NUM():&#160;samd20e14.h'],['../group___s_a_m_d20_e15__base.html#gae4b5dc7eb760330c53681cda768fe9a2',1,'GCLK_INST_NUM():&#160;samd20e15.h'],['../group___s_a_m_d20_e16__base.html#gae4b5dc7eb760330c53681cda768fe9a2',1,'GCLK_INST_NUM():&#160;samd20e16.h'],['../group___s_a_m_d20_e17__base.html#gae4b5dc7eb760330c53681cda768fe9a2',1,'GCLK_INST_NUM():&#160;samd20e17.h'],['../group___s_a_m_d20_e18__base.html#gae4b5dc7eb760330c53681cda768fe9a2',1,'GCLK_INST_NUM():&#160;samd20e18.h'],['../group___s_a_m_d20_g14__base.html#gae4b5dc7eb760330c53681cda768fe9a2',1,'GCLK_INST_NUM():&#160;samd20g14.h'],['../group___s_a_m_d20_g15__base.html#gae4b5dc7eb760330c53681cda768fe9a2',1,'GCLK_INST_NUM():&#160;samd20g15.h'],['../group___s_a_m_d20_g16__base.html#gae4b5dc7eb760330c53681cda768fe9a2',1,'GCLK_INST_NUM():&#160;samd20g16.h'],['../group___s_a_m_d20_g17__base.html#gae4b5dc7eb760330c53681cda768fe9a2',1,'GCLK_INST_NUM():&#160;samd20g17.h'],['../group___s_a_m_d20_g17_u__base.html#gae4b5dc7eb760330c53681cda768fe9a2',1,'GCLK_INST_NUM():&#160;samd20g17u.h'],['../group___s_a_m_d20_g18__base.html#gae4b5dc7eb760330c53681cda768fe9a2',1,'GCLK_INST_NUM():&#160;samd20g18.h'],['../group___s_a_m_d20_g18_u__base.html#gae4b5dc7eb760330c53681cda768fe9a2',1,'GCLK_INST_NUM():&#160;samd20g18u.h'],['../group___s_a_m_d20_j14__base.html#gae4b5dc7eb760330c53681cda768fe9a2',1,'GCLK_INST_NUM():&#160;samd20j14.h'],['../group___s_a_m_d20_j15__base.html#gae4b5dc7eb760330c53681cda768fe9a2',1,'GCLK_INST_NUM():&#160;samd20j15.h'],['../group___s_a_m_d20_j16__base.html#gae4b5dc7eb760330c53681cda768fe9a2',1,'GCLK_INST_NUM():&#160;samd20j16.h'],['../group___s_a_m_d20_j17__base.html#gae4b5dc7eb760330c53681cda768fe9a2',1,'GCLK_INST_NUM():&#160;samd20j17.h'],['../group___s_a_m_d20_j18__base.html#gae4b5dc7eb760330c53681cda768fe9a2',1,'GCLK_INST_NUM():&#160;samd20j18.h']]],
  ['gclk_5finsts',['GCLK_INSTS',['../group___s_a_m_d20_e14__base.html#gaa628d67bafdc0077c685b79f3344decb',1,'GCLK_INSTS():&#160;samd20e14.h'],['../group___s_a_m_d20_e15__base.html#gaa628d67bafdc0077c685b79f3344decb',1,'GCLK_INSTS():&#160;samd20e15.h'],['../group___s_a_m_d20_e16__base.html#gaa628d67bafdc0077c685b79f3344decb',1,'GCLK_INSTS():&#160;samd20e16.h'],['../group___s_a_m_d20_e17__base.html#gaa628d67bafdc0077c685b79f3344decb',1,'GCLK_INSTS():&#160;samd20e17.h'],['../group___s_a_m_d20_e18__base.html#gaa628d67bafdc0077c685b79f3344decb',1,'GCLK_INSTS():&#160;samd20e18.h'],['../group___s_a_m_d20_g14__base.html#gaa628d67bafdc0077c685b79f3344decb',1,'GCLK_INSTS():&#160;samd20g14.h'],['../group___s_a_m_d20_g15__base.html#gaa628d67bafdc0077c685b79f3344decb',1,'GCLK_INSTS():&#160;samd20g15.h'],['../group___s_a_m_d20_g16__base.html#gaa628d67bafdc0077c685b79f3344decb',1,'GCLK_INSTS():&#160;samd20g16.h'],['../group___s_a_m_d20_g17__base.html#gaa628d67bafdc0077c685b79f3344decb',1,'GCLK_INSTS():&#160;samd20g17.h'],['../group___s_a_m_d20_g17_u__base.html#gaa628d67bafdc0077c685b79f3344decb',1,'GCLK_INSTS():&#160;samd20g17u.h'],['../group___s_a_m_d20_g18__base.html#gaa628d67bafdc0077c685b79f3344decb',1,'GCLK_INSTS():&#160;samd20g18.h'],['../group___s_a_m_d20_g18_u__base.html#gaa628d67bafdc0077c685b79f3344decb',1,'GCLK_INSTS():&#160;samd20g18u.h'],['../group___s_a_m_d20_j14__base.html#gaa628d67bafdc0077c685b79f3344decb',1,'GCLK_INSTS():&#160;samd20j14.h'],['../group___s_a_m_d20_j15__base.html#gaa628d67bafdc0077c685b79f3344decb',1,'GCLK_INSTS():&#160;samd20j15.h'],['../group___s_a_m_d20_j16__base.html#gaa628d67bafdc0077c685b79f3344decb',1,'GCLK_INSTS():&#160;samd20j16.h'],['../group___s_a_m_d20_j17__base.html#gaa628d67bafdc0077c685b79f3344decb',1,'GCLK_INSTS():&#160;samd20j17.h'],['../group___s_a_m_d20_j18__base.html#gaa628d67bafdc0077c685b79f3344decb',1,'GCLK_INSTS():&#160;samd20j18.h']]],
  ['gclk_5fstatus_5fmask',['GCLK_STATUS_MASK',['../group___s_a_m_d20___g_c_l_k.html#gaeb39d54d6d2ecc2bdaafb2a846930171',1,'gclk.h']]],
  ['gclk_5fstatus_5foffset',['GCLK_STATUS_OFFSET',['../group___s_a_m_d20___g_c_l_k.html#ga1829e59ccf51b247d4c84f4b53356e59',1,'gclk.h']]],
  ['gclk_5fstatus_5fresetvalue',['GCLK_STATUS_RESETVALUE',['../group___s_a_m_d20___g_c_l_k.html#ga178a91eb9f5aa21feeb1b126b05dd8cd',1,'gclk.h']]],
  ['gclk_5fstatus_5fsyncbusy_5fpos',['GCLK_STATUS_SYNCBUSY_Pos',['../group___s_a_m_d20___g_c_l_k.html#ga8ef544f7407509465d3512c48226cab4',1,'gclk.h']]],
  ['gclk_5fstatus_5ftype',['GCLK_STATUS_Type',['../union_g_c_l_k___s_t_a_t_u_s___type.html',1,'']]],
  ['gclkreq',['GCLKREQ',['../union_e_v_s_y_s___c_t_r_l___type.html#a16dbc77504b156752d1b4ec6a79b3a8b',1,'EVSYS_CTRL_Type']]],
  ['gen',['GEN',['../union_g_c_l_k___c_l_k_c_t_r_l___type.html#a7cd47690130c0666134ae9aa4d71b99f',1,'GCLK_CLKCTRL_Type']]],
  ['gencen',['GENCEN',['../union_s_e_r_c_o_m___i2_c_s___a_d_d_r___type.html#a02c23c0f49702e04527263422a00c1af',1,'SERCOM_I2CS_ADDR_Type']]],
  ['genctrl',['GENCTRL',['../struct_gclk.html#a75d3c3004e8e8cfd954c5511dea59628',1,'Gclk']]],
  ['gendiv',['GENDIV',['../struct_gclk.html#a04bb17e3e75c7590a0f1a2a011f23475',1,'Gclk']]],
  ['genen',['GENEN',['../union_g_c_l_k___g_e_n_c_t_r_l___type.html#a78a083801d5776bdd03e8161f75b7901',1,'GCLK_GENCTRL_Type']]],
  ['generate_5fevent_5fon_5falarm',['generate_event_on_alarm',['../structrtc__calendar__events.html#aeefa103557489988c3536ff6c534feac',1,'rtc_calendar_events']]],
  ['generate_5fevent_5fon_5fconversion_5fdone',['generate_event_on_conversion_done',['../structadc__events.html#a7d649897bf882114b55934f2a154c3f6',1,'adc_events']]],
  ['generate_5fevent_5fon_5fdetect',['generate_event_on_detect',['../structextint__events.html#a26a0e0244944056312aca67c09a93de4',1,'extint_events']]],
  ['generate_5fevent_5fon_5foverflow',['generate_event_on_overflow',['../structrtc__calendar__events.html#a5a0a9888c328215ed96e4897bfd1a1e9',1,'rtc_calendar_events']]],
  ['generate_5fevent_5fon_5fperiodic',['generate_event_on_periodic',['../structrtc__calendar__events.html#a3ffeea6367b9c5c70999f382a1c5006f',1,'rtc_calendar_events']]],
  ['generate_5fevent_5fon_5fwindow_5fmonitor',['generate_event_on_window_monitor',['../structadc__events.html#a60695e351893456f55735b8a773e0471',1,'adc_events']]],
  ['generator_5fsource',['generator_source',['../structspi__config.html#a463fde643e8158b38d3455f99d933bd9',1,'spi_config::generator_source()'],['../structusart__config.html#a6f36554bb785451472ab8fa7909e47e5',1,'usart_config::generator_source()']]],
  ['get_5falign',['Get_align',['../group__group__sam0__utils.html#ga526bddb313be14057f5d65e199f6d814',1,'compiler.h']]],
  ['gpio_5fpin',['gpio_pin',['../structextint__chan__conf.html#a1876269a3fcae880a0266e277178b2f7',1,'extint_chan_conf::gpio_pin()'],['../structextint__nmi__conf.html#aafb28aca551188669d8bbb565940dabd',1,'extint_nmi_conf::gpio_pin()']]],
  ['gpio_5fpin_5fmux',['gpio_pin_mux',['../structextint__chan__conf.html#a6389076699e56f9f046aa04137943837',1,'extint_chan_conf::gpio_pin_mux()'],['../structextint__nmi__conf.html#a004050e2c30e794f74bf68363ae7a05c',1,'extint_nmi_conf::gpio_pin_mux()']]],
  ['gpio_5fpin_5fpull',['gpio_pin_pull',['../structextint__chan__conf.html#a02d7bf70ee96b3a520289630101419cf',1,'extint_chan_conf::gpio_pin_pull()'],['../structextint__nmi__conf.html#a2673cf810f95a0d148f5d55db8ae2fde',1,'extint_nmi_conf::gpio_pin_pull()']]],
  ['group',['Group',['../struct_port.html#a4b09978ac42934c6cf96f26829ec1f63',1,'Port']]],
  ['generic_20board_20support',['Generic board support',['../group__group__common__boards.html',1,'']]],
  ['global_20interrupt_20management',['Global interrupt management',['../group__interrupt__group.html',1,'']]],
  ['generic_20clock_20generator',['Generic Clock Generator',['../group___s_a_m_d20___g_c_l_k.html',1,'']]]
];
