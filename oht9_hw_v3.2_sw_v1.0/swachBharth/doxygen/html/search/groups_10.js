var searchData=
[
  ['vector_20clarke_20transform',['Vector Clarke Transform',['../group__clarke.html',1,'']]],
  ['vector_20inverse_20clarke_20transform',['Vector Inverse Clarke Transform',['../group__inv__clarke.html',1,'']]],
  ['vector_20inverse_20park_20transform',['Vector Inverse Park transform',['../group__inv__park.html',1,'']]],
  ['vector_20park_20transform',['Vector Park Transform',['../group__park.html',1,'']]],
  ['vcoroutineschedule',['vCoRoutineSchedule',['../group__v_co_routine_schedule.html',1,'']]],
  ['vsemaphorecreatebinary',['vSemaphoreCreateBinary',['../group__v_semaphore_create_binary.html',1,'']]],
  ['vsemaphorecreatemutex',['vSemaphoreCreateMutex',['../group__v_semaphore_create_mutex.html',1,'']]],
  ['vtaskdelay',['vTaskDelay',['../group__v_task_delay.html',1,'']]],
  ['vtaskdelayuntil',['vTaskDelayUntil',['../group__v_task_delay_until.html',1,'']]],
  ['vtaskdelete',['vTaskDelete',['../group__v_task_delete.html',1,'']]],
  ['vtaskendscheduler',['vTaskEndScheduler',['../group__v_task_end_scheduler.html',1,'']]],
  ['vtaskpriorityset',['vTaskPrioritySet',['../group__v_task_priority_set.html',1,'']]],
  ['vtaskresume',['vTaskResume',['../group__v_task_resume.html',1,'']]],
  ['vtaskresumefromisr',['vTaskResumeFromISR',['../group__v_task_resume_from_i_s_r.html',1,'']]],
  ['vtaskstartscheduler',['vTaskStartScheduler',['../group__v_task_start_scheduler.html',1,'']]],
  ['vtasksuspend',['vTaskSuspend',['../group__v_task_suspend.html',1,'']]],
  ['vtasksuspendall',['vTaskSuspendAll',['../group__v_task_suspend_all.html',1,'']]]
];
