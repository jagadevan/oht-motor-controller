var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxyz",
  1: "_abcdegijnpqrstuwx",
  2: "abcdegimnpqrstuw",
  3: "_abdenprsuw",
  4: "_abcdefghijklmnopqrstuvwxyz",
  5: "aefirsuw",
  6: "_abeginprsuw",
  7: "_abdeghnoprstuw",
  8: "_cepr",
  9: "abcdefgilmnprstuvwx",
  10: "adempqstuvx"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "files",
  3: "functions",
  4: "variables",
  5: "typedefs",
  6: "enums",
  7: "enumvalues",
  8: "defines",
  9: "groups",
  10: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Typedefs",
  6: "Enumerations",
  7: "Enumerator",
  8: "Macros",
  9: "Modules",
  10: "Pages"
};

