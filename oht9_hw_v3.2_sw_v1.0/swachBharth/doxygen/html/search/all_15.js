var searchData=
[
  ['u16',['U16',['../group__group__sam0__utils.html#ga0a0a322d5fa4a546d293a77ba8b4a71f',1,'compiler.h']]],
  ['u32',['U32',['../group__group__sam0__utils.html#ga696390429f2f3b644bde8d0322a24124',1,'compiler.h']]],
  ['u64',['U64',['../group__group__sam0__utils.html#ga25809e0734a149248fcf5831efa4e33d',1,'compiler.h']]],
  ['u8',['U8',['../group__group__sam0__utils.html#gaa63ef7b996d5487ce35a5a66601f3e73',1,'compiler.h']]],
  ['uc3',['UC3',['../group__uc3__part__macros__group.html#ga848d950beec67bc702bfcfdecc70bb5b',1,'parts.h']]],
  ['uc3_5fa3_5fxplained',['UC3_A3_XPLAINED',['../group__group__common__boards.html#ga6dba56934e370fd84b3126a71bd356e7',1,'board.h']]],
  ['uc3_5fl0_5fqt600',['UC3_L0_QT600',['../group__group__common__boards.html#gae06980d7c2c8a89eab699394bfb52897',1,'board.h']]],
  ['uc3_5fl0_5fxplained',['UC3_L0_XPLAINED',['../group__group__common__boards.html#ga200ef6ece7fd63cb742129efac4a935e',1,'board.h']]],
  ['uc3_5fl0_5fxplained_5fbc',['UC3_L0_XPLAINED_BC',['../group__group__common__boards.html#gab872a6d2f79f1bbb274b5f9fff4b855f',1,'board.h']]],
  ['uc3a',['UC3A',['../group__uc3__part__macros__group.html#gaf7f16cc2bde7daf34576983fee8824c8',1,'parts.h']]],
  ['uc3b',['UC3B',['../group__uc3__part__macros__group.html#ga61e51606324c212eb11079e5acf592e0',1,'parts.h']]],
  ['uc3b_5fboard_5fcontroller',['UC3B_BOARD_CONTROLLER',['../group__group__common__boards.html#ga79a559c8d51d9370af4f37c36b6e7d58',1,'board.h']]],
  ['uc3c',['UC3C',['../group__uc3__part__macros__group.html#gac7a13b9ceb67804d94b6a8967512ee71',1,'parts.h']]],
  ['uc3c_5fek',['UC3C_EK',['../group__group__common__boards.html#ga134b9977e8acdca22a47a93daad9112d',1,'board.h']]],
  ['uc3d',['UC3D',['../group__uc3__part__macros__group.html#ga351a876f40669641e6ba9620909143d3',1,'parts.h']]],
  ['uc3l',['UC3L',['../group__uc3__part__macros__group.html#gac8a7d715e500aa74cd05e0f0fc5bb005',1,'parts.h']]],
  ['uc3l_5fek',['UC3L_EK',['../group__group__common__boards.html#ga5119b352b04e0aad2323dc952a385995',1,'board.h']]],
  ['underrun',['UNDERRUN',['../union_d_a_c___i_n_t_e_n_c_l_r___type.html#af96da0c477868c26b66597bfcaeb69a1',1,'DAC_INTENCLR_Type::UNDERRUN()'],['../union_d_a_c___i_n_t_e_n_s_e_t___type.html#a468aeaf616ad2919bf78a29cc00f01a6',1,'DAC_INTENSET_Type::UNDERRUN()'],['../union_d_a_c___i_n_t_f_l_a_g___type.html#aca23abc68cf79f8485c0406341231189',1,'DAC_INTFLAG_Type::UNDERRUN()']]],
  ['union16',['Union16',['../union_union16.html',1,'']]],
  ['union32',['Union32',['../union_union32.html',1,'']]],
  ['union64',['Union64',['../union_union64.html',1,'']]],
  ['unioncptr',['UnionCPtr',['../union_union_c_ptr.html',1,'']]],
  ['unioncvptr',['UnionCVPtr',['../union_union_c_v_ptr.html',1,'']]],
  ['unionptr',['UnionPtr',['../union_union_ptr.html',1,'']]],
  ['unionvptr',['UnionVPtr',['../union_union_v_ptr.html',1,'']]],
  ['unlikely',['unlikely',['../group__group__sam0__utils.html#ga1c0c89beb84d05c5ba0bc7ce527a3925',1,'compiler.h']]],
  ['unused',['unused',['../group__group__sam0__utils.html#ga417dba7f63dde98dbebd6336d8af9d91',1,'unused():&#160;compiler.h'],['../group__group__sam0__utils.html#gada67c62b1c57e07efa04431bc40b9238',1,'UNUSED():&#160;compiler.h']]],
  ['usart',['USART',['../union_sercom.html#ae82be07be50298f8e692f755dbbdc7ab',1,'Sercom']]],
  ['usart_2ec',['usart.c',['../usart_8c.html',1,'']]],
  ['usart_2eh',['usart.h',['../usart_8h.html',1,'']]],
  ['usart_5fabort_5fjob',['usart_abort_job',['../group__asfdoc__sam0__sercom__usart__group.html#ga08e77aeb7ff01d060c5c936a63581a99',1,'usart_abort_job(struct usart_module *const module, enum usart_transceiver_type transceiver_type):&#160;usart_interrupt.c'],['../group__asfdoc__sam0__sercom__usart__group.html#ga08e77aeb7ff01d060c5c936a63581a99',1,'usart_abort_job(struct usart_module *const module, enum usart_transceiver_type transceiver_type):&#160;usart_interrupt.c']]],
  ['usart_5fcallback',['usart_callback',['../group__asfdoc__sam0__sercom__usart__group.html#gae257d5c9ac64a6835db020aa2458439d',1,'usart.h']]],
  ['usart_5fcallback_5fbuffer_5freceived',['USART_CALLBACK_BUFFER_RECEIVED',['../group__asfdoc__sam0__sercom__usart__group.html#ggae257d5c9ac64a6835db020aa2458439dad21b5e021545958df49158f3bbf21bd8',1,'usart.h']]],
  ['usart_5fcallback_5fbuffer_5ftransmitted',['USART_CALLBACK_BUFFER_TRANSMITTED',['../group__asfdoc__sam0__sercom__usart__group.html#ggae257d5c9ac64a6835db020aa2458439da83df73f78d94d100d7d7e0f5840ca487',1,'usart.h']]],
  ['usart_5fcallback_5ferror',['USART_CALLBACK_ERROR',['../group__asfdoc__sam0__sercom__usart__group.html#ggae257d5c9ac64a6835db020aa2458439da115952c86552b3dc3d94430a76e6f3a8',1,'usart.h']]],
  ['usart_5fcallback_5fn',['USART_CALLBACK_N',['../group__asfdoc__sam0__sercom__usart__group.html#ggae257d5c9ac64a6835db020aa2458439dad6e55cb6f0f87b63292fe063d76405b5',1,'usart.h']]],
  ['usart_5fcallback_5ft',['usart_callback_t',['../group__asfdoc__sam0__sercom__usart__group.html#ga1d3e2901d0cf76f20aae271f7122c67d',1,'usart.h']]],
  ['usart_5fcharacter_5fsize',['usart_character_size',['../group__asfdoc__sam0__sercom__usart__group.html#ga631ce7b4f60dccd392e6d6ef7d3cd4e2',1,'usart.h']]],
  ['usart_5fcharacter_5fsize_5f5bit',['USART_CHARACTER_SIZE_5BIT',['../group__asfdoc__sam0__sercom__usart__group.html#gga631ce7b4f60dccd392e6d6ef7d3cd4e2ab1db6dc41b52d70a9185b963d9140c7f',1,'usart.h']]],
  ['usart_5fcharacter_5fsize_5f6bit',['USART_CHARACTER_SIZE_6BIT',['../group__asfdoc__sam0__sercom__usart__group.html#gga631ce7b4f60dccd392e6d6ef7d3cd4e2a4b631e89e72f2b939afbc4a996d358b9',1,'usart.h']]],
  ['usart_5fcharacter_5fsize_5f7bit',['USART_CHARACTER_SIZE_7BIT',['../group__asfdoc__sam0__sercom__usart__group.html#gga631ce7b4f60dccd392e6d6ef7d3cd4e2ab5e1a6e76a61ee79563a551ad2431726',1,'usart.h']]],
  ['usart_5fcharacter_5fsize_5f8bit',['USART_CHARACTER_SIZE_8BIT',['../group__asfdoc__sam0__sercom__usart__group.html#gga631ce7b4f60dccd392e6d6ef7d3cd4e2a8679bfe526e0d92b5d313f9f5880da4b',1,'usart.h']]],
  ['usart_5fcharacter_5fsize_5f9bit',['USART_CHARACTER_SIZE_9BIT',['../group__asfdoc__sam0__sercom__usart__group.html#gga631ce7b4f60dccd392e6d6ef7d3cd4e2a6a7fe30ecca9dbde62c154d6619cab7f',1,'usart.h']]],
  ['usart_5fconfig',['usart_config',['../structusart__config.html',1,'']]],
  ['usart_5fdataorder',['usart_dataorder',['../group__asfdoc__sam0__sercom__usart__group.html#ga4352d9150bb8cbd54d26abe3055a5ee1',1,'usart.h']]],
  ['usart_5fdataorder_5flsb',['USART_DATAORDER_LSB',['../group__asfdoc__sam0__sercom__usart__group.html#gga4352d9150bb8cbd54d26abe3055a5ee1afb0c8f2d6f7da9f62dc67871d710b9af',1,'usart.h']]],
  ['usart_5fdataorder_5fmsb',['USART_DATAORDER_MSB',['../group__asfdoc__sam0__sercom__usart__group.html#gga4352d9150bb8cbd54d26abe3055a5ee1add3a3b7f39f32c42d22991ea981e2810',1,'usart.h']]],
  ['usart_5fget_5fjob_5fstatus',['usart_get_job_status',['../group__asfdoc__sam0__sercom__usart__group.html#ga80364bc53d6d6405a2a7f3b1aa507503',1,'usart_get_job_status(struct usart_module *const module, enum usart_transceiver_type transceiver_type):&#160;usart_interrupt.c'],['../group__asfdoc__sam0__sercom__usart__group.html#ga80364bc53d6d6405a2a7f3b1aa507503',1,'usart_get_job_status(struct usart_module *const module, enum usart_transceiver_type transceiver_type):&#160;usart_interrupt.c']]],
  ['usart_5finit',['usart_init',['../group__asfdoc__sam0__sercom__usart__group.html#gad67046f395137b2a7a1ef72f83907674',1,'usart_init(struct usart_module *const module, Sercom *const hw, const struct usart_config *const config):&#160;usart.c'],['../group__asfdoc__sam0__sercom__usart__group.html#gad67046f395137b2a7a1ef72f83907674',1,'usart_init(struct usart_module *const module, Sercom *const hw, const struct usart_config *const config):&#160;usart.c']]],
  ['usart_5finterrupt_2ec',['usart_interrupt.c',['../usart__interrupt_8c.html',1,'']]],
  ['usart_5finterrupt_2eh',['usart_interrupt.h',['../usart__interrupt_8h.html',1,'']]],
  ['usart_5fmodule',['usart_module',['../structusart__module.html',1,'']]],
  ['usart_5fparity',['usart_parity',['../group__asfdoc__sam0__sercom__usart__group.html#ga867cc5f0ea7d3bf651d68f0046cf6f41',1,'usart.h']]],
  ['usart_5fparity_5feven',['USART_PARITY_EVEN',['../group__asfdoc__sam0__sercom__usart__group.html#gga867cc5f0ea7d3bf651d68f0046cf6f41ae5d22c99a30184aff19d77c1a970fb23',1,'usart.h']]],
  ['usart_5fparity_5fnone',['USART_PARITY_NONE',['../group__asfdoc__sam0__sercom__usart__group.html#gga867cc5f0ea7d3bf651d68f0046cf6f41aecf52ec650226bdc63e12a21d3b5585d',1,'usart.h']]],
  ['usart_5fparity_5fodd',['USART_PARITY_ODD',['../group__asfdoc__sam0__sercom__usart__group.html#gga867cc5f0ea7d3bf651d68f0046cf6f41a69c6cdd4d354d3b26c8d2f09f49d2ede',1,'usart.h']]],
  ['usart_5fread_5fbuffer_5fjob',['usart_read_buffer_job',['../group__asfdoc__sam0__sercom__usart__group.html#ga95b1e55af02e69454ecff04c3d712527',1,'usart_read_buffer_job(struct usart_module *const module, uint8_t *rx_data, uint16_t length):&#160;usart_interrupt.c'],['../group__asfdoc__sam0__sercom__usart__group.html#ga95b1e55af02e69454ecff04c3d712527',1,'usart_read_buffer_job(struct usart_module *const module, uint8_t *rx_data, uint16_t length):&#160;usart_interrupt.c']]],
  ['usart_5fread_5fbuffer_5fwait',['usart_read_buffer_wait',['../group__asfdoc__sam0__sercom__usart__group.html#ga4f788b3478e9c1fa8f0dd8d09939d3a6',1,'usart_read_buffer_wait(struct usart_module *const module, uint8_t *rx_data, uint16_t length):&#160;usart.c'],['../group__asfdoc__sam0__sercom__usart__group.html#ga4f788b3478e9c1fa8f0dd8d09939d3a6',1,'usart_read_buffer_wait(struct usart_module *const module, uint8_t *rx_data, uint16_t length):&#160;usart.c']]],
  ['usart_5fread_5fjob',['usart_read_job',['../group__asfdoc__sam0__sercom__usart__group.html#ga095085e94cc009c31243df62395896fd',1,'usart_read_job(struct usart_module *const module, uint16_t *const rx_data):&#160;usart_interrupt.c'],['../group__asfdoc__sam0__sercom__usart__group.html#ga095085e94cc009c31243df62395896fd',1,'usart_read_job(struct usart_module *const module, uint16_t *const rx_data):&#160;usart_interrupt.c']]],
  ['usart_5fread_5fwait',['usart_read_wait',['../group__asfdoc__sam0__sercom__usart__group.html#gaf7db90c51a6f17edff5f1de2a0e3d8a5',1,'usart_read_wait(struct usart_module *const module, uint16_t *const rx_data):&#160;usart.c'],['../group__asfdoc__sam0__sercom__usart__group.html#gaf7db90c51a6f17edff5f1de2a0e3d8a5',1,'usart_read_wait(struct usart_module *const module, uint16_t *const rx_data):&#160;usart.c']]],
  ['usart_5fregister_5fcallback',['usart_register_callback',['../group__asfdoc__sam0__sercom__usart__group.html#ga39e38593fb41abe34cec95d53c7b1e37',1,'usart_register_callback(struct usart_module *const module, usart_callback_t callback_func, enum usart_callback callback_type):&#160;usart_interrupt.c'],['../group__asfdoc__sam0__sercom__usart__group.html#ga39e38593fb41abe34cec95d53c7b1e37',1,'usart_register_callback(struct usart_module *const module, usart_callback_t callback_func, enum usart_callback callback_type):&#160;usart_interrupt.c']]],
  ['usart_5frx_5f0_5ftx_5f0_5fxck_5f1',['USART_RX_0_TX_0_XCK_1',['../group__asfdoc__sam0__sercom__usart__group.html#gga87bbdb9f7edb3f1866aeb498bf7c9077ab2330cf3ae7faacb7fd28678a77327f4',1,'usart.h']]],
  ['usart_5frx_5f0_5ftx_5f2_5fxck_5f3',['USART_RX_0_TX_2_XCK_3',['../group__asfdoc__sam0__sercom__usart__group.html#gga87bbdb9f7edb3f1866aeb498bf7c9077a4ad0f5cd921dd8c0787ebeba372bcc70',1,'usart.h']]],
  ['usart_5frx_5f1_5ftx_5f0_5fxck_5f1',['USART_RX_1_TX_0_XCK_1',['../group__asfdoc__sam0__sercom__usart__group.html#gga87bbdb9f7edb3f1866aeb498bf7c9077ac937b1e1063cc3d094c4534c022af703',1,'usart.h']]],
  ['usart_5frx_5f1_5ftx_5f2_5fxck_5f3',['USART_RX_1_TX_2_XCK_3',['../group__asfdoc__sam0__sercom__usart__group.html#gga87bbdb9f7edb3f1866aeb498bf7c9077a884ae3424a45469c20b4ecf9c65a8e48',1,'usart.h']]],
  ['usart_5frx_5f2_5ftx_5f0_5fxck_5f1',['USART_RX_2_TX_0_XCK_1',['../group__asfdoc__sam0__sercom__usart__group.html#gga87bbdb9f7edb3f1866aeb498bf7c9077a730a5c0276235851c892b3a817a6d49f',1,'usart.h']]],
  ['usart_5frx_5f2_5ftx_5f2_5fxck_5f3',['USART_RX_2_TX_2_XCK_3',['../group__asfdoc__sam0__sercom__usart__group.html#gga87bbdb9f7edb3f1866aeb498bf7c9077aa64f03365c6e57705c9a154741523956',1,'usart.h']]],
  ['usart_5frx_5f3_5ftx_5f0_5fxck_5f1',['USART_RX_3_TX_0_XCK_1',['../group__asfdoc__sam0__sercom__usart__group.html#gga87bbdb9f7edb3f1866aeb498bf7c9077a01cd27a54558c7956d41d29c16cf4a40',1,'usart.h']]],
  ['usart_5frx_5f3_5ftx_5f2_5fxck_5f3',['USART_RX_3_TX_2_XCK_3',['../group__asfdoc__sam0__sercom__usart__group.html#gga87bbdb9f7edb3f1866aeb498bf7c9077a026e9eb10b4f94b2a0e5640a9fe22544',1,'usart.h']]],
  ['usart_5fsignal_5fmux_5fsettings',['usart_signal_mux_settings',['../group__asfdoc__sam0__sercom__usart__group.html#ga87bbdb9f7edb3f1866aeb498bf7c9077',1,'usart.h']]],
  ['usart_5fstopbits',['usart_stopbits',['../group__asfdoc__sam0__sercom__usart__group.html#gab7d8ac715e2bd7ccddc65bf2c5ceb1f5',1,'usart.h']]],
  ['usart_5fstopbits_5f1',['USART_STOPBITS_1',['../group__asfdoc__sam0__sercom__usart__group.html#ggab7d8ac715e2bd7ccddc65bf2c5ceb1f5a9011413422d1340bb5c343a1f3b57341',1,'usart.h']]],
  ['usart_5fstopbits_5f2',['USART_STOPBITS_2',['../group__asfdoc__sam0__sercom__usart__group.html#ggab7d8ac715e2bd7ccddc65bf2c5ceb1f5a1e3cd9a88a519d93c0c300e873437a2c',1,'usart.h']]],
  ['usart_5ftimeout',['USART_TIMEOUT',['../group__asfdoc__sam0__sercom__usart__group.html#gad3e37fdd22ce059c9158a818d608447a',1,'usart.h']]],
  ['usart_5ftransceiver_5frx',['USART_TRANSCEIVER_RX',['../group__asfdoc__sam0__sercom__usart__group.html#ggaab1b986bc581f76e99eec14ac37efe05a627832963965f11ae53299b802582c48',1,'usart.h']]],
  ['usart_5ftransceiver_5ftx',['USART_TRANSCEIVER_TX',['../group__asfdoc__sam0__sercom__usart__group.html#ggaab1b986bc581f76e99eec14ac37efe05a9d1050c223a28be7aefcbca46a0add81',1,'usart.h']]],
  ['usart_5ftransceiver_5ftype',['usart_transceiver_type',['../group__asfdoc__sam0__sercom__usart__group.html#gaab1b986bc581f76e99eec14ac37efe05',1,'usart.h']]],
  ['usart_5ftransfer_5fasynchronously',['USART_TRANSFER_ASYNCHRONOUSLY',['../group__asfdoc__sam0__sercom__usart__group.html#gga7ff4d85053b8ea0904b5a57587b39c8fa5662e0cc82a73f113bb13ebb1e4c65ef',1,'usart.h']]],
  ['usart_5ftransfer_5fmode',['usart_transfer_mode',['../group__asfdoc__sam0__sercom__usart__group.html#ga7ff4d85053b8ea0904b5a57587b39c8f',1,'usart.h']]],
  ['usart_5ftransfer_5fsynchronously',['USART_TRANSFER_SYNCHRONOUSLY',['../group__asfdoc__sam0__sercom__usart__group.html#gga7ff4d85053b8ea0904b5a57587b39c8fa4756b64694eb2e2ca4f4c77c6f53786d',1,'usart.h']]],
  ['usart_5funregister_5fcallback',['usart_unregister_callback',['../group__asfdoc__sam0__sercom__usart__group.html#ga5be8cd239bf4acd63e341be97a5cd602',1,'usart_unregister_callback(struct usart_module *const module, enum usart_callback callback_type):&#160;usart_interrupt.c'],['../group__asfdoc__sam0__sercom__usart__group.html#ga5be8cd239bf4acd63e341be97a5cd602',1,'usart_unregister_callback(struct usart_module *module, enum usart_callback callback_type):&#160;usart_interrupt.c']]],
  ['usart_5fwrite_5fbuffer_5fjob',['usart_write_buffer_job',['../group__asfdoc__sam0__sercom__usart__group.html#ga2811c1f5cd7fdcf653eac02812f2265c',1,'usart_write_buffer_job(struct usart_module *const module, uint8_t *tx_data, uint16_t length):&#160;usart_interrupt.c'],['../group__asfdoc__sam0__sercom__usart__group.html#ga2811c1f5cd7fdcf653eac02812f2265c',1,'usart_write_buffer_job(struct usart_module *const module, uint8_t *tx_data, uint16_t length):&#160;usart_interrupt.c']]],
  ['usart_5fwrite_5fbuffer_5fwait',['usart_write_buffer_wait',['../group__asfdoc__sam0__sercom__usart__group.html#gacffd0845249348d37d14c65a41132e41',1,'usart_write_buffer_wait(struct usart_module *const module, const uint8_t *tx_data, uint16_t length):&#160;usart.c'],['../group__asfdoc__sam0__sercom__usart__group.html#gacffd0845249348d37d14c65a41132e41',1,'usart_write_buffer_wait(struct usart_module *const module, const uint8_t *tx_data, uint16_t length):&#160;usart.c']]],
  ['usart_5fwrite_5fjob',['usart_write_job',['../group__asfdoc__sam0__sercom__usart__group.html#ga3e68847c609aa708be3e1f282f760e8d',1,'usart_write_job(struct usart_module *const module, const uint16_t *tx_data):&#160;usart_interrupt.c'],['../group__asfdoc__sam0__sercom__usart__group.html#ga3e68847c609aa708be3e1f282f760e8d',1,'usart_write_job(struct usart_module *const module, const uint16_t *tx_data):&#160;usart_interrupt.c']]],
  ['usart_5fwrite_5fwait',['usart_write_wait',['../group__asfdoc__sam0__sercom__usart__group.html#gaee8b142e8ad13e1e226334a9954e853c',1,'usart_write_wait(struct usart_module *const module, const uint16_t tx_data):&#160;usart.c'],['../group__asfdoc__sam0__sercom__usart__group.html#gaee8b142e8ad13e1e226334a9954e853c',1,'usart_write_wait(struct usart_module *const module, const uint16_t tx_data):&#160;usart.c']]],
  ['use_5fexternal_5fclock',['use_external_clock',['../structusart__config.html#a7c5c958b33dac14a00d863930acdbcd0',1,'usart_config']]],
  ['user',['USER',['../union_e_v_s_y_s___u_s_e_r___type.html#adc1799f23711c3b601b908f2649433a7',1,'EVSYS_USER_Type::USER()'],['../struct_evsys.html#a1b6579803317a293d3c8290250222e89',1,'Evsys::USER()']]],
  ['user_5fboard',['USER_BOARD',['../group__group__common__boards.html#ga6e6dffe821337dbaaa12bca06e8e5119',1,'board.h']]],
  ['user_5fboard_2eh',['user_board.h',['../user__board_8h.html',1,'']]],
  ['user_20board',['User board',['../group__user__board__group.html',1,'']]],
  ['user_5fext_5fboard',['USER_EXT_BOARD',['../group__group__common__boards.html#gaf75bc91f67cb2a761894691a9175a639',1,'board.h']]],
  ['usrrdy',['USRRDY',['../union_e_v_s_y_s___c_h_s_t_a_t_u_s___type.html#a5137b313d890d629cc3aea37e4ef6894',1,'EVSYS_CHSTATUS_Type']]],
  ['usrrdy0',['USRRDY0',['../union_e_v_s_y_s___c_h_s_t_a_t_u_s___type.html#aeeeacbd24c2d6016bf09d2b78da29880',1,'EVSYS_CHSTATUS_Type']]],
  ['usrrdy1',['USRRDY1',['../union_e_v_s_y_s___c_h_s_t_a_t_u_s___type.html#acd55fba237d0bdde95b399f9595aeb3f',1,'EVSYS_CHSTATUS_Type']]],
  ['usrrdy2',['USRRDY2',['../union_e_v_s_y_s___c_h_s_t_a_t_u_s___type.html#addd3120b348a9ec00ea4e1df62f306ea',1,'EVSYS_CHSTATUS_Type']]],
  ['usrrdy3',['USRRDY3',['../union_e_v_s_y_s___c_h_s_t_a_t_u_s___type.html#a0d330f19c8fc4880100a05734deaaa7d',1,'EVSYS_CHSTATUS_Type']]],
  ['usrrdy4',['USRRDY4',['../union_e_v_s_y_s___c_h_s_t_a_t_u_s___type.html#a6d9da16c146b3537d9366a8cb401855e',1,'EVSYS_CHSTATUS_Type']]],
  ['usrrdy5',['USRRDY5',['../union_e_v_s_y_s___c_h_s_t_a_t_u_s___type.html#a399211d9292acd1d6750936a6f71a800',1,'EVSYS_CHSTATUS_Type']]],
  ['usrrdy6',['USRRDY6',['../union_e_v_s_y_s___c_h_s_t_a_t_u_s___type.html#ad454cc75ac64f709b9855787750a30ee',1,'EVSYS_CHSTATUS_Type']]],
  ['usrrdy7',['USRRDY7',['../union_e_v_s_y_s___c_h_s_t_a_t_u_s___type.html#a33697e01aea3c22981f366df1860329d',1,'EVSYS_CHSTATUS_Type']]],
  ['uxqueuemessageswaiting',['uxQueueMessagesWaiting',['../ux_queue_messages_waiting.html',1,'']]],
  ['uxtaskgetnumberoftasks',['uxTaskGetNumberOfTasks',['../ux_task_get_number_of_tasks.html',1,'']]],
  ['uxtaskpriorityget',['uxTaskPriorityGet',['../group__ux_task_priority_get.html',1,'']]]
];
