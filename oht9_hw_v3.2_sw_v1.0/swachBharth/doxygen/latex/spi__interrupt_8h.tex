\hypertarget{spi__interrupt_8h}{}\section{src/\+A\+S\+F/sam0/drivers/sercom/spi/spi\+\_\+interrupt.h File Reference}
\label{spi__interrupt_8h}\index{src/\+A\+S\+F/sam0/drivers/sercom/spi/spi\+\_\+interrupt.\+h@{src/\+A\+S\+F/sam0/drivers/sercom/spi/spi\+\_\+interrupt.\+h}}


S\+AM Serial Peripheral Interface Driver (Callback Mode)  


{\ttfamily \#include \char`\"{}spi.\+h\char`\"{}}\newline
\subsection*{Functions}
\begin{Indent}\textbf{ Callback Management}\par
\begin{DoxyCompactItemize}
\item 
void \hyperlink{group__asfdoc__sam0__sercom__spi__group_ga9736ace2b57a6d45d3fb820aae552a48}{spi\+\_\+register\+\_\+callback} (struct \hyperlink{structspi__module}{spi\+\_\+module} $\ast$const module, \hyperlink{group__asfdoc__sam0__sercom__spi__group_ga88d771c03a254735de0053be3fa513ca}{spi\+\_\+callback\+\_\+t} callback\+\_\+func, enum \hyperlink{group__asfdoc__sam0__sercom__spi__group_ga4afb8830e0197ec11f6beb8140210a88}{spi\+\_\+callback} callback\+\_\+type)
\begin{DoxyCompactList}\small\item\em Registers a S\+PI callback function. \end{DoxyCompactList}\item 
void \hyperlink{group__asfdoc__sam0__sercom__spi__group_ga7df7ed6a6a9d6a5e1338ceaa0d5f07b2}{spi\+\_\+unregister\+\_\+callback} (struct \hyperlink{structspi__module}{spi\+\_\+module} $\ast$module, enum \hyperlink{group__asfdoc__sam0__sercom__spi__group_ga4afb8830e0197ec11f6beb8140210a88}{spi\+\_\+callback} callback\+\_\+type)
\begin{DoxyCompactList}\small\item\em Unregisters a S\+PI callback function. \end{DoxyCompactList}\end{DoxyCompactItemize}
\end{Indent}
\begin{Indent}\textbf{ Writing and Reading}\par
\begin{DoxyCompactItemize}
\item 
enum \hyperlink{group__group__sam0__utils__status__codes_ga751c892e5a46b8e7d282085a5a5bf151}{status\+\_\+code} \hyperlink{group__asfdoc__sam0__sercom__spi__group_ga7a7ddeab7e3ed355e13366ed635ff152}{spi\+\_\+write\+\_\+buffer\+\_\+job} (struct \hyperlink{structspi__module}{spi\+\_\+module} $\ast$const module, uint8\+\_\+t $\ast$tx\+\_\+data, uint16\+\_\+t length)
\begin{DoxyCompactList}\small\item\em Asynchronous buffer write. \end{DoxyCompactList}\item 
enum \hyperlink{group__group__sam0__utils__status__codes_ga751c892e5a46b8e7d282085a5a5bf151}{status\+\_\+code} \hyperlink{group__asfdoc__sam0__sercom__spi__group_gac5022683cb35d827578c26cf7689ae61}{spi\+\_\+read\+\_\+buffer\+\_\+job} (struct \hyperlink{structspi__module}{spi\+\_\+module} $\ast$const module, uint8\+\_\+t $\ast$rx\+\_\+data, uint16\+\_\+t length, uint16\+\_\+t dummy)
\begin{DoxyCompactList}\small\item\em Asynchronous buffer read. \end{DoxyCompactList}\item 
enum \hyperlink{group__group__sam0__utils__status__codes_ga751c892e5a46b8e7d282085a5a5bf151}{status\+\_\+code} \hyperlink{group__asfdoc__sam0__sercom__spi__group_gaba6ac49efcf4b51fa8131d4a07071967}{spi\+\_\+transceive\+\_\+buffer\+\_\+job} (struct \hyperlink{structspi__module}{spi\+\_\+module} $\ast$const module, uint8\+\_\+t $\ast$tx\+\_\+data, uint8\+\_\+t $\ast$rx\+\_\+data, uint16\+\_\+t length)
\begin{DoxyCompactList}\small\item\em Asynchronous buffer write and read. \end{DoxyCompactList}\item 
void \hyperlink{group__asfdoc__sam0__sercom__spi__group_ga74d13a9fe3203f121664b864f68c94fd}{spi\+\_\+abort\+\_\+job} (struct \hyperlink{structspi__module}{spi\+\_\+module} $\ast$const module)
\begin{DoxyCompactList}\small\item\em Aborts an ongoing job. \end{DoxyCompactList}\end{DoxyCompactItemize}
\end{Indent}


\subsection{Detailed Description}
S\+AM Serial Peripheral Interface Driver (Callback Mode) 

Copyright (C) 2013-\/2015 Atmel Corporation. All rights reserved.